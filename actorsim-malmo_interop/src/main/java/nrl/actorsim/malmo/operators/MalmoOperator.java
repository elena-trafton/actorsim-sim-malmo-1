package nrl.actorsim.malmo.operators;

import nrl.actorsim.domain.Operator;
import nrl.actorsim.domain.ProgramStatement;
import nrl.actorsim.goalnetwork.GoalNetworkNode;

public class MalmoOperator extends GoalNetworkNode<ProgramStatement> {
    public MalmoOperator(String name) {
        super(new Operator(name));
    }
    public MalmoOperator(Operator operator) {
        super(operator);
        setPrimitive();
    }

    @Override
    public Operator getStored() {
        return (Operator) super.getStored();
    }
}
