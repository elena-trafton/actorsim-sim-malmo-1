package nrl.actorsim.malmo.operators;

import nrl.actorsim.domain.WorldObject;

/**
 * An Operator to crafts an item in inventory.
 *
 * preconditions:
 *   near(CRAFTING_TABLE) - ignored in Malmo crafting
 *   have required ingredients
 * effects:
 *   required ingredients consumed
 *   inventory has crafted item
 * body:
 *   malmo-exec: craft(item)
 */

public class CraftItem extends MalmoOperator {

    private final WorldObject item;

    public CraftItem(WorldObject item) {
        super("craft");
        this.item = item;
        
//        StateVariable haveItem = CRAFT_ITEM.newBuilder().(item);
//        NumericPersistenceStatement haveOneBread
//                = new NumericPersistenceStatement(haveItem, quantity);
//
    }

    @Override
    public String toString() {
        return stored.toString() + "(" + item + ")";
    }
}
