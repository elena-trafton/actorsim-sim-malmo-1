package nrl.actorsim.malmo;

import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.goalnetwork.Method;
import nrl.actorsim.goalnetwork.TemporalGoalNetwork;
import nrl.actorsim.malmo.goals.*;
import nrl.actorsim.malmo.operators.MalmoOperator;
import nrl.actorsim.domain.StateVariable;
import nrl.actorsim.malmo.operators.CraftItem;

import static nrl.actorsim.malmo.MalmoTypes.*;
import static nrl.actorsim.domain.WorldType.NUMBER;

@SuppressWarnings("unused")
public class MalmoPlanningDomain extends PlanningDomain {
    private static final MalmoLocation LOCATION = new MalmoLocation();
    public static StateVariable ENTITY_AT = StateVariable.builder()
            .name("entity_at").argTypes(ENTITY).valueType(LOCATION).build();
    public static StateVariable INVENTORY_CONTAINS = StateVariable.builder()
            .name("contains-inventory").argTypes(ITEM).valueType(NUMBER).build();


    public MalmoPlanningDomain() {
        super(Options.builder()
                .closedWorldAssumption()
                .hideTypesWhenPossible()
                .build()
        );
    }

    /**
     * A numeric TGN for crafting one loaf of bread from wheat in inventory.
     * <p>
     * in-inventory(bread) >= 1
     *   m-craft-from-inventory(bread) ORDERED
     *   ..inventory-contains(wheat) == 3 -> 0
     *   ..craft(bread)
     * <p>
     * @return a method for crafting bread from inventory
     */
    public static Method buildCraftBreadFromInventoryMethod() {
        TemporalGoalNetwork decomposition = new TemporalGoalNetwork();
        MalmoGoal haveBread = InventoryContains.buildPersistence(BREAD, 1);
        MalmoGoal haveWheat = InventoryContains.buildConsume(WHEAT, 3, 0);
        MalmoOperator craftBread = new CraftItem(BREAD);

        decomposition.addSubsOrdered(haveBread, haveWheat, craftBread);
        return new Method(decomposition);
    }
}
