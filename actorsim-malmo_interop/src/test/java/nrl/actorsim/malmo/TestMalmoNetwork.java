package nrl.actorsim.malmo;

import nrl.actorsim.goalnetwork.Method;
import nrl.actorsim.goalnetwork.TemporalGoalNetwork;
import nrl.actorsim.malmo.goals.InventoryContains;
import nrl.actorsim.malmo.goals.MalmoGoal;
import org.testng.annotations.Test;

import static nrl.actorsim.malmo.MalmoPlanningDomain.buildCraftBreadFromInventoryMethod;
import static nrl.actorsim.malmo.MalmoTypes.BREAD;

public class TestMalmoNetwork {

    @Test
    public void createObtainBreadInventory() {
        MalmoGoal haveBread = InventoryContains.buildPersistence(BREAD, 1);
        TemporalGoalNetwork tgn = new TemporalGoalNetwork();
        tgn.add(haveBread);
        Method craftBread = buildCraftBreadFromInventoryMethod();
        assert tgn.decomposeWith(craftBread);
    }

    /**
     * A numeric TGN for crafting one loaf of bread by exploring for wheat.
     *
     * in-inventory(bread) = 1
     *   m-craft-with-recipe(bread, 1) ORDERED
     *   ..in-inventory(wheat) = 3
     *   .   m-explore-for(wheat, 3)
     *   .     *explore-for(wheat)
     *   .     *move-near(wheat-1)
     *   .     *collect(wheat-1)
     *   .     *explore-for(wheat)
     *   .     *move-near(wheat-2)
     *   .     *collect(wheat-2)
     *   .     *explore-for(wheat)
     *   .     *move-near(wheat-3)
     *   .     *collect(wheat-3)
     *   ..craft(bread)
     */
    public void createObtainBreadUsingExplore() {
    }

    /**
     * A numeric TGN for making a torches.
     *
     * obtain(torch, 4)
     *    m-craft-with-recipe(torch,4) {BEFORE((1,2), 3)}
     *    1.in-inventory(sticks) = 1
     *    .    m-craft-with-recipe(sticks, 1) ORDERED
     *    .    ..in-inventory(planks, 2)
     *    .    .    m-craft-with-recipe(planks, 2) ORDERED
     *    .    .    ..in-inventory(wood) = 1
     *    .    .    .   m-explore-fore(wood, 1)
     *    .    .    .     *exploreFor(tree)
     *    .    .    .     *moveNear(tree-1)
     *    .    .    .     *lookAt(woodblock-1)
     *    .    .    .     *breakBlock(woodblock-1)
     *    .    .    .     *collect(wood)
     *    .    .    ..craft(planks)
     *    .    ..craft(sticks)
     *    2.in-inventory(coal, 1) ORDERED
     *    .    m-explore-for(coal, 1)
     *    .      *exploreFor(coal)
     *    .      *moveNear(coal-1)
     *    .      *lookAt(coal-1)
     *    .      *breakBlock(coal-1)
     *    .      *collect(coal)
     *    3.craft(torch)
     */
    public void createTorchNetwork() {
        //TODO write this!
    }


}
