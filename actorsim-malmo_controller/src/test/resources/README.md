# Unit Tests

Idea: In the future, I think it might be a beneficial to set up an automated testing environment on Bitbucket. That way, any commit that we do is automatically tested against these unit tests.

# Scripts to run to ensure SHOP2 Planner is Working

In `goal_manager_test.py`, there are a set of functions with the word `shop` in their name. These functions test the SHOP2 planner.

Function names:
  1. `test_fast_shop_slaughter_chicken`
    Success condition: `chicken_meat > 6`
  2. `test_shop_fast_obtain_beef`
    Success condition: `beef > 6`
  3. `test_shop_obtain_pumpkin_pie`
    Success condition: `pumpkin_pie >=1`
  4. `test_shop_obtain_potatoes`
    Success condition: `potato > 5`
  5. `test_shop_obtain_carrots`
    Success condition: `carrot > 6`
  6. `test_shop_obtain_beetroots`
    Success condition: `beetroot >= 4`
  7. `test_shop_omnivore`
    Success condition: ?
  8. `test_shop_herbivore`
    Success condition: ?
  9. `test_shop_carnivore`
    Success condition: ?
  10. `test_fast_shop_obtain_pumpkin_pie_told_about_sugar`
    Success condition: `pumpkin_pie >= 1 `
  11. `test_fast_shop_omnivore_told_about_chickens`
    Success condition: `chicken_meat >= 3`
  12. `test_fast_shop_omnivore_told_about_cows_then_chickens`
    Success condition: `egg > 0` and `chicken_meat > 0`
  13. `test_fast_shop_herbivore_tidies_inventory_for_planting`
    Success condition: `bread >= 1` and preferred planting hotbar positions matches
    Preferred hotbar positions:
    ```lisp ("iron_sword", 1),
            ("bone_meal", 2),
            ("wheat_seeds", 3),
            ("beetroot_seeds", 4),
            ("carrot", 5),
            ("potato", 6)
            ```
  14. `test_fast_shop_obtain_cake_only_iron_sword_in_inventory`
    Success condition: `cake >= 1`
  15. `test_fast_shop_obtain_cake`
    Success condition: `cake >= 1`
  16. `test_fast_shop_plant_potatoes_when_pumpkin_planted`
    Success condition: `potato >=2` and `pumpkin_stem` has been observed
  17. `test_fast_shop_pumpkin_pie_planted_pumpkin_explore_for_egg`
    Success condition: Inventory contains `pumpkin_pie`
  18. `test_fast_omnivore_shop_told_about_eggs` (NOT READY YET?)
    Success condition: ?
  19. `test_fast_omnivore_shop_told_about_pumpkin_block` (NOT READY YET?)
    Success condition: ?

# Scripts to runt o ensure Plan Recognition is Working
