import unittest

import time

import test_missions
from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from location import Location, BlockLocation
from loggers import MalmoLoggers
from mission import ObservationGridDetails
from action import OrderedConjunctiveAction
from actions import MoveNearTarget, Pause, MoveSafelyToTarget

class WalkSafelyTest(unittest.TestCase):

    def test_move_safely_target_high_up(self):

        mission = test_missions.get_empty_mission("Positive X")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(7, 20, 0)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        goal_xz = Location(7, 16, 0).int_key_str()
        player_location = character.get_location().int_key_str()
        assert player_location == goal_xz

    def test_move_safely_along_positive_x(self):

        mission = test_missions.get_empty_mission("Positive X")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 16, 2, type='air')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(7, 16, 0)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        start_location = Location(0, 16, 0).int_key_str()
        player_location = character.get_location().int_key_str()
        assert player_location == start_location

    def test_move_safely_water(self):

        mission = test_missions.get_empty_mission("Positive X")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 15, 2, type='water')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(7, 16, 0)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        start_location = Location(0, 16, 0).int_key_str()
        player_location = character.get_location().int_key_str()
        assert player_location == start_location

    def test_move_safely_lava(self):

        mission = test_missions.get_empty_mission("Positive X")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 15, 2, type='lava')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(7, 16, 0)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        start_location = Location(0, 16, 0).int_key_str()
        player_location = character.get_location().int_key_str()
        assert player_location == start_location

    def test_move_safely_along_negitive_x(self):

        mission = test_missions.get_empty_mission("Negitive X")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 16, 2, type='air')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 5.5
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(0, 16, 0)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        start_location = Location(5, 16, 0).int_key_str()
        player_location = character.get_location().int_key_str()
        assert player_location == start_location

    def test_move_safely_along_positive_z(self):

        mission = test_missions.get_empty_mission("Positive Z")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 16, 2, type='tallgrass')
        mission.add_cuboid(4, 10, -2, 1, 13, 2, type='grass')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 3.5
        mission.start["Alex"].z = -2.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(3, 16, 4)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)
        character.wait_on_mission_end()

        start_location = Location(3, 16, -2).int_key_str()
        player_location = character.get_location().int_key_str()
        assert player_location == start_location

    def test_move_safely_along_negitive_z(self):

        mission = test_missions.get_empty_mission("Negitive z")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 16, 2, type='tallgrass')
        mission.add_cuboid(4, 10, -2, 1, 13, 2, type='grass')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 3.5
        mission.start["Alex"].z = 3.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(3, 16, -4)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        start_location = Location(3, 16, 3).int_key_str()
        player_location = character.get_location().int_key_str()
        assert player_location == start_location

    def test_move_safely_along_positive_diagonal(self):

        mission = test_missions.get_empty_mission("Positive diagonal")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 16, 2, type='tallgrass')
        mission.add_cuboid(4, 10, -2, 1, 13, 2, type='grass')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 5.5
        mission.start["Alex"].z = -1.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(1, 16, 4)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        assert character.get_location().x > 3.0 and character.get_location().z < 0.0

    def test_move_safely_along_negitive_diagonal(self):

        mission = test_missions.get_empty_mission("negative diagonal")

        mission.time_in_ms = 25000
        mission.ms_per_tick = 25
        mission.clear_inventory()
        mission.add_cuboid(4, 14, -2, 1, 16, 2, type='tallgrass')
        mission.add_cuboid(4, 10, -2, 1, 13, 2, type='grass')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 5.5
        mission.start["Alex"].z = 4.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(0, 16, -3)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        assert character.get_location().x > 2.0 and character.get_location().z > 1.0

    def test_move_safely_survey_sample(self):

        mission = test_missions.get_empty_mission("Small pit example")

        mission.time_in_ms = 105000
        mission.ms_per_tick = 30
        mission.clear_inventory()
        mission.add_cuboid(-2, 10, 8, -4, 13, 10, type='grass')
        mission.add_cuboid(-2, 14, 8, -4, 15, 10, type='tallgrass')
        mission.add_cuboid(-2, 15, 8, -4, 16, 10, type='air')
        mission.add_cuboid(-4, 10, 10, -4, 14, 10, type='grass')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(-19, 16, 40)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        assert character.get_location().x > -6.0 and character.get_location().z < 8.5

    def test_move_safely_at_world_level(self):

        mission = test_missions.get_empty_mission("Small pit example at world level")

        mission.time_in_ms = 105000
        mission.ms_per_tick = 30
        mission.clear_inventory()
        mission.add_cuboid(5, 10, -5, -30, 63, 50, type='grass')
        mission.add_cuboid(-2, 62, 8, -4, 62, 10, type='tallgrass')
        mission.add_cuboid(-2, 63, 8, -4, 64, 10, type='air')
        mission.add_cuboid(-4, 62, 10, -4, 62, 10, type='grass')

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -2, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0.5
        mission.start["Alex"].y = 64
        mission.start["Alex"].z = 0.5

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)

        goal_location = Location(-19, 16, 40)
        root = OrderedConjunctiveAction("Move")
        root.add(MoveSafelyToTarget(character, target=goal_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()

        assert character.get_location().x > -6.0 and character.get_location().z < 8.5