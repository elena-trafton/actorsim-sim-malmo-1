import unittest

import test_missions
from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from loggers import MalmoLoggers
from mission import ObservationGridDetails


class ExploreTests(unittest.TestCase):
    def test_perimeter_placement(self):
        """Ensure that entities and items are spawned nearby.  Largely a visual test."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        item_list = test_missions.get_perimeter_mission_items()
        mission = test_missions.create_basic_perimeter_mision("walk about", 5000,
                                                              area, item_list, 15)
        mission.add_melon_farm(0, 15, 0, plant=False, width=1)
        mission.ms_per_tick = 50

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0
        mission.start["Alex"].z = 0

        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_on_mission_end()

        assert (False)


    def test_fast_perimeter_explore(self):
        """Walk a larger perimeter to ensure items and entities appear as expected."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        area = Area(min_x=-45, max_x=45, min_z=-45, max_z=45)
        item_list = test_missions.get_perimeter_mission_items()
        mission = test_missions.create_basic_perimeter_mision("walk about", 120000,
                                                              area, item_list, 15)
        mission.add_melon_farm(0, 15, 0, plant=False, width=1)
        mission.ms_per_tick = 15

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0
        mission.start["Alex"].z = 0

        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_on_mission_end()

        assert (False)

