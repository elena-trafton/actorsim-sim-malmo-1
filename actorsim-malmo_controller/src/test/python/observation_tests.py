import time
import unittest

import test_missions
from character.character import Character
from utils.LogManager import LogManager
from location import Location
from loggers import MalmoLoggers
from mission import ObservationGridDetails

logger = LogManager.get_logger("actorsim.malmo.tests")

class ObservationTests(unittest.TestCase):

    def test_observe_next_to_alex(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)

        mission_name = "multi_block"
        mission = test_missions.get_multi_block_mission()
        mission.time_in_ms = 3000
        mission.start["Alex"].pitch = 60
        character = Character(mission)
        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.log_observation_blocks = True
        MalmoLoggers.log_observation_entities = True


        character.start_mission()

        character.wait_on_mission_end()

        stone = character.get_closest_block_location("stone")  # type: Location
        farmland = character.get_closest_block_location("farmland")  # type: Location
        sand = character.get_closest_block_location("sand")  # type: Location
        sandstone = character.get_closest_block_location("sandstone")  # type: Location
        gravel = character.get_closest_block_location("gravel")  # type: Location
        assert (stone.int_key_str() == "0_15_0")
        assert (farmland.int_key_str() == "-1_16_0")
        assert (sand.int_key_str() == "1_15_0")
        assert (sandstone.int_key_str() == "0_15_-1")
        assert (gravel.int_key_str() == "0_16_-1")


    def test_pumpkin_blocks_from_start(self):
        malmo_root_logger = LogManager.get_logger("actorsim.malmo")
        malmo_root_logger.setLevel(LogManager.INFO)

        mission = test_missions.get_simple_melon_mission()
        alex_grid = ObservationGridDetails("Alex3x3").start(-20, -1, -20).size(40, 3, 40).relative() # allow Alex to see the pumpkins
        mission.observation_grids["Alex3x3"] = alex_grid
        # mission.start["Alex"].x = 9.5
        # mission.start["Alex"].z = 0.5
        # mission.start["Alex"].yaw = -90
        # mission.start["Alex"].pitch = 30

        mission.time_in_ms = 2000
        character = Character(mission)
        MalmoLoggers.log_observation_blocks = True

        character.start_mission()

        character.wait_on_mission_end(sleep_between_checks_in_seconds=1)

        #observe the character state of blocks around the character
        latest_state = character.get_latest_state()
        lines = character.UNIT_TEST_ONLY_get_memory().get_layer_matrix_as_string_array(alex_grid.name, latest_state.location, y_layer=16)
        logger.info("block knowledge:")
        for line in lines:
            logger.info("{}".format(line.replace("air", ".  ")))
        location = Location()
        assert(character.get_block_type_at(Location(10, 16, -1)) == "air")
        assert(character.get_block_type_at(Location(10, 16, 0)) == "air")
        assert(character.get_block_type_at(Location(10, 16, 1)) == "pumpkin")
