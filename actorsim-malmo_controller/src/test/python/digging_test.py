import unittest

import test_missions
from utils.LogManager import LogManager
from actions import *
from loggers import MalmoLoggers

logger = LogManager.get_logger("actorsim.malmo.tests")

class DiggingTest(unittest.TestCase):

    def test_block_breaking(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission_name = "test_block_breaking"
        mission = test_missions.get_simple_melon_mission(plant_pumpkin=True)
        mission.time_in_ms = 10000
        alex_grid = ObservationGridDetails("Alex3x3").start(-5, -1, -5).size(10, 3, 5).relative() # allow Alex to see the pumpkins
        mission.observation_grids["Alex3x3"] = alex_grid

        character = Character(mission)

        location_to_break = Location(10, 16, 1)
        root = TestBreakBlock(character, location_to_break)
        character.enqueue_action(root)

        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True

        character.start_mission()
        character.wait_for_item_in_inventory("pupkin", desired=1)

        pumpkin_in_inventory = character.amount_of_item_in_inventory("pumpkin") > 0
        pumpkin_floating_nearby = False
        if character.has_observed("pumpkin", at_least_one_valid_location=True):
            pumpkin_floating_nearby = True

        assert(pumpkin_in_inventory or pumpkin_floating_nearby)

    def test_block_break_by_type_single(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission_name = "test_pumpkin_breaking"
        mission = test_missions.get_simple_melon_mission()
        mission.start["Alex"].x = 7
        mission.start["Alex"].z = -3

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.time_in_ms = 8000
        character = Character(mission)

        root = TestBreakBlockType(character, "pumpkin")
        character.enqueue_action(root)

        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True

        character.start_mission()

        character.wait_for_item_in_inventory("pupkin", desired=1)

        pumpkin_in_inventory = character.amount_of_item_in_inventory("pumpkin") > 0
        pumpkin_floating_nearby = False
        pumpkin_drop_location = character.get_closest_entity_location("pumpkin")
        if isinstance(pumpkin_drop_location, DropLocation):
            pumpkin_floating_nearby = True

        assert(pumpkin_in_inventory)
        assert(not pumpkin_floating_nearby)

    def test_block_break_by_type_fast(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.INFO)

        mission_name = "test_pumpkin_breaking"
        mission = test_missions.get_simple_melon_mission(plant_pumpkin=True)
        mission.start["Alex"].x = 5
        mission.start["Alex"].z = 5
        mission.ms_per_tick = 25
        alex_grid = ObservationGridDetails("Alex3x3").start(-15, -1, -15).size(30, 3, 30).relative() # allow Alex to see the pumpkins
        mission.observation_grids["Alex3x3"] = alex_grid

        mission.time_in_ms = 45000
        character = Character(mission)

        root = TestBreakBlockType(character, "pumpkin")
        character.enqueue_action(root)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_for_item_in_inventory("pupkin", desired=5)

        pumpkin_in_inventory = character.amount_of_item_in_inventory("pumpkin") >= 5
        assert pumpkin_in_inventory

