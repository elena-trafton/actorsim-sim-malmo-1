import time
import unittest

import test_missions
from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from loggers import MalmoLoggers
from mission import ObservationGridDetails
from location import Location


class WorldTests(unittest.TestCase):
    seeds = [1004, 1008, 1015, 1018, 1020, 1024, 1027, 1035, 1038, 1040,
             1041, 1043, 1053, 1067, 1075, 1088, 1113, 1114, 1118, 1129,
             1139, 1141, 1142, 1144, 1159, 1161, 1165, 1175, 1176, 1177,
             1179, 1190, 1192, 1193, 1205, 1206, 1207, 1213, 1224, 1228,
             1230, 1234, 1240, 1246]


    start_positions = [[0.5, 64.0, 0.5],
                       [0.5, 70.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 68.0, 0.5],
                       [0.5, 70.0, 0.5],
                       [0.5, 72.0, 0.5],
                       [0.5, 69.0, 0.5],
                       [0.5, 66.0, 0.5],
                       [0.5, 66.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 66.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 69.0, 0.5],
                       [0.5, 64.0, 0.5],
                       [0.5, 72.0, 0.5],
                       [0.5, 72.0, 0.5],
                       [0.5, 70.0, 0.5],
                       [0.5, 64.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 72.0, 0.5],
                       [0.5, 67.0, 0.5],
                       [0.5, 70.0, 0.5],
                       [0.5, 68.0, 0.5],
                       [0.5, 63.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 69.0, 0.5],
                       [0.5, 62.0, 0.5],
                       [0.5, 62.0, 0.5],
                       [0.5, 64.0, 0.5],
                       [0.5, 69.0, 0.5],
                       [0.5, 67.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 67.0, 0.5],
                       [0.5, 69.0, 0.5],
                       [0.5, 64.0, 0.5],
                       [0.5, 64.0, 0.5],
                       [0.5, 72.0, 0.5],
                       [0.5, 67.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 63.0, 0.5],
                       [0.5, 67.0, 0.5],
                       [0.5, 71.0, 0.5],
                       [0.5, 72.0, 0.5],
                       [0.5, 70.0, 0.5]]

    observation_test = [['double_plant', 'tallgrass', 'grass', 'dirt', 'stone', 'air'],
                        ['tallgrass', 'Wolf', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'],
                        ['double_plant', 'dirt', 'water', 'log', 'Sheep', 'ice', 'sand', 'stone', 'air', 'snow_layer',
                         'tallgrass', 'leaves', 'grass'],
                        ['leaves', 'tallgrass', 'chicken', 'grass', 'gravel', 'dirt', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'air', 'red_flower', 'grass', 'dirt', 'stone', 'coal_ore'],
                        ['tallgrass', 'chicken', 'log', 'leaves', 'grass', 'dirt', 'cow', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'yellow_flower', 'grass', 'gravel', 'dirt', 'stone', 'air',
                         'red_flower'],
                        ['log2', 'dirt', 'clay', 'log', 'sand', 'stone', 'air', 'yellow_flower', 'double_plant',
                         'tallgrass', 'gravel', 'water', 'grass'],
                        ['lava', 'double_plant', 'tallgrass', 'grass', 'dirt', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'coal_ore', 'leaves', 'grass', 'dirt', 'stone', 'air', 'Horse'],
                        ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'air'],
                        ['tallgrass', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air', 'yellow_flower'],
                        ['double_plant', 'tallgrass', 'Pig', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'],
                        ['tallgrass', 'dirt', 'Sheep', 'leaves', 'grass', 'sand', 'log2', 'air', 'yellow_flower'],
                        ['double_plant', 'tallgrass', 'Pig', 'vine', 'FallingSand', 'grass', 'dirt', 'cow', 'stone',
                         'air'],
                        ['tallgrass', 'grass', 'gravel', 'dirt', 'cow', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'dirt', 'water', 'air', 'grass', 'sand', 'stone', 'coal_ore'],
                        ['leaves2', 'log2', 'air', 'Sheep'],
                        ['grass', 'tallgrass', 'dirt', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'Pig', 'grass', 'gravel', 'dirt', 'stone', 'air', 'red_flower'],
                        ['double_plant', 'tallgrass', 'sand', 'log', 'Sheep', 'red_flower', 'grass', 'dirt', 'stone',
                         'air'],
                        ['tallgrass', 'Pig', 'log', 'grass', 'dirt', 'stone', 'air'],
                        ['tallgrass', 'sand', 'water', 'log', 'grass', 'waterlily', 'gravel', 'dirt', 'air'],
                        ['double_plant', 'tallgrass', 'log', 'log2', 'grass', 'dirt', 'cow', 'stone', 'air',
                         'yellow_flower'],
                        ['deadbush', 'cactus', 'Rabbit', 'sand', 'stone', 'air'],
                        ['brown_mushroom', 'chicken', 'clay', 'log', 'iron_ore', 'water', 'vine', 'dirt', 'stone',
                         'air', 'tallgrass', 'coal_ore', 'grass', 'waterlily'],
                        ['tallgrass', 'dirt', 'water', 'grass', 'gravel', 'sand', 'air'],
                        ['deadbush', 'dirt', 'air', 'Rabbit', 'sand', 'stone', 'coal_ore', 'cactus'],
                        ['tallgrass', 'water', 'log', 'leaves', 'grass', 'dirt', 'yellow_flower', 'air'],
                        ['tallgrass', 'dirt', 'log2', 'grass', 'sand', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'sand', 'leaves', 'grass', 'dirt', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'Pig', 'log', 'grass', 'dirt', 'stone', 'air'],
                        ['tallgrass', 'coal_ore', 'leaves', 'grass', 'dirt', 'stone', 'air', 'log'],
                        ['double_plant', 'gravel', 'tallgrass', 'sand', 'clay', 'air', 'grass', 'water', 'dirt',
                         'stone', 'coal_ore'],
                        ['double_plant', 'tallgrass', 'red_flower', 'grass', 'dirt', 'stone', 'air', 'yellow_flower'],
                        ['tallgrass', 'yellow_flower', 'coal_ore', 'Sheep', 'leaves', 'grass', 'dirt', 'stone', 'air',
                         'log'],
                        ['double_plant', 'tallgrass', 'log', 'leaves', 'grass', 'dirt', 'stone', 'air'],
                        ['double_plant', 'tallgrass', 'dirt', 'water', 'flowing_water', 'Pig', 'grass', 'sand', 'stone',
                         'air', 'yellow_flower'],
                        ['chicken', 'water', 'sand', 'stone', 'air', 'yellow_flower', 'double_plant', 'tallgrass',
                         'gravel', 'grass', 'cow', 'dirt'],
                        ['tallgrass', 'log', 'leaves', 'FallingSand', 'grass', 'dirt', 'air']]

    # I think this is all of the animals that currently spawn in worlds
    # If test cases start to fail it might be worth it to check this list again.
    animals = ['Cow', 'Chicken', 'Rabbit', 'Horse', 'Sheep', 'Pig']

    # While loading in, I think malmo might read the world state. This falling sands block seems very odd
    block_anomalies = ['FallingSand']

    def test_mushroom_world(self):
        seed = 27
        mission = test_missions.get_default_world_mission("Test in real world seed: " + str(self.seeds[seed]),
                                                          world_seed=str(self.seeds[seed]))

        mission.time_in_ms = 5000
        mission.ms_per_tick = 50
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = self.start_positions[seed][0]
        mission.start["Alex"].y = self.start_positions[seed][1]
        mission.start["Alex"].z = self.start_positions[seed][2]

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

        character.start_mission()
        time.sleep(1)
        character.wait_on_mission_end()

        observed_types = list(character.UNIT_TEST_ONLY_get_memory().get_list_observed_block_types())
        self.check_equals_obs(self.observation_test[seed], observed_types)
        closest_type = character.UNIT_TEST_ONLY_get_memory().get_closest_block_location('red_mushroom')
        closest_type = character.UNIT_TEST_ONLY_get_memory().get_closest_entity_location('red_mushroom')
        location = Location(2, 56, -14)
        mem_check = character.UNIT_TEST_ONLY_get_memory().block_type_at(location)
        assert (self.check_equals_obs(self.observation_test[seed], observed_types))

    def tests_world_loads_and_observations_0_9(self):

        for seed in range(0, 10):
            mission = test_missions.get_default_world_mission("Test in real world seed: " + str(self.seeds[seed]),
                                                              world_seed=str(self.seeds[seed]))

            mission.time_in_ms = 5000
            mission.ms_per_tick = 50
            mission.clear_inventory()

            alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
            mission.observation_grids["Alex3x3"] = alex_grid
            mission.start["Alex"].x = self.start_positions[seed][0]
            mission.start["Alex"].y = self.start_positions[seed][1]
            mission.start["Alex"].z = self.start_positions[seed][2]

            area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
            factory = CharacterFactory()
            character = factory.create_character(mission,
                                                 PlannerType.NONE,
                                                 food_preference=FoodPreference.NONE,
                                                 explore_area=area,
                                                 include_explore_at_end=False)

            MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
            MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
            character.start_mission()
            time.sleep(1)
            character.wait_on_mission_end()

            observed_types = list(character.UNIT_TEST_ONLY_get_memory().get_list_observed_block_types())
            assert (self.check_equals_obs(self.observation_test[seed], observed_types))

    def tests_world_loads_and_observations_10_19(self):

        for seed in range(10, 20):
            mission = test_missions.get_default_world_mission("Test in real world seed: " + str(self.seeds[seed]),
                                                              world_seed=str(self.seeds[seed]))

            mission.time_in_ms = 5000
            mission.ms_per_tick = 50
            mission.clear_inventory()

            alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
            mission.observation_grids["Alex3x3"] = alex_grid
            mission.start["Alex"].x = self.start_positions[seed][0]
            mission.start["Alex"].y = self.start_positions[seed][1]
            mission.start["Alex"].z = self.start_positions[seed][2]

            area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
            factory = CharacterFactory()
            character = factory.create_character(mission,
                                                 PlannerType.NONE,
                                                 food_preference=FoodPreference.NONE,
                                                 explore_area=area,
                                                 include_explore_at_end=False)

            MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
            MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

            character.start_mission()
            time.sleep(1)
            character.wait_on_mission_end()

            observed_types = list(character.UNIT_TEST_ONLY_get_memory().get_list_observed_block_types())
            assert (self.check_equals_obs(self.observation_test[seed], observed_types))

    def tests_world_loads_and_observations_20_29(self):

        for seed in range(20, 30):
            mission = test_missions.get_default_world_mission("Test in real world seed: " + str(self.seeds[seed]),
                                                              world_seed=str(self.seeds[seed]))

            mission.time_in_ms = 5000
            mission.ms_per_tick = 50
            mission.clear_inventory()

            alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
            mission.observation_grids["Alex3x3"] = alex_grid
            mission.start["Alex"].x = self.start_positions[seed][0]
            mission.start["Alex"].y = self.start_positions[seed][1]
            mission.start["Alex"].z = self.start_positions[seed][2]

            area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
            factory = CharacterFactory()
            character = factory.create_character(mission,
                                                 PlannerType.NONE,
                                                 food_preference=FoodPreference.NONE,
                                                 explore_area=area,
                                                 include_explore_at_end=False)

            MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
            MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

            character.start_mission()
            time.sleep(1)
            character.wait_on_mission_end()

            observed_types = list(character.UNIT_TEST_ONLY_get_memory().get_list_observed_block_types())
            assert (self.check_equals_obs(self.observation_test[seed], observed_types))

    def tests_world_loads_and_observations_30_39(self):

        for seed in range(30, 40):
            mission = test_missions.get_default_world_mission("Test in real world seed: " + str(self.seeds[seed]),
                                                              world_seed=str(self.seeds[seed]))

            mission.time_in_ms = 5000
            mission.ms_per_tick = 50
            mission.clear_inventory()

            alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
            mission.observation_grids["Alex3x3"] = alex_grid
            mission.start["Alex"].x = self.start_positions[seed][0]
            mission.start["Alex"].y = self.start_positions[seed][1]
            mission.start["Alex"].z = self.start_positions[seed][2]

            area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
            factory = CharacterFactory()
            character = factory.create_character(mission,
                                                 PlannerType.NONE,
                                                 food_preference=FoodPreference.NONE,
                                                 explore_area=area,
                                                 include_explore_at_end=False)

            MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
            MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

            character.start_mission()
            time.sleep(1)
            character.wait_on_mission_end()

            observed_types = list(character.UNIT_TEST_ONLY_get_memory().get_list_observed_block_types())
            assert (self.check_equals_obs(self.observation_test[seed], observed_types))

    def tests_world_loads_and_observations_40_44(self):
        for seed in range(40, 41):
            mission = test_missions.get_default_world_mission("Test in real world seed: " + str(self.seeds[seed]),
                                                              world_seed=str(self.seeds[seed]))

            mission.time_in_ms = 5000
            mission.ms_per_tick = 50
            mission.clear_inventory()

            alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
            mission.observation_grids["Alex3x3"] = alex_grid
            mission.start["Alex"].x = self.start_positions[seed][0]
            mission.start["Alex"].y = self.start_positions[seed][1]
            mission.start["Alex"].z = self.start_positions[seed][2]

            area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
            factory = CharacterFactory()
            character = factory.create_character(mission,
                                                 PlannerType.NONE,
                                                 food_preference=FoodPreference.NONE,
                                                 explore_area=area,
                                                 include_explore_at_end=False)

            MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
            MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
            MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
            MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)

            character.start_mission()
            time.sleep(1)
            character.wait_on_mission_end()

            observed_types = list(character.UNIT_TEST_ONLY_get_memory().get_list_observed_block_types())
            test_obs = self.observation_test[seed]
            assert (self.check_equals_obs(self.observation_test[seed], observed_types))

    def check_equals_obs(self, test_case_obs, current_obs):
        set(test_case_obs)
        test_obs = set(test_case_obs).difference(set(self.animals)).difference(self.block_anomalies)
        cur_obs = set(current_obs).difference(set(self.animals)).difference(self.block_anomalies)
        return test_obs == cur_obs
