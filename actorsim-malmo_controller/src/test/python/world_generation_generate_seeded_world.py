import time
import unittest

import test_missions
from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from loggers import MalmoLoggers
from mission import ObservationGridDetails


class GenerateWorld(unittest.TestCase):

    """given a seed, this creates the starting location and what the agent observed. The data is formatted and written
    to files which can then be put in to world test as an additional world to explore"""
    def test_generate_world_information_and_tests(self):

        observed_file = open("observed_file.txt", "w")
        location_file = open("location_file.txt", "w")

        world_seed = 1175

        mission = test_missions.get_default_world_mission("Test in real world seed: " + str(world_seed),
                                                          world_seed=str(world_seed))

        mission.time_in_ms = 5000
        mission.ms_per_tick = 50
        mission.clear_inventory()

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -3, -10).size(20, 5, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].y = 62.0

        area = Area(min_x=-15, max_x=15, min_z=-15, max_z=15)
        factory = CharacterFactory()
        character = factory.create_character(mission,
                                             PlannerType.NONE,
                                             food_preference=FoodPreference.NONE,
                                             explore_area=area,
                                             include_explore_at_end=False)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.bindings_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.obs_logger.setLevel(LogManager.TRACE)
        MalmoLoggers.memory_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.command_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.set_log_mission_spec(character, True)
        MalmoLoggers.set_log_entities(character, True)
        MalmoLoggers.set_log_last_command(character, True)

        character.start_mission()
        time.sleep(1)

        observed_file.write("[")
        observed_types = character.UNIT_TEST_ONLY_get_memory().get_list_observed_block_types()
        observed_file.write(", ".join(str(x) for x in observed_types))
        observed_file.write("]\n")

        location_file.write("[")
        location_file.write(", ".join(str(x) for x in character.get_location().as_array()))
        location_file.write("]\n")

        observed_file.close()
        location_file.close()
