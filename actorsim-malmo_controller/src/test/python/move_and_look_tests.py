import unittest

import time

import test_missions
from utils.LogManager import LogManager
from action import OrderedConjunctiveAction
from actions import MoveNearTarget
from character.character import Character
from loggers import MalmoLoggers
from mission import ObservationGridDetails

logger = LogManager.get_logger("actorsim.malmo.tests")


class MoveAndLookTests(unittest.TestCase):

    def test_move_to_farmland_q1(self):
        malmo_root_logger = LogManager.get_logger('actorsim.malmo')
        malmo_root_logger.setLevel(LogManager.INFO)

        mission = test_missions.get_empty_mission("Find the farmland")
        mission.add_cross_farm(5, 15, 5, num_directions=1)
        #mission.add_cross_farm(-5, 15, 5, num_directions=1)
        #mission.add_cross_farm(5, 15, -5, num_directions=1)
        #mission.add_cross_farm(-5, 15, -5, num_directions=1)
        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.time_in_ms = 7000
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0
        mission.start["Alex"].z = 0
        mission.clear_inventory()
        mission.add_inventory_item(0, 'iron_sword', 1)
        mission.add_inventory_item(1, 'iron_hoe', 1)
        mission.add_inventory_item(2, 'dye', 64, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 1)
        mission.add_inventory_item(4, 'pumpkin_seeds', 1)
        mission.add_inventory_item(5, 'potato', 1)
        mission.add_inventory_item(6, 'melon_seeds', 1)
        mission.add_inventory_item(7, 'beetroot_seeds', 1)

        character = Character(mission)

        MalmoLoggers.set_all_non_root_loggers(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        character.start_mission()

        time.sleep(1) #wait for the character to take some observations

        farmland_location = character.get_closest_block_location("farmland")
        root = OrderedConjunctiveAction("Move and look at farmland")
        root.add(MoveNearTarget(character, farmland_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()
        hit_type = character.get_look_hit_type()
        assert(hit_type == "farmland")

    def test_move_to_farmland_q2(self):
        malmo_root_logger = LogManager.get_logger('actorsim.malmo')
        malmo_root_logger.setLevel(LogManager.INFO)

        mission = test_missions.get_empty_mission("Find the farmland")
        #mission.add_cross_farm(5, 15, 5, num_directions=1)
        mission.add_cross_farm(-5, 15, 5, num_directions=1)
        #mission.add_cross_farm(5, 15, -5, num_directions=1)
        #mission.add_cross_farm(-5, 15, -5, num_directions=1)
        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.time_in_ms = 7000
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0
        mission.start["Alex"].z = 0
        mission.clear_inventory()
        mission.add_inventory_item(0, 'iron_sword', 1)
        mission.add_inventory_item(1, 'iron_hoe', 1)
        mission.add_inventory_item(2, 'dye', 64, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 1)
        mission.add_inventory_item(4, 'pumpkin_seeds', 1)
        mission.add_inventory_item(5, 'potato', 1)
        mission.add_inventory_item(6, 'melon_seeds', 1)
        mission.add_inventory_item(7, 'beetroot_seeds', 1)

        character = Character(mission)

        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        character.start_mission()

        time.sleep(1) #wait for the character to take some observations

        farmland_location = character.get_closest_block_location("farmland")
        root = OrderedConjunctiveAction("Move and look at farmland")
        root.add(MoveNearTarget(character, farmland_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()
        hit_type = character.get_look_hit_type()
        assert(hit_type == "farmland")

    def test_move_to_farmland_q3(self):
        malmo_root_logger = LogManager.get_logger('actorsim.malmo')
        malmo_root_logger.setLevel(LogManager.INFO)

        mission = test_missions.get_empty_mission("Find the farmland")
        #mission.add_cross_farm(5, 15, 5, num_directions=1)
        #mission.add_cross_farm(-5, 15, 5, num_directions=1)
        mission.add_cross_farm(-5, 15, -5, num_directions=1)
        #mission.add_cross_farm(5, 15, -5, num_directions=1)
        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.time_in_ms = 7000
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0
        mission.start["Alex"].z = 0
        mission.clear_inventory()
        mission.add_inventory_item(0, 'iron_sword', 1)
        mission.add_inventory_item(1, 'iron_hoe', 1)
        mission.add_inventory_item(2, 'dye', 64, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 1)
        mission.add_inventory_item(4, 'pumpkin_seeds', 1)
        mission.add_inventory_item(5, 'potato', 1)
        mission.add_inventory_item(6, 'melon_seeds', 1)
        mission.add_inventory_item(7, 'beetroot_seeds', 1)

        character = Character(mission)


        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        character.start_mission()

        time.sleep(1) #wait for the character to take some observations

        farmland_location = character.get_closest_block_location("farmland")
        root = OrderedConjunctiveAction("Move and look at farmland")
        root.add(MoveNearTarget(character, farmland_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()
        hit_type = character.get_look_hit_type()
        assert(hit_type == "farmland")

    def test_move_to_farmland_q4(self):
        malmo_root_logger = LogManager.get_logger('actorsim.malmo')
        malmo_root_logger.setLevel(LogManager.INFO)

        mission = test_missions.get_empty_mission("Find the farmland")
        #mission.add_cross_farm(5, 15, 5, num_directions=1)
        #mission.add_cross_farm(-5, 15, 5, num_directions=1)
        #mission.add_cross_farm(-5, 15, -5, num_directions=1)
        mission.add_cross_farm(5, 15, -5, num_directions=1)
        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.time_in_ms = 7000
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 0
        mission.start["Alex"].z = 0
        mission.clear_inventory()
        mission.add_inventory_item(0, 'iron_sword', 1)
        mission.add_inventory_item(1, 'iron_hoe', 1)
        mission.add_inventory_item(2, 'dye', 64, 'WHITE')
        mission.add_inventory_item(3, 'wheat_seeds', 1)
        mission.add_inventory_item(4, 'pumpkin_seeds', 1)
        mission.add_inventory_item(5, 'potato', 1)
        mission.add_inventory_item(6, 'melon_seeds', 1)
        mission.add_inventory_item(7, 'beetroot_seeds', 1)

        character = Character(mission)

        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        character.start_mission()

        time.sleep(1) #wait for the character to take some observations

        farmland_location = character.get_closest_block_location("farmland")
        root = OrderedConjunctiveAction("Move and look at farmland")
        root.add(MoveNearTarget(character, farmland_location))
        character.enqueue_action(root)

        character.wait_on_mission_end()
        hit_type = character.get_look_hit_type()
        assert(hit_type == "farmland")

