;; TODO: use axiom to remove inventory_count that drops to zero?


(defdomain minecraft
  (
   ;; ------- Operators -------

   ;; empty method to tell executive to aquire an item
   (:operator (!acquire ?item ?count)
	      ;; prec
	      nil
	      ;; delete
	      nil
	      ;; add
	      nil
	      )

   ;; empty method to tell executive to craft an item
   (:operator (!craft_item ?placeholder)
   	      nil
   	      nil
   	      nil)
   
   
   ;; helper method to deplete inventory when used
   (:operator (!!use_inventory_resource ?item ?index ?count ?used)
	      ;; prec
	      (and (inventory_count ?index ?item ?count)
		   (not (placeholder ?item))
		   )
	      ;; delete
	      ((inventory_count ?index ?item ?count))
	      ;; add
	      ((inventory_count ?index ?item (call - ?count ?used)))
	      )
   
   ;; helper method to add an item to inventory when created
   (:operator (!!add_crafted_item_to_inventory ?item)
	      ;; prec
	      ((next_empty_inventory_index ?index))
	      ;; delete
	      ((next_empty_inventory_index ?index))
	      ;; add
	      ((inventory_count ?index ?item 1)
	       (next_empty_inventory_index (call + ?index 1)))
	      )
   
   ;; helpful debug success action to show that base case was matched
   (:operator (!!have_item ?item)
   	      nil
   	      nil
   	      nil)
   
   ;; helpful debug fail action to show that no preconditions matched in methods
   (:operator (!!fail)
   	      nil
   	      nil
   	      nil)
   
	      
   ;; ------- Methods -------

   ;;determine whether to use existing inventory or a placeholder
   (:method (use_or_acquire ?item ?index ?count ?used)
	    use_inventory
	    ;; prec
	    (and (not (placeholder ?item))
		 (inventory_count ?index ?item ?count)
		 (call >= ?count ?used)
		 )
	    ;; subtasks
	    ((!!use_inventory_resource ?item ?index ?count ?used))

	    use_placeholder
	    ;; prec
	    ((placeholder ?item))
	    ((!acquire ?item ?used))

	    fail
	    nil
	    ((!fail))
	    )


   (:method (obtain_iron_sword)
	    sword_in_inventory_already
	    ;; prec
	    (and (type_iron_sword ?iron_sword)
		 (inventory_count ?index ?iron_sword ?sword_count)
		 (call >= ?sword_count 0))
	    ;; subtasks
	    ((!!have_item ?iron_sword))
	    
	    craft_item
	    ;; prec
	    (and (type_iron ?iron)
		 (or (not (placeholder ?iron))  ;; disjunction uses real inventory first
		     (placeholder ?iron))
		 (inventory_count ?iron_index ?iron ?iron_count)
		 (assign ?iron_used 2)
		 (call >= ?iron_count ?iron_used)

		 (type_wood ?wood)
		 (or (not (placeholder ?wood))  ;; disjunction uses real inventory first
		     (placeholder ?wood))
		 (inventory_count ?wood_index ?wood ?wood_count)
		 (assign ?wood_used 1)
		 (call >= ?wood_count ?wood_used)
		 (assign ?placeholder iron_sword_placeholder)
		 )
	    ;; subtasks
	    ((use_or_acquire ?iron ?iron_index ?iron_count ?iron_used)
	     (use_or_acquire ?wood ?wood_index ?wood_count ?wood_used)
	     (!craft_item ?placeholder)
	     (!!add_crafted_item_to_inventory ?placeholder)
	     )

	    fail
	    nil
	    ((!fail))
	    )
   ))


