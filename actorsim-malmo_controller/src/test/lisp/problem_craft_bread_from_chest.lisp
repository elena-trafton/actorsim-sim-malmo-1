(defproblem problem minecraft 
  (
   (type_wheat wheat)

   (contains chest1 0 wheat 1)
   (contains chest1 1 wheat 1)
   (contains chest1 2 wheat 1)
   
   (inventory_open_index 4)
   (inventory_open_index 5)
   (inventory_open_index 6)
   (inventory_open_index 7)
   (inventory_open_index 8)
   

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; Observations
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (type_container chest1)
    
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_wheat)
   (type_wheat placeholder_wheat)
   (type_crop placeholder_wheat)

   (placeholder placeholder_wheat_seeds)
   (type_seeds placeholder_wheat_seeds)
   (grows_from placeholder_wheat placeholder_wheat_seeds)
   (grows_from placeholder_wheat wheat_seeds)

   (placeholder placeholder_bone_meal)
   (type_bone_meal placeholder_bone_meal)

   (placeholder placeholder_farmland)
   (plantable placeholder_farmland)
   )
  ((obtain_bread))
)
