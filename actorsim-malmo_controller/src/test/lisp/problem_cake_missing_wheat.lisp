(defproblem problem minecraft 
  (
   (type_milk_bucket milk_bucket)	
;   (type_wheat wheat)
;;   (type_crop wheat)
   (type_sugar sugar)
   (type_egg egg)
   (type_bone_meal bone_meal)
;;   (type_seeds wheat_seeds)
;;   (grows_from wheat wheat_seeds)

   ;; case 1: already have cake - ignored

   ;; case 2: use existing inventory
   ;; case 3: remove one of the below inventory to use placeholders
   (inventory_count 0 sugar 3)
   ;; (inventory_count 1 wheat 1)
   (inventory_count 2 egg 1)
   (inventory_count 3 milk_bucket 3)
   (inventory_count 4 bone_meal 6)
;;   (inventory_count 5 wheet_seeds 1)

   (inventory_open_index 5)
   (inventory_open_index 6)
   (inventory_open_index 7)
   (inventory_open_index 8)
   (inventory_open_index 9)
   (inventory_open_index 10)
   (inventory_open_index 11)
   (inventory_open_index 12)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; Observations
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   (type_egg egg_1)
   (type_location location_egg_1)
   (entity_at egg_1 location_egg_1)

   (plantable farmland_001)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_chicken)
   (type_chicken placeholder_chicken)
   (type_animal placeholder_chicken)

   (placeholder placeholder_egg)
   (type_egg placeholder_egg)
   (found_near placeholder_egg placeholder_chicken)

   (placeholder placeholder_cow)
   (type_cow placeholder_cow)
   (type_animal placeholder_cow)

   (placeholder placeholder_milk_bucket)
   (type_milk_bucket placeholder_milk_bucket)
   (found_near placeholder_milk_bucket placeholder_cow)

   (placeholder placeholder_sugar)
   (type_sugar placeholder_sugar)

   (placeholder placeholder_wheat)
   (placeholder placeholder_wheat_seeds)
   (type_wheat placeholder_wheat)
   (type_crop placeholder_wheat)
   (type_seeds placeholder_wheat_seeds)
   (grows_from placeholder_wheat placeholder_wheat_seeds)

   (placeholder placeholder_bone_meal)
   (type_bone_meal placeholder_bone_meal)
   
   )
  ((obtain_cake))
)
