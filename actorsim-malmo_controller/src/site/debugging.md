## Debugging the Malmo Controller

It will often be the case that you will need to determine why a goal
priority is not working correctly.  Failures can be attributed to:
- timing issues by the controller
- flaws in the action being executed from `actions.py` 
- required state being missing from character.memory
- the planner failing to create a valid plan, which may be due to:
  - an incorrect lisp domain model in `domain_minecraft_full.lisp`
  - an incorrect lisp problem model created by in `plan_manager.py`
  - the JSHOP2 planner process not running
  - communications problems with the JSHOP2 planner instance
- the plan_manager incorrectly parsing a plan due to:
  - a missing mapping in `plan_manager.PRIMITIVE_NAME_TO_ACTION_MAP` 
- [add your own here!]

What follows below are various techniques that may help you debug your problem

### Tweaking the log output


### Using breakpoints


### Stopping the cognitive cycle during debugging


### Saving and running the planner model outside the controller

