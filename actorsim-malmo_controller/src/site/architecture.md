## ActorSim's Malmo Controller Architecture

This document describes the python-based controller that uses JSHOP2 to 
decompose some goals (e.g., obtain_bread) into primitive actions (e.g., 
`collect(wheat_seeds)`,  `move_to(farmland)`, `plant(wheet_seeds)`).


The aim of the controller is to move the character in Minecraft around to 
achieve goals. 
We frequently call this character Alex.
The controller consists of two loops: the CognitiveCycleWorker
(or just 'Worker') and the GoalManager.  
Each loop runs on its own thread.
These two loops work together to determine what to do.
The GoalManager selects the best goal (e.g., obtain bread) for the current
situation and enqueues actions (e.g., turn, move, plant wheet, use bonemeal) 
to achieve the current goal.
The Worker takes the next action, if any, and executes commands to  
complete the goal.
In the case of action failure, the worker invalidates the plan for that action
Together these two cycles determine the goal-directed behavior of the character. 

### Goal Priorities
Goals in the python controller are implemented in something we call a GoalPriority, 
which links an object of the agent with the appropriate means for achieving it.
These are found the file `goal_priority.py`.

When the character is initialized, a set of goal priorities are enqueued.
A priority that is earlier in the queue has a higher precedence. 
The GoalManager cycles through the priority queue in order until it finds 
one that is consistent, which it markes as active.
A priority remains active until one with higher precedence becomes consistent, 
at which point the active goal priority is switched.

A few priorities are handcoded to provide fallback behavior.
Handcoded priorities include:
- **TidyInventoryPriority** which reorders the items in inventory so that
  other actions will work properly.  This priority is usually run first.
- **ExploreForPriority** which moves around an area in a spiral to gather
  observations about what is within that area.  This priority is usually
  run if no other priorities have become active, since exploring can drive
  new observations that make other goals consistent.

Most of the the other priorities are based on the base class **PlanningPriority**
A PlanningPriority that is inconsistent (i.e., it has a missing or invalid plan) 
will attempt to construct a new plan using the JSHOP2 planner to decompose the
 objective into a sequence of primitive actions.  
If a valid plan is provided, its actions are executed. 
The primary activity of this controller is to obtain items, which is managed
by the **ObtainItemPriority**.
For this reason, the main task solved by JSHOP2 is `[(obtain_?item)]` where
`?item` is replaced with an item such as `bread` or `pumpkin_pie`.   

### Controller Actions and SHOP2 Primitive Operators
**We will be refactoring the actions hierarchy during the summer of 2020.**

Consistent priorities instantiate and enqueue actions for the character.
Actions are small programs that execute a set of commands to achieve an objective.
They correspond to the primitive operators of a task hierarchy in SHOP.
These can be found in the file  `actions.py`.
Many of these actions align with the operators listed in the SHOP2 domain file
 `src/main/lisp/domain_full_minecraft.lisp`, and a mapping between them 
 is provided in the file `plan_manager.py`:
 
-  ```python
    PRIMITIVE_NAME_TO_ACTION_MAP = {
        # ordered by the domain file
        'explore_for': ExploreFor,
        'craft_item': Craft,
        'monitor_near': MonitorNear,
        'gather': CollectDrop,
        'swap_one_from_chest': SwapWithChest,
        'collect_drop': CollectDrop,
        'collect_from_location': CollectFromLocation,
        'select': SelectItem,
        'move_near_target': MoveNearTarget,
        'move_near_entity': MoveNearEntity,
        'move_near_block_type': MoveNearBlockType,
        'look_at_block_type': LookAtBlockType,
        'look_at_crop': LookAtStaticTarget,
        'look_at_stem_crop': LookAtStem,
        'look_at_entity': LookAtEntity,
        'attack': Attack,
        'harvest': Attack,
        'break_using_look': BreakUsingLook,
        'use': Use,
        'pause': Pause,
        'pause_until_entity_update': PauseUntilEntityUpdate,
        'pause_until_entity_dead': PauseUntilEntityDead
        }
   ```


#### Placeholder (Skolem) Variables
Minecraft is an open world and objects that are needed to complete planning
are sometimes unavailable because they have not yet been found. 
Suppose the plan for obtain bread involves finding wheat_seeds and planting 
them in farmland.  
If farmland or wheat_seeds have not been observed, then no plan can be 
created, even for exploring to find them, because those objects do not exist
in the planning model.
To overcome this, Pavan introduced the idea of a *placeholder* item.
These show up in problem files with the prefix `placeholder_` and can be 
used to generate a valid plan.  
The controller knows that these are unavailable items and keeps a list of
placeholder items needed for each goal priority.
When one of those items appears, the goal priority knows to create a 
new plan.


#### Pausing for updates 
You may know that Minecraft sends an update tick at 20 hertz. 
But the observations provided by Malmo do not get updated every tick of the game,
and the controller may lag behind the actual game state.

Entities in the game include mobs (e.g., cows, chickens, zombies) as well as 
drops (e.g., items laying on the ground).  
Malmo only sends entity updates every so often, as specified in the mission 
specification.  
This means that any action that interacts with entities (e.g., picking up
a drop or killing a caw) must wait until after the next entity update
before checking completion.
For this reason, plans produced with entity interaction will have an action
to wait for an entity update.  

#### Skip Actions in the SHOP2 domain file 

Some operator primitives in the SHOP2 file do not correspond to executable actions.  
These are usually denoted with a bang (`!`) or double bang (`!!`)
These so-called *skip actions* are usually in place for bookkepping or debugging. 
Examples of these include:
- ```python
    SKIP_ACTION_MAP = {
        # ordered by the domain file
        'directive_complete_in_sequence',
        'directive_end_sequence',
        'consume_inventory',
        'increment_item_in_inventory',
        'add_new_item_to_inventory',
        'have_item',
        'fail',
        'fail_with_message',
        'success',
        'success_with_message',
        'completed_for',
        'remove_unopened'
    }
  ```

#### Empty preconditions and effects for SHOP2 operators

The astute reader may notice that many of the primitive operators in the SHOP2
file have empty (i.e., `nil`) preconditions, add effects, and delete effects.  
For example:
-  ```lisp
    ;; command executive to look at an entity
    (:operator (!look_at_entity ?entity)
	       nil nil nil)
   ``` 
This lack of preconditions and effects is due to the controller managing 
these details.  We only need SHOP2 to provide a plan skeleton for the 
controller to execute. 


### Commands
Malmo's command list is managed in the `Command` class found in `command.py`.
These include:
-  ```python
        self.move = 0.0  # type: float
        self.strafe = 0.0  # type: float
        self.pitch = 0.0  # type: float
        self.turn = 0.0  # type: float
        self._attack_count = 0  # type: int
        self.use = False  # type: bool
        self.jump = False  # type: bool
        self.crouch = False  # type: bool
        self.quit = False  # type: bool
   ```

The move, strafe, pitch, and turn commands all send a value between -1 and 1.
The remainder of the commands send either a 0 or 1.  
A special command, attack, is actually sent multiple times to ensure success, 
which is why it gets an attack count.

At the beginning of each cognitive cycle, this command structure is reset.
An executing command updates the values in this command structure.
At the end of the cognitive cycle, the values of the commmand structure
are converted into an appropriate command string that is sent to the Malmo 
environment for execution. 


## The Worker and GoalManager Threads

A complete view of thread interaction is found in [Full UML](uml/full_cognitive_loop.plantuml)

Or you can look at independent threads of this in:
  - [Init Sequence](uml/init.plantuml)
  - [CognitiveCycleWorker Sequence](uml/cognitiveCycleWorker.plantuml)
  - [GoalManager Sequence](uml/goal_reasoner.plantuml)
  
### The CognitiveCycleWorker
You can see a sequence diagram at [CognitiveCycleWorker Sequence](uml/cognitiveCycleWorker.plantuml).
The cognitive cycle consists of several phases:
  - Resetting the Command object so any previous commands will be cancelled.
    Malmo continues to execute commands until they are cancelled, so this 
    eliminates the situation where a move or turn action is cancelled.
  - Processing new observations from malmo
    - *receiving a new observation the malmo environment*.  An observation 
    consists of the observation `json_obs` in a json format, the rendered `image`, 
    and the reward.  We usually ignore the reward because we calculate our own.
    - processing the observation into a CharacterState (state) object
    - registering the state with memory
  - Attempting to execute any actions enqueued by the GoalManager
    - For each action, determine it is consistent 
    - If so, update the Command object based on the action
    - Only one action is active at a time to avoid multiple actions creating
      conflicting commands.  Thus, this process usually stops after the first
      action is successfully executed.
  - Collecting and sending the command string to the Malmo environment  
    
### The Python-based GoalManager
You can see the sequence diagram at [GoalManager Sequence](uml/goal_reasoner.plantuml)
The python-based GoalManager is responsible for enqueuing actions based
on goal priorities.
When the character initializes, it is provided a set of goal priorities to 
review on each loop of the GoalManager.  
The phases of the GoalManager include:
- waiting until a new state arrives in the Character's memory.  The Worker is 
  calls notify() 
- obtaining the latest state
- selecting the next active goal priority in a loop that:
  - generates a plan for the priority if needed, calling JSHOP2
  - getting the next action from the plan
  - enqueuing that action into the character
  - terminating if an action was successfully enqueued

If no goal priority succeeds, then no action is enqueued and the character
will not do anything.  
For this reason, there is often a fallback goal priority to explore.
The explore priority simply walks around, which updates the character's memory. 
Some observed items interest may enable a priority on a future cycle, which 
will update the plan for that priority and enqueue the appropriate actions.


## The Java-based GoalManager
**This section is under construction and will be updated when this goal manager
has been completed.**


