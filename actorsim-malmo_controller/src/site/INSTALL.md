# Installing the Malmo Connector


Please do your best to follow the exact setup, including directory naming,
listed below.  Doing so ensures easier team troubleshooting with a similar
setup across machines.  Many wasted hours occur with very minor setup differences.

Please: **Help us maintain and update this documentation!**
If you spot an error, correct it and commit the change on the `develop` branch.
Each section ends with a 'Common Issues' subsection, look there first if you get stuck. 
Also, this is where you can add notes on details that tripped you up.
If you are in doubt about updating the documentation ask your install guru for guidance. 


## Prerequesites and Platforms

The steps to set up this connector for ActorSim involve:
- (Optional) Installing a supercharged shell environment
- Installing Java OpenJDK 8.0 for your system
- Installing Python 3.6 for your system
- Installing Python's virtualenv and virtualenvwrapper for your system
- Configuring a virtual environment in which to run Malmo
- Installing and testing Microsoft's Malmo
- Cloning the source directories
- Installing the IntelliJ IDEA, into which you will also install: 
  - The ActorSim Python Controller
  - As needed, ActorSim's Shop planner connector
  - As needed, ActorSim's Java goal management suite

We have tested this setup on Ubuntu 16.04LTS.  Please understand we can offer
very little support for other installation platforms.

For those on Windows 10, we have tested the WSL with Ubuntu 16.04.  Instructions 
for installing on WSL are identical except where noted below. 

We have also tested this on MacOSX 10.14.6 with homebrew, however we have 
noticed timing issues in the python controller with this setup. Instructions
are identical/readily adaptable except where noted below.

#### Common Issues
- I don't want to use Ubuntu16 or IntelliJ.  Can I use X instead?  (where X is some other IDE).
  - In short, not really.  We have such limited time as it is that 
    supporting one Platform/IDE is challenging enough. Ubuntu 16.04
    is in the middle of its lifecycle and fairly stable. IntelliJ 
    products support a vibrant ecosystem of plugins that will
    ease your transition.
  - However, we have recently switched to a Maven build structure, which should
    make it much easier for people to install in any IDE.  Just be aware, 
    we'll probably ask you to reproduce any problem in the standard setup first.


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## (Optional) System: Install Zsh
Although it is not required, Mak highly recommends the zsh environment with a variety
of excellent plugins including oh-my-zsh.  If you're interested, ask Mak for a demo 
of zsh setup and for his config files. 

[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## System: Install Java OpenJDK 8.0

**Note:** There is a bug with Malmo and openjdk-1.8.0-242 that will result in malmo
being unable to start Minecraft. This is shown with  an error saying "Execution failed
for task ':runClient'.". For more information check out ticket ACTORSIM-339 in jira.


#### Ubuntu Setup
If you expect to be installing many varieties of Java, you may want to install
via SDKMan: https://sdkman.io/

Otherwise, simply follow the instructions for installing the latest version of
OpenJDK 8.0 on Ubuntu.  


#### Mac Setup
On a Mac, you can install this using homebrew:
```    
brew cask install java8
```

Then check to make sure that it is the right version (see above). If not, 
let one of us know and we'll dig up the specific formula or it.

#### Common issues 
[[Place your common issues here]]



[comment]: =================================================================================
[comment]: =================================================================================
[comment]: =================================================================================
## System: Install Python 3.6

#### Ubuntu Setup
For Python3.6, on older machines you may need to install the ppa
You also need to install numpy, image, and future.
 
```
sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt update
sudo apt install python3.6
sudo apt install python3-pip
pip3 install numpy image future
```

#### Mac Setup
On Macs, if you don't already have python3.6 installed, come talk to
one of us. Homebrew does not make it easy to install earlier versions
of python. 


##### Common issues 



[comment]: =================================================================================
[comment]: =================================================================================
[comment]: =================================================================================
## System: Install and configure python's virtual environment
IntelliJ IDEs will set up a default virtual environment and that probably isn't 
what you want.  We highly recommended to install and use virtual environments (virtualenv) 
for _all_ python development.
To do this, install virtualenv and virtualenvwrapper
```
sudo apt install virtualenv virtualenvwrapper
```

If you haven't used virtualenv before, a good tutorial is available at:
https://www.bogotobogo.com/python/python_virtualenv_virtualenvwrapper.php
or https://virtualenvwrapper.readthedocs.io/en/latest/

To complete the common setup, please do the following.

#### Setup your base virtualenv directory:
```
cd ~
mkdir virtualenv
```

#### Update your shell configuration to load virtualenvwrapper
Add the following config to your shell configuration
(via the .profile, .bashrc, or .zshrc files).  For bash and ZSH, 
the following will add the correct aliases; adapt yours appropriately.

```
export WORKON_HOME=$HOME/virtualenv
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
```

[[please add your shell if it isn't listed above!]]  

##### Common issues 
[[Place your common issues here]]



[comment]: =================================================================================
[comment]: =================================================================================
[comment]: =================================================================================
## Install Malmo into a virtualenv 

### Create a virtual environment

First, create the virtual environment we will use for malmo
```
cd ~/virtualenv
virtualenv -p python3.6 malmo-latest
```

Start the virtual environment.
Note the 'workon' command will only work if virtualenvwrapper is installed correctly.
```
workon malmo-latest  
cd ~/virtualenv/malmo-latest
```

### Download Malmo

 - This code is tested with Malmo-0.37.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5.zip
   Malmo 0.37.0 seems to work fine with Python 3.6 even though the library was purportedly built for Python 3.5.
 - Follow the instructions for installing dependencies for Malmo at <https://github.com/Microsoft/malmo>  
 - Download the prebuilt version at: https://github.com/Microsoft/malmo/releases
 - Place the zip file in ~/virtualenv/malmo-latest
 
 - This was also tested with Malmo 0.37 for Macs using both Python 3.6 and Python 3.7 install. Both seemed to work.

### Unzip the file  
 - this should create a directory `~/virtualenv/malmo-latest/Malmo-0.37.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5/`
 - From now on, we will refer to this as the MALMO directory
 - In other words MALMO = `~/virtualenv/malmo-latest/Malmo-0.37.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5/`

### Create a symbolic links
Setting up some symbolic links now will help from typing errors in the 
IDE setup and enable easier Malmo upgrades.

In case you don't know, you can use tab to complete long directory names.
```shell script
cd ~/virtualenv/malmo-latest
ln -s Malmo-0.37.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5/ Malmo
ln -s Malmo-0.37.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5/Minecraft
ln -s Malmo-0.37.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5/Python_Examples
```
   
### Test the base Malmo installation

You will always need to run a 'client-GUI' and a character script
for Malmo programs.  The 'client-GUI' is really a server with a
client window, which is usually all you need to play Minecraft. 
(In case you're interested, you can run a standalone server and 
do what's called a direct connect to play that server.  It's how 
realms and other servers work.)  So that client-GUI actually
starts a server in the background and to run a character script 
you need to start the server/client.

To test your Malmo installation, start Malmo in one terminal.
This can take a while (10-20 minutes) to build the first time!
```
workon malmo-latest
cd ~/virtualenv/malmo-latest/Minecraft
./launchClient.sh
```
Minecraft should eventually start.  On the first run, this can take a while to
compile everything.

Once it builds, in another terminal, run:
(remember you can use tab to auto-complete long directories!)
```
workon malmo-latest
export MALMO_XSD_PATH=/home/[username]/virtualenv/malmo-latest/Malmo/Schemas
cd ~/virtualenv/malmo-latest/Python_Examples
python mob_zoo.py
```
You can swap the last line to run a few other examples. Some of them can be very
instructive about how to use features of Malmo.

#### (Optional) Going further with Malmo
Try to run through the tutorial to get a sense of how Malmo works.

##### Common issues 
- Sometimes a system lacks specific dependencies for Malmo
  - Confirm you have installed everything listed at https://github.com/Microsoft/malmo  
- ```No module named ImageTk```
  - Install image in your virtual environment
    Image is the successor to image-tk that uses pillow instead of PIL
    ```
    workon malmo-latest
    pip install image  
    ``` 
- You may see errors complaining about JAVA_HOME or PYTHON_PATH
  - If you just started a new terminal, remember to start virtualenv with ```workon malmo-latest``` 
  - Modify the paths in the appropriate ~/virtualenv/malmo-latest/bin/activate script:
    ```
    JAVA_HOME="/usr/lib/jvm/latest"
    export JAVA_HOME
    export PYTHONPATH "$PYTHONPATH:/home/[username]/virtualenv/malmo-latest/Python_Examples"
    ```
- You see an error about MALMO_XSD_PATH
  - Did you forget to run the export command listed above?
  - Alternatively, add the following to the appropriate ~/virtualenv/malmo-latest/bin/activate script:
    ```
    MALMO_XSD_PATH="/home/.../virtualenv/malmo/malmo-latest/Malmo/Schemas"
    export MALMO_XSD_PATH
    ```

- You see an error about how execution failed for the task.
  - Did you remember to install the correct version of Java 8, as mentioned above?

- [[Place your common issues here]]


# Installing The ActorSim Controller 
Now that you have a working version of the Malmo code, it's time to install 
ActorSim's Malmo Controller.  

[comment]: =================================================================================
[comment]: =================================================================================
[comment]: =================================================================================
## Setting up the ActorSim Malmo connector

Installing the ActorSim Controller involves:
  - Creating your base directory
  - Cloning the Actorsim Malmo connector code
  - For the python only controller:
    - install an IntelliJ IDE
    - install and configure the IDE to run the code
    - test the python controller
  - For using the JSHOP2 Planner, you will also need to 
    - install ActorSim's JSHOP2 planner connector
    - configure the IDE to run the planner
    - test the python controller access to SHOP2  
  - For the python controller to communicate with ActorSim's Goal Management
    - install IntelliJ IDEA Community Edition
    - test the python controller
    - install the ActorSim MongoDB Interop, written in Java
    - test the Java interop

### Create your Malmo base directory (MALMO_BASE_DIR)

To simplify and unify our development environments across many projects
 **please** clone your repos into these directories and names.  
 
Let MALMO_BASE_DIR represent the root directory for your Malmo project.  
This directory can be anywhere you like, but keep in mind that you may want
to have a short path because you will be required to type _absolute_ paths
 during the IDE setup.  Shown below are the recommended paths.
 
![Recommended Directory Structure](_images/RecommendedDirectoryStructure.png)

which equate to the following table: 

| VARIABLE NAME     | Local Directory           | Description  |
| :------------- |:------------- | :-----|
| MALMO_BASE_DIR          | `~/git-workspace/malmo`  | Your base install directory |
| MALMO_HOME   | `MALMO_BASE_DIR/actorsim-sim-malmo-git` | The Malmo Connector code |
| ACTORSIM_HOME | `MALMO_BASE_DIR/actorsim-core-git` | ActorSim's core goal refinement library |
| SHOP2_HOME  | `MALMO_BASE_DIR/actorsim-planner-shop2-git` | ActorSim's SHOP2 connector |
Henceforth, we will refer to these directories by the variable name and
 you will need to replace them as appropriate.

Set up a common workspace for this connector
```shell script
cd ~
mkdir -p MALMO_BASE_DIR
```

### Clone the repo(s) into your workspace  
Your git repository access URL will differ based on your credentials and will be
emailed to you by a development lead.

*Note the use of the suffix `-git` for local repositories.  This is a handy way
to visually detect local repos on your system, since the .git directory 
is often hidden!  (Git best practices preclude using a `.git` suffix
 because that is reserved for a bare repository on a remote server.)*

Clone and checkout the develop branch of the malmo connector
- ```shell script
  cd MALMO_BASE_DIR
  git clone [URL.. actorsim-sim-malmo.git]  actorsim-sim-malmo-git 
  cd  actorsim-sim-malmo-git
  git checkout develop #in zsh with the git plugin: gcd
  ```

If you will be using the JSHOP planner, clone its repo and checkout the develop branch.
- ```shell script
  cd MALMO_BASE_DIR
  git clone [URL.. actorsim-planner-shop2.git] actorsim-planner-shop2-git
  cd actorsim-core-git
  git checkout develop   #in zsh with the git plugin: gcd
  ```

If you will be using the ActorSim's core library, clone its repo.
- ```shell script
  cd MALMO_BASE_DIR
  git clone [URL.. actorsim-core.git] actorsim-core-git
  cd actorsim-core-git
  git checkout develop   #in zsh with the git plugin: gcd
  ```

Install an IDE
--------------
Now you're ready to install an IDE.
We support two options: 
1. **(Preferred)** IntelliJ IDEA with a python plugin that allows for running
 java an python code simultaneously.  This is the best in stall if: (1) you
 already do Java programming; (2) you expect to try or develop for ActorSim's
  JSHOP2 connector; or (3)  you expect to use ActorSim's core library for
  goal management (i.e., the Java interop layer).
 For this option, see [INSTALL_IDEA.md](INSTALL_IDEA.md).
2. **(Deprecated)** IntelliJ Pycharm if you're just wanting to use the python 
controller without using the java libraries in ActorSim.  For this option, 
see [INSTALL_Pycharm.md](INSTALL_Pycharm.md).

