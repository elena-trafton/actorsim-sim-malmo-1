

=====================
Character Control
=====================

The character has a threshold for determining if it is close to the target:
  - collect_threshold
  - hit_threshold
  - farming_threshold

------------------------
Movement and looking
------------------------

- moveToTarget(target, threshold, min_speed, max_speed)
  - moves within threshold of target using move, strafe, and look commands
- turnToTarget(target, threshold, min_speed, max_speed)
  - turns to target using look commands
- collect(target)
  - moves to the target to collect the item

------------------------
Inventory
------------------------

- move_item(item, position)
  - moves the item to the hotbar position
- select_hotbar(position)
  - makes the item in position active in the right hand
- equip(armor_item)
  - equips the item, which must be armor or a shield
- craft_without_table(item)
  - crafts the item if resources are available, ignoring whether a table is nearby
- craft_with_table 
 
------------------------
Clicking
------------------------
- harvest(target)
  - moves to target and right clicks the top of target
- plant(seed_type, target)
  - moves to target, confirms the target is dirt, tills it, and plants seed
- hit(target, [item_to_use])
  - moves to target and hits it when within range


=====================
Observations
=====================

- Need a way to track what was seen in the world
  - entities
  - blocks of interest such as wood, crafting table, chests
  - the terrain
  - 

- Need a way for companion to "tell" character about something in the world


=====================
Decision Making
=====================

Need a way to link missions with actions

