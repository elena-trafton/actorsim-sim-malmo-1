import traceback

from loggers import MalmoLoggers

logger = MalmoLoggers.malmo_root_logger


class MalmoOptions:
    """An optional class that can be overridden by a developer for debug behavior."""
    INSTANCE = None

    @staticmethod
    def initialize_options(options_filename = "custom.txt"):
        options_instance = MalmoOptions(custom_filename=options_filename)
        MalmoOptions.INSTANCE = options_instance

    @staticmethod
    def get_instance() -> 'MalmoOptions':
        if MalmoOptions.INSTANCE == None:
            MalmoOptions.initialize_options()
        return MalmoOptions.INSTANCE

    def __init__(self, custom_filename="custom.txt"):
        #################################################################################
        # These variables are default vaules and should NOT be changed
        #
        # Instead, create an INI file, the default is custom.txt, to store custom values.
        # An example file called custom.txt.example can be copied and modified.
        #
        # Note: custom.txt is ignored by git should not be committed!
        #################################################################################
        self.custom_filename = custom_filename

        self.shop_description_fileout = None

        self._customize()

    def _customize(self):
        try:
            with open(self.custom_filename) as fp:
                logger.info("Processing custom option file '{}'".format(self.custom_filename))
                for line in fp:
                    logger.info("  processing line: {}".format(line))
                    if line.startswith("#"):
                        continue
                    if line == "":
                        continue
                    tokens = line.split("=")
                    key = tokens[0]
                    value = tokens[1]
                    logger.info("  token '{}' has value '{}'".format(key, value))
                    if hasattr(self, key):
                        setattr(self, key, value)
        except FileNotFoundError as fnfe:
            import os
            cwd = os.getcwd()
            logger.info("File '{}' not found in cwd '{}'".format(self.custom_filename, cwd))
        except Exception as e:
            logger.info("exception occurred:{}".format(e))
            logger.info("{}".format(traceback.format_exc()))

