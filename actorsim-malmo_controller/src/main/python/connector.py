import MalmoPython
import os
import sys
import time
import threading

from utils.LogManager import LogManager

logger = LogManager.get_logger('actorsim.malmo.connector')
obs_logger = LogManager.get_logger('actorsim.malmo.character.obs')

class MinecraftConnector():
    # TODO create a MinecraftConnectorOptions class; move to it: ports, sleep_between_restart, max_retries, etc.
    # TODO remove ports parameter from character.start_mission using options class instead
    ports = [10001]
    flush_immediately = False
    max_retries = 10

    def __init__(self):
        self.sleep_between_start_retries_in_seconds = 2.5

    def initialize_minecraft(self):
        # set up python to flush print output immediately
        if self.flush_immediately is False:
            if sys.version_info[0] == 2:
                sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
            else:
                import functools
                global print
                print = functools.partial(print, flush=True)
            self.flush_immediately = True

        self.agent_host = MalmoPython.AgentHost()
        logger.debug("Successfully set agent host")
        try:
            self.agent_host.parse( sys.argv )
        except RuntimeError as e:
            print('ERROR:',e)
            print(self.agent_host.getUsage())
            exit(1)
        if self.agent_host.receivedArgument("help"):
            print(self.agent_host.getUsage())
            exit(0)

    def start_sim_from_mission_file(self, mission_file="mission.xml", experimentId = "0", role=0):
        with open(mission_file, 'r') as f:
            print("Loading mission from %s" % mission_file)
            self.mission_xml = f.read()
        self.start_sim(self.mission_xml, experimentId, role)

    def start_sim(self, mission_xml, experimentId, role):
        logger.debug("{} Attempting to start mission".format(threading.current_thread().name))
        self.my_mission_record = MalmoPython.MissionRecordSpec()
        self.mission = MalmoPython.MissionSpec(mission_xml, True)
        for retry in range(self.max_retries):
            logger.debug("{} Retry {} of {}".format(threading.current_thread().name, retry, self.max_retries))
            client_pool = MalmoPython.ClientPool()
            for p in self.ports:
                logger.info("{} Expect minecraft client on port:{}".format(threading.current_thread().name, p))
                client_pool.add( MalmoPython.ClientInfo('127.0.0.1', p))
            #print "Mission pool", x, client_pool
            try:
                logger.debug("  {} attempting to start mission with experimentID:{}".format(threading.current_thread().name, experimentId))
                self.agent_host.startMission( self.mission, client_pool, self.my_mission_record, role, experimentId)
                break
            except RuntimeError as e:
                if isinstance(e, MalmoPython.MissionException):
                    logger.error("{} Error starting mission: {} - {}".format(threading.current_thread().name, e.details.errorCode, e.details.message))
                else:
                    logger.error("{} Error starting mission: {}".format(threading.current_thread().name, e))
                logger.exception(e)

                if retry == self.max_retries - 1:
                    exit(1)
                else:
                    logger.debug("Mission did not start waiting {} seconds".format(self.sleep_between_start_retries_in_seconds))
                    time.sleep(2.5)

        logger.debug("Finished starting mission.  Waiting for mission to start in server...")

        self.world_state = self.agent_host.getWorldState()
        while not self.world_state.has_mission_begun:
            sys.stdout.write(".")
            time.sleep(0.1)
            self.world_state = self.agent_host.getWorldState()
            for error in self.world_state.errors:
                logger.error("Error occurred:'{}'".format(error.text))
            logger.trace("Has mission begun:{}".format(self.world_state.has_mission_begun))



    def get_observation(self, wait_time_between_checks_in_seconds=0.005):
        obs_logger.debug("obtaining observation")
        obvsText = 0
        image = None
        latest_reward = 0

        waiting_on_observation = True
        while waiting_on_observation:
            time.sleep(wait_time_between_checks_in_seconds)
            self.world_state = self.agent_host.getWorldState()
            for error in self.world_state.errors:
                logger.error("Error: {}".format(error.text))
            for reward in self.world_state.rewards:
                latest_reward = reward.getValue()
            if self.world_state.is_mission_running:
                if len(self.world_state.observations)>0:
                    obvsText = self.world_state.observations[-1].text
                    waiting_on_observation = False
                if self.world_state.number_of_video_frames_since_last_state > 0:
                    image = self.world_state.video_frames[0].pixels

            if not self.world_state.is_mission_running:
                break

        return obvsText, image, latest_reward
