import numpy as np

from loggers import MalmoLoggers

logger = MalmoLoggers.malmo_root_logger


class Location():
    x = 0  # type: float
    y = 0  # type: float
    z = 0  # type: float

    def __init__(self, x: float = 0.0, y: float = 0.0, z: float = 0.0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "({:.2f}, {:.2f}, {:.2f})".format(self.x, self.y, self.z)

    def toJSON(self):
        return dict(x=self.x, y=self.y, z=self.z)

    def int_key_str(self):
        return "{}_{}_{}".format(int(self.x), int(self.y), int(self.z))

    def set_from_key_str(self, key_str: str):
        tokens = key_str.split("_")
        self.x = float(tokens[0])
        self.y = float(tokens[1])
        self.z = float(tokens[2])
        return self

    def copy(self):
        location_copy = Location()
        location_copy.set(self.x, self.y, self.z)
        return location_copy

    def update(self, other: 'Location'):
        self.set(other.x, other.y, other.z)

    def intersects(self, min_loc: 'Location', max_loc: 'Location'):
        """Returns true if this location is within the bounding box between min and max.
           An invariant of this method is that min <= max for every dimension."""
        min_is_bigger = False
        if min_loc.x > max_loc.x:
            min_is_bigger = True
        if min_loc.y > max_loc.y:
            min_is_bigger = True
        if min_loc.z > max_loc.z:
            min_is_bigger = True

        if min_is_bigger:
            logger.warning("Cannot check intersection for {} when min {} is larger than max {}".format(self, min_loc, max_loc))
            return False

        is_intersecting = False
        if min_loc.x <= self.x and self.x <= max_loc.x:
            if min_loc.y <= self.y and self.y <= max_loc.y:
                if min_loc.z <= self.z and self.z <= max_loc.z:
                    is_intersecting = True
        return is_intersecting

    def dist(self, other : 'Location' ):
        delta_x = other.x - self.x
        delta_y = other.y - self.y
        delta_z = other.z - self.z
        return np.sqrt((delta_x * delta_x) + (delta_y * delta_y) + (delta_z * delta_z))

    def set(self, x: float = 0, y: float =0, z: float = 0):
        self.x = x
        self.y = y
        self.z = z

    def as_array(self):
        return np.array([self.x, self.y, self.z])

    def as_array_xz(self):
        return np.array([self.x, self.z])


class NullLocation(Location):
    """A Null object pattern for use in client code."""
    def __init__(self):
        Location.__init__(self, 10000, 10000, 10000)

    def __repr__(self):
        return "NULL_LOCATION"


NULL_LOCATION = NullLocation()


class ObjectLocation(Location):
    def __init__(self, x, y, z, object_id, mc_type):
        super(ObjectLocation, self).__init__(x, y, z)
        self.id = object_id
        self.mc_type = mc_type

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def toJSON(self):
        d = super(ObjectLocation, self).toJSON()
        d["id"] = self.id
        d["mc_type"] = self.mc_type
        return d

class BlockLocation(ObjectLocation):
    def __init__(self, x: float = 0, y: float = 0, z: float = 0, mc_type ="", colour=""):
        super(BlockLocation, self).__init__(x, y, z, self.int_key_str(), mc_type)
        self.colour = colour
        self.id = self.int_key_str()

    def set(self, x: float = 0, y: float =0, z: float = 0):
        super(BlockLocation, self).set(x, y, z)
        self.id = self.int_key_str()

    def __repr__(self):
        type_str = "???"
        if self.mc_type != "":
            type_str = "{}@".format(self.mc_type)
        return "{}({:.2f}, {:.2f}, {:.2f}) id:{}".format(type_str, self.x, self.y, self.z, self.id)

    def toJSON(self):
        d = super(BlockLocation, self).toJSON()
        d["id"] = self.id
        d["colour"] = self.colour
        return d

    def copy(self):
        location = BlockLocation()
        location.set(self.x, self.y, self.z)
        location.mc_type = self.mc_type
        location.colour = self.colour
        location.id = self.id
        return location

    def update(self, other: 'BlockLocation'):
        self.set(other.x, other.y, other.z)
        self.mc_type = other.mc_type
        self.colour = other.colour


class EntityLocation(ObjectLocation):
    def __init__(self, x: float = 0, y: float = 0, z: float = 0, pitch : float = 0.0, yaw : float = 0.0, object_id ="", mc_type =""):
        super(EntityLocation, self).__init__(x, y, z, object_id, mc_type)
        self.pitch = pitch  # type: float
        self.yaw = yaw  # type: float
        self.variant = ""
        self.quantity = ""

    def _get_location_str(self, type_str):
        return "{}({:.2f}, {:.2f}, {:.2f}) p:{:.3f} y:{:.3f} id:{}".format(type_str, self.x, self.y, self.z, self.pitch, self.yaw, self.id)

    def __repr__(self):
        type_str = "???"
        if self.mc_type != "":
            type_str = "{}@".format(self.mc_type)
        return self._get_location_str(type_str)

    def toJSON(self):
        d = super(EntityLocation, self).toJSON()
        d["pitch"] = self.pitch
        d["yaw"] = self.yaw
        d["variant"] = self.variant
        d["quantity"] = self.quantity
        return d

    def copy(self):
        location = EntityLocation()
        self._copy_internal(location)
        return location

    def _copy_internal(self, location):
        location.set(self.x, self.y, self.z)
        location.mc_type = self.mc_type
        location.pitch = self.pitch
        location.yaw = self.yaw
        location.variant = self.variant
        location.quantity = self.quantity
        location.id = self.id

    def update(self, other: 'EntityLocation'):
        self.set(other.x, other.y, other.z)
        if isinstance(other, EntityLocation):
            self.mc_type = other.mc_type
            self.pitch = other.pitch
            self.yaw = other.yaw
            self.variant = other.variant
            self.quantity = other.quantity
            self.id = other.id


class MobLocation(EntityLocation):
    def __init__(self, x: float = 0, y: float = 0, z: float = 0, pitch : float = 0.0, yaw : float = 0.0, mc_type ="", uuid ="", ):
        super(MobLocation, self).__init__(x, y, z, pitch, yaw, object_id=uuid, mc_type=mc_type)

    def __repr__(self):
        type_str = "???"
        if self.mc_type != "":
            type_str = "{}_Mob@".format(self.mc_type)
        return self._get_location_str(type_str)

    def copy(self):
        location = MobLocation()
        self._copy_internal(location)
        return location

    def get_short_entity_name(self):
        shortened_uuid = self.id.replace("_", "").replace("-", "")[0:10]
        return "{}_{}".format(self.mc_type, shortened_uuid)

    def convert_to_entity(self):
        ent_dict = {}
        ent_dict['name'] = self.mc_type
        ent_dict['x'] = self.x
        ent_dict['y'] = self.y
        ent_dict['z'] = self.z
        ent_dict['pitch'] = self.pitch
        ent_dict['yaw'] = self.yaw

        from entity import Entity
        entity = Entity(ent_dict)
        return entity


class DropLocation(EntityLocation):
    def __init__(self, x: float = 0, y: float = 0, z: float = 0, pitch : float = 0.0, yaw : float = 0.0, mc_type ="", uuid ="", ):
        super(DropLocation, self).__init__(x, y, z, object_id=uuid, mc_type=mc_type)

    def __repr__(self):
        type_str = "???"
        if self.mc_type != "":
            type_str = "{}_Drop@".format(self.mc_type)
        return self._get_location_str(type_str)

    def copy(self):
        location = DropLocation()
        self._copy_internal(location)
        return location

