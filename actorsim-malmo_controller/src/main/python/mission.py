import random

import character
from location import EntityLocation

class ObservationGridDetails():
    """A class to unify various observation grids possible.
       For ObservationFromNearbyEntities, only [x, y, z]_range apply using relative coordinates.

       For ObservationFromGrid, the remaining variables specify the grid details.

       Methods in this class return 'self' so that they can be chained for simpler initialization:

         alex_grid = ObservationGrid("Alex3x3").start(-1, -1, -1).size(3, 3, 3).relative()
         companion_grid = ObservationGrid("CompanionWholeMap").start(20, 2, 20).size(3, 3, 3).absolute()
       """
    def __init__(self, name):
        self.name = name
        self.x_start = -1
        self.y_start = -1
        self.z_start = -1
        self.x_range = 3
        self.y_range = 3
        self.z_range = 3
        self.update_frequency = 7
        self.use_absolute_coordinates = False

    def start(self, x, y, z):
        self.x_start = x
        self.y_start = y
        self.z_start = z
        return self

    def size(self, x_range, y_range, z_range):
        self.x_range = x_range
        self.y_range = y_range
        self.z_range = z_range
        return self

    def absolute(self):
        self.use_absolute_coordinates = True
        return self

    def relative(self):
        self.use_absolute_coordinates = False
        return self

    def update_tick(self, game_ticks):
        self.update_frequency = game_ticks
        return self

class MissionOptions():
    def __init__(self):
        self.decorator_read_filename = ""
        self.decorator_write_filename = ""



class Mission():

    def __init__(self, summary_string, seed=0, options: MissionOptions = MissionOptions()):
        self.options = options
        self.summary_string = summary_string #used in the mission spec

        self.prioritize_offscreen_rendering = False
        self.mc_start_time = 1000
        self.generator_str = "2;7,15x42;1"
        self.time_in_ms = 30000
        self.force_reset = True
        self.ms_per_tick = 50 #game usually runs at 20 ticks / second or 50 milliseconds per tick

        self.character_names = ["Alex"] # this allows single-agent missions to not break
        self.decorators = []

        self.inventories = {"Alex": [], "Companion": []}
        self.mode = "Survival"
        self.start = {"Alex": EntityLocation(0.5, 16, 0.5, pitch=0, yaw=90),
                      "Companion": EntityLocation(0.5, 16, 0.5, pitch=0, yaw=90)}

        self.use_absolute_grids = True  # used in mission spec
        alex_grid = ObservationGridDetails("Alex3x3").start(-1, -1, -1).size(3, 3, 3).relative()
        companion_grid = ObservationGridDetails("CompanionWholeMap").start(20, 2, 20).size(3, 3, 3).absolute()
        self.observation_grids = {alex_grid.name : alex_grid,
                                  companion_grid.name : companion_grid}
        self.char_to_grids = {"Alex": [alex_grid.name],
                              "Companion": [companion_grid.name]}

        alex_entities = ObservationGridDetails("AlexEntities").size(20, 20, 20)
        companion_entities = ObservationGridDetails("CompanionEntities").size(-127, 3, 127) #TODO mak wonders: is -127 correct?
        self.ents_to_details = {alex_entities.name: alex_entities,
                                companion_entities.name: companion_entities} # TODO does Companion get everything?
        self.char_to_ents = {"Alex": [alex_entities.name],
                             "Companion": [companion_entities.name]}

        self.observation_from_full_stats = True
        self.observation_from_full_inventory = True
        self.observation_from_hot_bar = False  # duplicates full inventory except size
        self.observation_from_recent_commands = False
        self.observation_from_ray = True
        self.simple_craft_commands = True

        self.video_width = 1600
        self.video_height = 800

        self.spec = ""
        self.decorator_str = ""

        self.is_flat_world = True
        self.seed = "BattleShip"  # zero is the same as not setting a key
        self.destroy_after_use = True

    def generate_mission_spec(self):
        self.spec = ""
        self.spec += self.generate_intro()
        self.spec += self.generate_server_section()
        for char in self.character_names:
            self.spec += self.generate_agent_section(char)  # need a separate agent section for each character
        self.spec += self.generate_outro()


    def generate_intro(self):
        intro = \
            '<?xml version="1.0" encoding="UTF-8" ?>' + "\n" + \
            '<Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' + "\n" + \
            '<About> <Summary> {} </Summary></About>'.format(self.summary_string) + "\n" + \
            '<ModSettings>' + "\n" + \
            '    <MsPerTick>{}</MsPerTick>'.format(self.ms_per_tick) + "\n" + \
            '    <PrioritiseOffscreenRendering> {}</PrioritiseOffscreenRendering>'\
                .format(str(self.prioritize_offscreen_rendering).lower()) + "\n" + \
            '</ModSettings>' + "\n"
        return intro

    def generate_outro(self):
        outro = \
            '</Mission>'
        return outro

    def write_dec_str(self):
        fo = open("{}".format(self.options.decorator_write_filename), "w")
        fo.write("{}".format(self.decorator_str), )
        fo.close()

    def read_dec_str(self):
        fo = open("{}".format(self.options.decorator_read_filename), "r")
        self.decorator_str = fo.read()
        fo.close()

    def generate_server_section(self):
        if self.options.decorator_read_filename != "":
            self.read_dec_str()
        else:
            self.decorator_str = ""
            if len(self.decorators) > 0:
                self.decorator_str = '  <DrawingDecorator>' + "\n"
                for decorator in self.decorators:
                    self.decorator_str += decorator + "\n"
                self.decorator_str += '  </DrawingDecorator>' + "\n"
        if self.options.decorator_write_filename != "":
            self.write_dec_str()

        server = \
            '<ServerSection>' + "\n" + \
            '  <ServerInitialConditions>' + "\n" + \
            '    <Time><StartTime>{}</StartTime></Time>'.format(self.mc_start_time) + "\n" + \
            '    <Weather>clear</Weather>' + "\n" + \
            '  </ServerInitialConditions>' + "\n" + \
            '  <ServerHandlers>' + "\n" + \
            '  <!-- see http://www.minecraft101.net/superflat/ for details -->' + "\n"
        if self.is_flat_world:
            server += '  <FlatWorldGenerator generatorString="{}" forceReset="{}"/>'.format(self.generator_str, str(self.force_reset).lower()) + "\n"
        else:
            server += '  <DefaultWorldGenerator seed="{}" forceReset="{}" destroyAfterUse="{}"/>'.format(self.seed, str(self.force_reset).lower(), str(self.destroy_after_use).lower())

        server += '{}'.format(self.decorator_str) + \
            '{}'.format(self.decorator_str) + \
            '  <ServerQuitFromTimeUp timeLimitMs="{}"/>'.format(self.time_in_ms) + "\n" + \
            '  <ServerQuitWhenAnyAgentFinishes/>' + "\n" + \
            '  </ServerHandlers>' + "\n" + \
            '</ServerSection>'
        return server

    def generate_agent_section(self, character_name="Alex"):
        start = self.start[character_name]
        character_start = '    <Placement x="{}" y="{}" z="{}" pitch="{}" yaw="{}"/> \n' \
            .format(start.x, start.y, start.z,
                    start.pitch, start.yaw)

        observation_str = ""
        if self.observation_from_full_stats:
            observation_str += '    <ObservationFromFullStats/>' + "\n"
        if self.observation_from_full_inventory:
            observation_str += '    <ObservationFromFullInventory/>' + "\n"
        if self.observation_from_hot_bar:
            observation_str += '    <ObservationFromHotBar/>' + "\n"
        if self.observation_from_ray:
            observation_str += '    <ObservationFromRay/>' + "\n"
        for grid_name in self.char_to_grids[character_name]:
            details = self.observation_grids[grid_name] #type: ObservationGridDetails
            min_x = details.x_start
            min_y = details.y_start
            min_z = details.z_start
            max_x = details.x_start + (details.x_range - 1)
            max_y = details.y_start + (details.y_range - 1)
            max_z = details.z_start + (details.z_range - 1)
            observation_str += '    <ObservationFromGrid>' + "\n"
            observation_str += '      <Grid name = "{}"'.format(grid_name)
            if self.use_absolute_grids and details.use_absolute_coordinates:
                observation_str += ' absoluteCoords="true"'
            observation_str += ">\n"
            observation_str += '        <min x= "{}" y="{}" z="{}"/>'.format(min_x, min_y, min_z) + "\n"
            observation_str += '        <max x= "{}" y="{}" z="{}"/>'.format(max_x, max_y, max_z) + "\n"
            observation_str += '      </Grid>' + "\n"
            observation_str += '    </ObservationFromGrid>' + "\n"
        for entity_grid in self.char_to_ents[character_name]:
            details = self.ents_to_details[entity_grid] #type: ObservationGridDetails
            observation_str += '    <ObservationFromNearbyEntities>' + "\n"
            observation_str += '      <Range name="{}" xrange="{}" yrange="{}" zrange="{}" update_frequency="{}"/>' \
                .format(entity_grid, details.x_range, details.y_range, details.z_range, details.update_frequency) + "\n" #TODO: set vars; from mak: vars?
            observation_str += '    </ObservationFromNearbyEntities>' + "\n"

        simple_craft_str = ""
        if self.simple_craft_commands:
            simple_craft_str += '    <SimpleCraftCommands/>' + "\n"

        inventory_str = ""
        if len(self.inventories[character_name]) > 0:
            inventory_str = '  <Inventory>' + "\n"
            for decorator in self.inventories[character_name]:
                inventory_str += decorator + "\n"
            inventory_str += '  </Inventory>' + "\n"

        agent = \
            '<AgentSection mode="{}">'.format(self.mode) + "\n" + \
            '  <Name>{}</Name>'.format(character_name) + "\n" + \
            '  <AgentStart>' + "\n" + \
            character_start + "\n" + \
            inventory_str + \
            '  </AgentStart>' + "\n" + \
            '  <AgentHandlers>' + "\n" + \
            observation_str + "\n" + \
            '    <VideoProducer>' + "\n" + \
            '      <Width>{}</Width>'.format(self.video_width) + "\n" + \
            '      <Height>{}</Height>'.format(self.video_height) + "\n" + \
            '    </VideoProducer>' + "\n" + \
            '    <ContinuousMovementCommands/>' + "\n" + \
            '    <DiscreteMovementCommands autoJump="1"/>' + "\n" + \
            '    <InventoryCommands/>' + "\n" + \
            simple_craft_str + \
            '    <MissionQuitCommands/>' + "\n" + \
            '  </AgentHandlers>' + "\n" + \
            '</AgentSection>'

        return agent

    ###########################################################################################
    #
    #                           Server Handler Decorators
    #
    ###########################################################################################

    def draw_entity(self, x, y, z, type):
        entity_str = '<DrawEntity x="{}" y="{}" z="{}" type="{}"/>'.format(x, y, z, type)
        return entity_str

    def draw_item(self, x, y, z, type, colour=None):
        colour_str = ""
        if colour is not None:
            colour_str = 'colour="{}"'.format(colour)
        entity_str = '<DrawItem x="{}" y="{}" z="{}" type="{}" {}/>'.format(x, y, z, type, colour_str)
        return entity_str

    def draw_block(self, x, y, z, type, colour=None):
        colour_str = ""
        if colour is not None:
            colour_str = 'colour="{}"'.format(colour)
        block_str = '<DrawBlock x="{}" y="{}" z="{}" type="{}" {}/>'.format(x, y, z, type, colour_str)
        return block_str

    def draw_cuboid(self, x1, y1, z1, x2, y2, z2, type, description="", colour=None):
        colour_str = ""
        if colour is not None:
            colour_str = 'colour="{}"'.format(colour)
        desc_str = ""
        if description != "":
            desc_str = '<!-- {} -->'.format(description)
        cuboid_str = \
            desc_str + \
            '<DrawCuboid x1="{}" y1="{}" z1="{}" x2="{}" y2="{}" z2="{}" type="{}" {}/>' \
                .format(x1, y1, z1, x2, y2, z2, type, colour_str)
        return cuboid_str

    def draw_line(self, x1, y1, z1, x2, y2, z2, type, description="", colour=None):
        colour_str = ""
        if colour is not None:
            colour_str = 'colour="{}"'.format(colour)
        desc_str = ""
        if description != "":
            desc_str = '<!-- {} -->'.format(description)
        line_str = \
            desc_str + \
            '<DrawLine x1="{}" y1="{}" z1="{}" x2="{}" y2="{}" z2="{}" type="{}" {}/>' \
                .format(x1, y1, z1, x2, y2, z2, type, colour_str)
        return line_str

    def draw_chest(self, x, y, z, type='chest', contents=[], description=""):
        desc_str = ""
        if description != "":
            desc_str = '<!-- {} -->'.format(description)
        line_str = \
            desc_str + \
            '<DrawContainer x="{}" y="{}" z="{}" type="{}" >'.format(x, y, z, type)
        for item in contents:
            line_str += '<Object type="{}" />'.format(item)
        line_str += '</DrawContainer>'
        return line_str

    def add_decorator_comment(self, comment):
        comment_str = "    <!-- {} -->".format(comment)
        self.decorators.append(comment_str)

    def add_block(self, x, y, z, type, colour=None):
        block_str = "    " + self.draw_block(x, y, z, type, colour)
        self.decorators.append(block_str)

    def add_line(self, x1, y1, z1, x2, y2, z2, type, description="", colour=None):
        line_str = "    " + self.draw_line(x1, y1, z1, x2, y2, z2, type, description=description, colour=colour)
        self.decorators.append(line_str)

    def add_cuboid(self, x1, y1, z1, x2, y2, z2, type, description="", colour=None):
        cuboid_str = "    " + self.draw_cuboid(x1, y1, z1, x2, y2, z2, type, description=description, colour=colour)
        self.decorators.append(cuboid_str)

    def add_chest(self, x, y, z, type, contents=[], description=""):
        line_str = "    " + self.draw_chest(x, y, z, type, contents, description)
        self.decorators.append(line_str)

    def add_entity(self, x, y, z, type, description=""):
        if description != "":
            self.add_decorator_comment(description)
        entity_str = "    " + self.draw_entity(x, y, z, type)
        self.decorators.append(entity_str)

    def add_item(self, x, y, z, type, colour=None):
        item_str = "    " + self.draw_item(x, y, z, type, colour)
        self.decorators.append(item_str)

    ###########################################################################################
    #
    #                           Agent Handler Decorators
    #
    ###########################################################################################

    def draw_inventory_item(self, slot, name, count, colour=None):
        colour_str = ""
        if colour is not None:
            colour_str = 'colour="{}"'.format(colour)
        item_str = '<InventoryObject slot="{}" type="{}" {} quantity="{}"/>'.format(slot, name, colour_str, count)
        return item_str

    def clear_inventory(self, character="Alex"):
        self.inventories[character].clear()

    def add_inventory_item(self, slot, name, count, colour=None, character="Alex"):
        item_str = "    " + self.draw_inventory_item(slot, name, count, colour)
        self.inventories[character].append(item_str)

    def use_farming_inventory(self):
        self.clear_inventory()
        self.add_inventory_item(0, 'iron_hoe', 1)
        self.add_inventory_item(1, 'dye', 64, 'WHITE')
        self.add_inventory_item(2, 'wheat_seeds', 1)
        self.add_inventory_item(3, 'pumpkin_seeds', 1)
        self.add_inventory_item(4, 'potato', 1)
        self.add_inventory_item(5, 'melon_seeds', 1)
        self.add_inventory_item(6, 'beetroot_seeds', 1)
        self.add_inventory_item(9, 'dirt', 16)

    ###########################################################################################
    #
    #                           Structures
    #
    ###########################################################################################

    def add_challenge_room(self, xStart=-10, xEnd=10, zStart=-10, zEnd=10, yStart=1, yEnd=6, caged=False, mob_type="Zombie",
                           num_zombies=1, material="bedrock"):
        # Monster Number
        monster_string = ''
        for i in range(num_zombies):
            yPos = yStart + 1
            xPos = 0
            zPos = 6
            zombieX = xPos + 0.5
            zombieZ = zPos + 0.5
            monster_string += '<DrawEntity x="{}" y="{}" z="{}" type="{}" />' \
                .format(zombieX, yPos, zombieZ, mob_type)
            if caged:
                block_type = "bedrock"
                above = self.draw_block(xPos, yPos + 2, zPos, block_type)
                left = self.draw_block(xPos - 1, yPos, zPos, block_type)
                right = self.draw_block(xPos + 1, yPos, zPos, block_type)
                back = self.draw_block(xPos, yPos, zPos - 1, block_type)
                front = self.draw_block(xPos, yPos, zPos + 1, block_type)
                monster_string += above + left + right + back + front

        northMaterial = material
        southMaterial = material
        eastMaterial = material
        westMaterial = material

        floor = self.draw_cuboid(xStart, yStart, zStart, xEnd, yStart, zEnd, material, description="floor")
        air = self.draw_cuboid(xStart, yStart, zStart, xEnd, yEnd, zEnd, "air", description="air void in center")
        lighting = self.draw_cuboid(xStart, yEnd, zStart, xEnd, yEnd + 1, zEnd, "glowstone", "lighted ceiling")

        east_wall = self.draw_cuboid(xStart - 1, yStart, zStart, xStart, yEnd, zEnd, eastMaterial, description="east wall")
        west_wall = self.draw_cuboid(xEnd, yStart, zStart, xEnd + 1, yEnd, zEnd, westMaterial, "west wall")
        south_wall = self.draw_cuboid(xStart, yStart, zStart - 1, xEnd, yEnd, zStart, southMaterial, "south wall")
        north_wall = self.draw_cuboid(xStart, yStart, zEnd, xEnd, yEnd, zEnd + 1, northMaterial, "north wall")

        room = \
            north_wall + south_wall + east_wall + west_wall + \
            floor + lighting + air + \
            monster_string

        return room

    def add_chicken_coop(self, x, y, z):
        coop = self.draw_block(x, y, z, 'wool', 'MAGENTA')
        coop += self.draw_block(x + 1, y, z + 1, 'chest')
        coop += self.draw_block(x + 2, y, z + 1, 'chest')
        coop += self.draw_block(x + 2, y + 1, z + 1, 'hopper')
        coop += self.draw_block(x + 2, y + 2, z + 1, 'hopper')

        coop += self.draw_block(x + 2, y + 4, z + 1, 'glass')

        coop += self.draw_block(x + 3, y + 3, z + 1, 'glass')
        coop += self.draw_block(x + 1, y + 3, z + 1, 'glass')
        coop += self.draw_block(x + 2, y + 3, z + 2, 'glass')
        coop += self.draw_block(x + 2, y + 3, z, 'glass')

        coop += self.draw_entity(x + 2.5, y + 3, z + 1.5, 'Chicken')
        return coop

    def add_pond(self, x, y, z):
        pond = self.draw_block(x, y, z, 'wool', 'MAGENTA')
        pond += self.draw_cuboid(x + 1, y, z + 1, x + 7, y, z + 7, 'dirt')
        pond += self.draw_cuboid(x + 2, y, z + 2, x + 6, y, z + 6, 'water')
        pond += self.draw_cuboid(x + 2, y + 1, z + 2, x + 6, y + 1, z + 6, 'glass')
        return pond

    def add_wheat_farm(self, x, y, z, plant=False):
        self.add_decorator_comment("Wheat Farm")
        self.add_block(x, y, z, 'wool', 'MAGENTA')
        self.add_line(x + 1, y, z, x + 1, y, z + 7, "farmland")
        if plant:
            self.add_line(x + 1, y + 1, z, x + 1, y + 1, z + 7, "wheat")

        self.add_line(x + 2, y, z, x + 2, y, z + 7, "water")
        self.add_line(x + 2, y + 1, z, x + 2, y + 1, z + 7, "glass")
        self.add_line(x + 3, y + 1, z, x + 3, y + 1, z + 7, "torch")

    def add_melon_farm(self, x, y, z, type="pumpkin", plant=False, width=7, depth=5):
        self.add_decorator_comment("{} Farm".format(type))
        self.add_block(x, y, z, 'wool', 'MAGENTA')
        included_width = width - 3
        for i in range(1, depth + 1):
            self.add_block(x + i, y, z, "dirt")
        for i in range(1, depth + 1):
            self.add_block(x + i, y, z + (width-1), "dirt")
        z = z + 1

        if depth < 4:
            for i in range(0, depth):
                self.add_line(x + i, y, z, x + i, y, z + included_width, "farmland")
        else:
            dirt_size = depth - 4
            for i in range(1, dirt_size + 1):
                self.add_line(x + i, y, z, x + i, y, z + included_width, "dirt")
            for i in range(dirt_size + 1, depth + 1):
                self.add_line(x + i, y, z, x + i, y, z + included_width, "farmland")


        # self.add_line(x + 2, y, z, x + 2, y, z + width, "farmland")
        # self.add_line(x + 3, y, z, x + 3, y, z + width, "farmland")
        # self.add_line(x + 4, y, z, x + 4, y, z + width, "farmland")
        if plant:
            self.add_line(x + 1, y + 1, z, x + 1, y + 1, z + included_width, type)

        water_x = x + depth + 1
        torch_x = x + depth + 2
        self.add_line(water_x, y, z, water_x, y, z + included_width, "water")
        self.add_line(water_x, y + 1, z, water_x, y + 1, z + included_width, "wooden_slab")
        self.add_line(torch_x, y + 1, z, torch_x, y + 1, z + included_width, "torch")

    def add_cross_farm(self, x, y, z, num_directions=4):
        self.add_decorator_comment("Cross Farm")
        coordinates = ((-1, 0), (1, 0), (0, 1), (0, -1))
        direction = 0
        for coordinate in coordinates:
            if direction >= num_directions:
                break
            x_offset = coordinate[0]
            z_offset = coordinate[1]
            self.add_line(x + x_offset, y, z + z_offset, x + x_offset, y, z + z_offset, "farmland")
            direction += 1

        self.add_line(x, y, z, x, y, z, "water")
        self.add_line(x, y + 1, z, x, y + 1, z, "glass")
        self.add_line(x, y + 2, z, x, y + 2, z, "torch")

    def add_chicken(self, x, y, z):
        self.add_entity(x, y, z, "Chicken")

    def add_egg(self, x, y, z):
        self.add_item(x, y, z, "egg")

    def add_cow(self, x, y, z):
        self.add_decorator_comment("Cows!")
        self.add_entity(x, y, z, "Cow")

    ###################################################################################################################
    #
    # ASSETS FOR WORLDGEN
    #
    ###################################################################################################################

    def add_cage(self, x, y, z):
        self.add_decorator_comment("Testing Cage")
        self.add_cuboid(x - 1, y, z - 1, x + 129, y + 6, z + 129, "stone")
        self.add_cuboid(x, y, z, x + 128, y + 6, z + 128, "air")
        self.add_cuboid(x - 1, y, z - 1, x + 129, y, z + 129, "iron_block")

    def add_farm(self, x, y, z):
        self.add_decorator_comment("Player Farm")
        #self.add_cuboid(x, y + 1, z, x + 11, y + 1, z + 11, "fence")
        self.add_cuboid(x + 1, y + 1, z + 1, x + 10, y + 1, z + 10, "air")
        self.add_cuboid(x + 1, y, z + 2, x + 4, y, z + 5, "dirt")
        self.add_line(x + 1, y, z + 1, x + 4, y, z + 1, "wool")
        self.add_block(x, y, z + 1, "water")
        self.add_line(x + 5, y + 1, z + 0, x + 6, y + 1, z + 0, "air")
        self.add_line(x + 11, y + 1, z + 5, x + 11, y + 1, z + 6, "air")

    def add_barn(self, x, y, z, ):
        self.add_decorator_comment("Cow Barn")
        self.add_cuboid(x - 1, y + 1, z - 1, x + 2, y + 1, z + 2, "fence")
        self.add_cuboid(x, y + 1, z, x + 1, y + 1, z + 1, "air")
        self.add_entity(x, y + 1, z, "Cow")
        self.add_entity(x, y + 1, z + 1, "Cow")

    def add_coop(self, x, y, z, include_fence=False):
        self.add_decorator_comment("Chicken Coop")
        if include_fence:
            self.add_cuboid(x - 1, y + 1, z - 1, x + 2, y + 1, z + 2, "fence")
        self.add_cuboid(x, y + 1, z, x + 1, y + 1, z + 1, "air")
        self.add_entity(x, y + 1, z, "Chicken")
        self.add_entity(x, y + 1, z + 1, "Chicken")
        self.add_entity(x + 1, y + 1, z, "Chicken")
        self.add_entity(x + 1, y + 1, z + 1, "Chicken")

    def add_sugar(self, x, y, z):
        self.add_decorator_comment("Sugar Field")
        self.add_block(x - 1, y, z, "dirt")
        self.add_block(x, y, z + 1, "dirt")
        self.add_block(x - 2, y, z + 1, "dirt")
        self.add_block(x - 1, y, z + 2, "dirt")
        self.add_block(x - 1, y + 1, z, "reeds")
        self.add_block(x, y + 1, z + 1, "reeds")
        self.add_block(x - 2, y + 1, z + 1, "reeds")
        self.add_block(x - 1, y + 1, z + 2, "reeds")
        self.add_block(x - 1, y, z + 1, "water")
        self.add_block(x - 1, y + 1, z + 1, "glass")

    def add_pumpkins(self, x, y, z):
        self.add_decorator_comment("Pumpking Field")
        self.add_line(x - 1, y + 1, z, x - 1, y + 1, z + 2, "pumpkin")

    def add_wheat_field(self, x, y, z):
        self.add_decorator_comment("Wheat Field")
        self.add_cuboid(x, y, z, x + 2, y, z + 2, "farmland")
        self.add_cuboid(x, y + 1, z, x + 2, y + 1, z + 2, "wheat")

    def add_beet_field(self, x, y, z):
        self.add_decorator_comment("Beet Field")
        self.add_cuboid(x, y, z, x + 2, y, z + 2, "farmland")
        self.add_cuboid(x, y + 1, z, x + 2, y + 1, z + 2, "beetroots")

    def add_potato_field(self, x, y, z):
        self.add_decorator_comment("Potato Field")
        self.add_cuboid(x, y, z, x + 2, y, z + 2, "farmland")
        self.add_cuboid(x, y + 1, z, x + 2, y + 1, z + 2, "potatoes")

    def add_carrot_field(self, x, y, z):
        self.add_decorator_comment("Carrot field")
        self.add_cuboid(x, y, z, x + 2, y, z + 2, "farmland")
        self.add_cuboid(x, y + 1, z, x + 2, y + 1, z + 2, "carrots")

    def add_woods(self, x, y, z):
        self.add_decorator_comment("Woods")
        self.add_cuboid(x, y + 1, z, x + 2, y + 1, z + 2, "log")

    def add_bones(self, x, y, z):
        self.add_decorator_comment("Bones")
        self.add_cuboid(x, y + 1, z, x + 2, y + 1, z + 2, "bone_block")

    ########################################################################################################################
    #####
    ##### WORLD GEN COMMANDS
    #####
    ########################################################################################################################
    assets = [
        "beets",
        "wheat",
        "potato",
        "carrot",
        "pumpkin",
        "sugar",
        "chicken",
        "cow"
    ]

    def add_from_list(self, itemnum, x , z , y = 15):
        asset_type = self.assets[itemnum]
        if asset_type == "beets":
            self.add_beet_field(x, y, z)
        elif asset_type == "wheat":
            self.add_wheat_field(x, y, z)
        elif asset_type == "potato":
            self.add_potato_field(x, y, z)
        elif asset_type == "carrot":
            self.add_carrot_field(x, y, z)
        elif asset_type == "pumpkin":
            self.add_pumpkins(x, y, z)
        elif asset_type == "sugar":
            self.add_sugar(x, y, z)
        elif asset_type == "chicken":
            self.add_coop(x, y, z)
        elif asset_type == "cow":
            self.add_barn(x, y, z)

    def pop_from_list(self, itemnum, x, z, y = 15):
        asset_type = self.assets.pop(itemnum)
        if asset_type == "beets":
            self.add_beet_field(x, y, z)
        elif asset_type == "wheat":
            self.add_wheat_field(x, y, z)
        elif asset_type == "potato":
            self.add_potato_field(x, y, z)
        elif asset_type == "carrot":
            self.add_carrot_field(x, y, z)
        elif asset_type == "pumpkin":
            self.add_pumpkins(x, y, z)
        elif asset_type == "sugar":
            self.add_sugar(x, y, z)
        elif asset_type == "chicken":
            self.add_coop(x, y, z)
        elif asset_type == "cow":
            self.add_barn(x, y, z)

    def random_offset_calculator(self, x_origin, z_origin):
        x_ogn_abs = abs(x_origin)
        z_ogn_abs = abs(z_origin)
        x_skew = random.randint(0, x_ogn_abs / 2)
        z_skew = random.randint(0, z_ogn_abs / 2)
        x_neg = random.randint(-1, 0)
        z_neg = random.randint(-1, 0)
        if x_neg == -1:
            x_offset = x_skew * -1
        else:
            x_offset = x_skew
        if z_neg == -1:
            z_offset = z_skew * -1
        else:
            z_offset = z_skew
        x_pos = x_origin + x_offset
        z_pos = z_origin + z_offset
        return x_pos, z_pos

    def add_random_positions_2_7(self, xwidth, zwidth):
        x, z = self.random_offset_calculator(xwidth, -zwidth)
        self.add_from_list(2, x, z)
        x, z = self.random_offset_calculator(0, -zwidth)
        self.add_from_list(3, x, z)
        x, z = self.random_offset_calculator(-xwidth, -zwidth)
        self.add_from_list(4, x, z)
        x, z = self.random_offset_calculator(xwidth, zwidth)
        self.add_from_list(5, x, z)
        x, z = self.random_offset_calculator(0, zwidth)
        self.add_from_list(6, x, z)
        x, z = self.random_offset_calculator(-xwidth, zwidth)
        self.add_from_list(7, x, z)

    def pop_random_positions(self, xwidth, zwidth):
        x, z = self.random_offset_calculator( xwidth, -zwidth)
        self.pop_from_list(0, x, z)
        x, z = self.random_offset_calculator( 0,      -zwidth)
        self.pop_from_list(0, x, z)
        x, z = self.random_offset_calculator(-xwidth, -zwidth)
        self.pop_from_list(0, x, z)
        x, z = self.random_offset_calculator( xwidth,  zwidth)
        self.pop_from_list(0, x, z)
        x, z = self.random_offset_calculator( 0,       zwidth)
        self.pop_from_list(0, x, z)
        x, z = self.random_offset_calculator(-xwidth,  zwidth)
        self.pop_from_list(0, x, z)

    def random_positions_world(self, xwidth = 20, zwidth = 20):
        self.add_farm(0, 15, 0)
        self.add_from_list(0, int(xwidth/2), 0)
        self.add_from_list(1, int(-zwidth/2), 0)
        self.add_random_positions_2_7(xwidth, zwidth)

    def random_assets_and_pos(self, xwidth = 20, zwidth = 20, asset0 = 0, asset1 = 1):
        self.add_farm(0, 15, 0)
        self.add_from_list(asset0, int(xwidth/2), 0)
        self.add_from_list(asset1, int(-zwidth/2), 0)
        del self.assets[asset0]
        del self.assets[asset1]
        random.shuffle(self.assets)
        self.pop_random_positions(xwidth, zwidth)

    def add_random_asset(self, x, y, z):
        rndm_num = random.randint(0, 7)
        if rndm_num == 0:
            self.add_barn(x, y, z)
        elif rndm_num == 1:
            self.add_coop(x, y, z)
        elif rndm_num == 2:
            self.add_sugar(x, y, z)
        elif rndm_num == 3:
            self.add_pumpkins(x, y, z)
        elif rndm_num == 4:
            self.add_wheat_field(x, y, z)
        elif rndm_num == 5:
            self.add_beet_field(x, y, z)
        elif rndm_num == 6:
            self.add_potato_field(x, y, z)
        elif rndm_num == 7:
            self.add_carrot_field(x, y, z)

    def add_meta_asset(self, x, y, z):
        self.add_cage(x - 64, y, z - 64)
        self.add_coop(x, y + 1, z)
        self.add_barn(x + 10, y + 1, z)
        self.add_sugar(x + 15, y, z)
        self.add_pumpkins(x + 20, y, z)
        self.add_wheat_field(x + 25, y, z)
        self.add_beet_field(x + 30, y, z)
        self.add_potato_field(x + 35, y, z)
        self.add_woods(x + 40, y, z)
        self.add_bones(x + 45, y, z)

    def generate_testzone(self, x, y, z):
        self.add_cage(x, y, z)
        self.add_farm(x + 59, y, z + 59)
        self.add_random_asset(x + 10, y, z + 10)
        self.add_random_asset(x + 10, y, z + 120)
        self.add_random_asset(x + 120, y, z + 10)
        self.add_random_asset(x + 120, y, z + 120)
        self.add_random_asset(x + 10, y, z + 64)
        self.add_random_asset(x + 64, y, z + 10)
        self.add_random_asset(x + 120, y, z + 64)
        self.add_random_asset(x + 64, y, z + 120)

    def add_static_assets(self, x, y, z):
        self.add_farm(x + 59, y, z + 59)
        self.add_beet_field(x + 10, y, z + 10)
        self.add_wheat_field(x + 10, y, z + 120)
        self.add_carrot_field(x + 120, y, z + 10)
        self.add_potato_field(x + 120, y, z + 120)
        self.add_coop(x + 10, y, z + 64)
        self.add_barn(x + 64, y, z + 10)
        self.add_pumpkins(x + 120, y, z + 64)
        self.add_sugar(x + 64, y, z + 120)
