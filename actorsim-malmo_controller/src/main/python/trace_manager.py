from threading import Thread, Condition

from character.action_trace import TraceList, ActionTrace

from utils.LogManager import LogManager

logger = LogManager.get_logger("actorsim.malmo.trace_manager")

class TraceManager:

    def __init__(self, character: 'Character'):
        self.name = "trace_manager" # Logging ID for class...? (todo: Ask if we really need this.)

        self.character = character
        self.character_post_trace_condition = Condition()
        self.worker_tread = TraceManager.Worker(self, self.character)
        self.character.add_thread_to_start_with_mission(self.worker_tread)

        self.trace_list = TraceList()
        self.last_update_game_time = 0
        self.last_processed_game_time = 0

        self.character.add_post_hook_030_action_trace(self.post_trace_hook)
        self.character.trace_manager = self  #NB: there can only be one trace_manager per character!

        self.actions_to_remove_from_symbolic_trace = ["pause",
                                                      "pause_until_entity_update",
                                                      "pause_until_entity_dead"
                                                      ]


    def post_trace_hook(self, character: 'Character'):
        logger.debug("Running trace_list hook!")
        self.last_update_game_time = character.get_current_time()
        with self.character_post_trace_condition:
            self.character_post_trace_condition.notify()

    def get_and_reset(self):
        return self.trace_list.get_and_reset()

    def convert_trace_to_symbolic_trace(self, trace_list: [ActionTrace]):
        symbolic_trace_list = list()
        for obs in trace_list:
            symbolic_observation = obs.clone()  # type: ActionTrace
            params = symbolic_observation.primitive_action_params
            symbolic_params = list()
            logger.trace("{}: Looking at plan state {}".format(self.name,
                                                               symbolic_observation.applied_state_index))

            state = self.character.memory.get_plan_state(symbolic_observation.applied_state_index)  # type: PlanState
            logger.trace("{}: Converting observation: {}".format(self.name, symbolic_observation))
            logger.trace("{}: Primitive Action Name: {}, "
                         "Parameters: {}".format(self.name, symbolic_observation.primitive_action_name, params))
            is_action_inconsistent = False
            for param in params:
                if isinstance(param, str):
                    not_integer = False
                    not_float = False

                    try:
                        int(param)
                    except ValueError:
                        not_integer = True

                    try:
                        float(param)
                    except ValueError:
                        not_float = True

                    if not_float and not_integer:
                        symbolic_params.append(param)
                    continue

                # Assuming that the parameter is a Minecraft object
                object_found = False
                for obj_name, mc_object in state.variable_bindings.items():
                    if mc_object.variable_object == param:
                        symbolic_params.append(obj_name)
                        object_found = True
                        break

                if not object_found:
                    is_action_inconsistent = True
                    break

            logger.trace("{}: Symbolic Parameter for action {}: "
                         "{}".format(self.name, symbolic_observation.primitive_action_name, symbolic_params))

            if not is_action_inconsistent:
                symbolic_observation.primitive_action_params = symbolic_params
                symbolic_trace_list.append(symbolic_observation)
            else:
                logger.debug("This action is inconsistent (contains object not in state). "
                             "Removing...".format(symbolic_observation))
        return symbolic_trace_list

    def construct_symbolic_action_trace(self, symbolic_trace: [ActionTrace]):
        symbolic_action_trace = list()
        for action in symbolic_trace:
            name = action.primitive_action_name
            params = " ".join(action.primitive_action_params)
            if len(name) == 0 or name in self.actions_to_remove_from_symbolic_trace:
                continue
            if len(params) == 0:
                _action = "({})".format(name)
            else:
                _action = "({} {})".format(name, params)
            if _action not in symbolic_action_trace:
                symbolic_action_trace.append(_action)
        return symbolic_action_trace


    class Worker(Thread):
        def __init__(self, manager: 'TraceManager', character : 'Character'):
            Thread.__init__(self)
            self.manager = manager
            self.character = character
            self.connector = character.connector
            old_name = self.getName()
            self.name="trace_manager_thread_{}_{}".format(self.character.name, old_name)

        def run(self):
            while (self.connector.world_state.is_mission_running and self.character._keep_running):
                c = self.character
                with self.manager.character_post_trace_condition:
                    self.manager.character_post_trace_condition.wait()
                    #release the lock for external processing

                action_traces = self.character.action_trace_list.get_and_reset()
                plan_traces = self.character.plan_trace_list.get_and_reset()

                # todo mak: add same line for plan manager - get new plans - DONE
                # todo pavan: add processing for trace details - DONE
                # todo mak: figure out how to save memory in json format
                # todo pavan: convert plan state (to json?) - DONE
                # todo mak: ensure the agent only pursues a single goal to completion
                self.manager.trace_list.append_all(plan_traces)
                self.manager.trace_list.append_all(action_traces)
                logger.debug("Added latest details from {} traces.".format(len(action_traces)))

                if not c._keep_running:
                    break
            logger.debug("{} finished run".format(self.name))



