
import collections

from utils.LogManager import LogManager
from loggers import MalmoLoggers
from mission import Mission

logger = MalmoLoggers.command_logger

class Command:

    def __init__(self, mission : Mission, character_name : str):
        self.mission = mission
        self.char_name = character_name

        self.move = 0.0  # type: float
        self.strafe = 0.0  # type: float
        self.pitch = 0.0  # type: float
        self.turn = 0.0  # type: float
        self._attack_count = 0  # type: int
        self.use = False  # type: bool
        self.jump = False  # type: bool
        self.crouch = False  # type: bool
        self.quit = False  # type: bool

        self.last_move = -1.0 #type: float
        self.last_strafe = -1.0 #type: float
        self.last_pitch = -1.0 #type: float
        self.last_turn = -1.0 #type: float
        self.last_use = None #type: bool
        self.last_jump = None #type: bool
        self.last_attack = None #type: bool
        self.last_crouch = None #type: bool
        self.last_command = ""

        self.command_queue = collections.deque() #type: Deque[str]
        self.reset()

    def __repr__(self):
        command_str = ""
        if self.quit:
            command_str = "quit"
        else:
            move_str = " move {:.3f}".format(self.move)
            strafe_str = " strafe {:.3f}".format(self.strafe)
            pitch_str = " pitch {:.3f}".format(self.pitch)
            turn_str = " turn {:.3f}".format(self.turn)
            jump_str = " jump 0"
            if self.jump:
                jump_str = " jump 1"
            crouch_str = " crouch 0"
            if self.crouch:
                crouch_str = " crouch 1"
            attack_str = " attack 0"
            if self._attack_count > 0:
                attack_str = " attack 1"
            use_str = " use 0"
            if self.use:
                use_str = " use 1"
            quit_str = ""
            if self.quit:
                quit_str = "quit"

            command_str = move_str + strafe_str + pitch_str + turn_str + \
                          jump_str + crouch_str + attack_str + use_str + quit_str


        return command_str

    def __str__(self):
        return self.__repr__()

    def enqueue_command(self, command):
        '''Queues a command to be executed along with the next send_command.
           More than one command may be queued to provide a sequence.
           However, only one command will be executed per cognitive cycle of the agent.

           This method is mainly for sending commands that are not part of the common set.
           For example, use this to add a command to select a hotbar item:
             slot = new_hotbar_slot_to_select #zero indexed
             add_command("hotbar.{} 1".format(slot + 1))
             add_command("hotbar.{} 0".format(slot + 1))
        '''
        self.command_queue.append(command)

    def send_commands(self, agent_host):
        new_commands = ""
        no_change_commands = ""

        queue_str = ""
        if len(self.command_queue) > 0:
            queue_str = self.command_queue.popleft()
            agent_host.sendCommand(queue_str)
            new_commands += "'{}' ".format(queue_str)
        else:
            if self.move != self.last_move:
                move_str = "move {:.6f}".format(self.move)
                agent_host.sendCommand(move_str)
                self.last_move = self.move
                new_commands += move_str + " "
            else:
                no_change_commands += "(move {:.6f}) ".format(self.last_move)

            if self.turn != self.last_turn:
                turn_str = "turn {:.6f}".format(self.turn)
                agent_host.sendCommand(turn_str)
                self.last_turn = self.turn
                new_commands += turn_str + " "
            else:
                no_change_commands += "(turn {:.6f})".format(self.turn)

            if self.pitch != self.last_pitch:
                pitch_str = "pitch {:.6f}".format(self.pitch)
                agent_host.sendCommand(pitch_str)
                self.last_pitch = self.pitch
                new_commands += pitch_str + " "
            else:
                no_change_commands += "(pitch {:.6f})".format(self.pitch)

            if self.strafe != self.last_strafe:
                strafe_str = "strafe {:.6f} ".format(self.strafe)
                agent_host.sendCommand(strafe_str)
                self.last_strafe = self.strafe
                new_commands += strafe_str + " "
            else:
                no_change_commands += "(strafe {:.6f})".format(self.strafe)

            attack_str = "attack 0"
            if self._attack_count > 0:
                attack_str = "attack 1"
                self._attack_count -= 1
            if attack_str != self.last_attack:
                agent_host.sendCommand(attack_str)
                self.last_attack = attack_str
                new_commands += attack_str + " "
            else:
                no_change_commands += "(" + attack_str + ") "

            jump_str = "jump 0"
            if self.jump:
                jump_str = "jump 1"
            if jump_str != self.last_jump:
                agent_host.sendCommand(jump_str)
                self.last_jump = jump_str
                new_commands += jump_str + " "
            else:
                no_change_commands += "(" + jump_str + ") "
                #always send a no-jump command due to a bug in Malmo with auto-jump
                #TODO change this to detect auto-jump and send command
                agent_host.sendCommand(jump_str)

            crouch_str = "crouch 0"
            if self.crouch:
                crouch_str = "crouch 1"
            if crouch_str != self.last_crouch:
                agent_host.sendCommand(crouch_str)
                self.last_crouch = crouch_str
                new_commands += crouch_str + " "
            else:
                no_change_commands += "(" + crouch_str + ") "

            use_str = "use 0"
            if self.use:
                use_str = "use 1"
            if use_str != self.last_use:
                agent_host.sendCommand(use_str)
                self.last_use = use_str
                new_commands += use_str + " "
            else:
                no_change_commands + "(" + use_str + ") "

            quit_str = ""
            if self.quit:
                quit_str = "quit"
                agent_host.sendCommand(quit_str)
                new_commands += quit_str

        self.last_command = new_commands + no_change_commands
        logger.debug("new: {}".format(new_commands))
        logger.debug("  implied: {}".format(no_change_commands))
        logger.debug("  attack_count:{} ".format(self._attack_count))


    def reset(self):
        self.reset_move()
        self.reset_strafe()
        self.reset_pitch()
        self.reset_turn()
        self.reset_jump()
        self.reset_crouch()
        self.reset_attack()
        self.reset_use()

    def set_move(self, value : float):
        '''move [-1, 1]
          “move 1” is full speed ahead; “move -0.5” moves backwards at half speed, etc.'''
        if value < -1 \
            or value > 1:
            msg = "The move command only accepts [-1, 1] 'move {}' is invalid".format(value)
            logger.error(msg)
            raise RuntimeError(msg)
        self.move = value

    def reset_move(self):
        '''reset move'''
        self.move = 0.0

    def set_strafe(self, value: float):
        '''
        strafe [-1,1]
        “strafe -1” moves left at full speed; “strafe 1” moves right at full speed, etc.
        '''
        if value < -1 \
                or value > 1:
            msg = "The strafe command only accepts [-1, 1] 'strafe {}' is invalid".format(value)
            logger.error(msg)
            raise RuntimeError(msg)
        self.strafe = value

    def reset_strafe(self):
        '''reset strafe'''
        self.strafe = 0.0


    def set_pitch(self, value : float):
        '''
        pitch [-1,1]
        “pitch -1” starts tipping camera upwards at full speed, “pitch 0.1” starts looking down slowly, etc.
        '''
        if value < -1 \
            or value > 1:
            msg = "The pitch command only accepts [-1, 1] 'pitch {}' is invalid".format(value)
            logger.error(msg)
            raise RuntimeError(msg)
        self.pitch = value

    def reset_pitch(self):
        '''reset pitch'''
        self.pitch = self.mission.start[self.char_name].pitch

    def set_turn(self, value: float):
        '''
        turn [-1,1]
        “turn -1” starts turning full speed left, etc.
        '''
        if value < -1 \
                or value > 1:
            msg = "The turn command only accepts [-1, 1] 'turn {}' is invalid".format(value)
            logger.error(msg)
            raise RuntimeError(msg)
        self.turn = value

    def reset_turn(self):
        '''reset pitch'''
        self.turn = 0.0

    def set_jump(self):
        self.jump = True

    def reset_jump(self):
        self.jump = False

    def set_crouch(self):
        self.crouch = True

    def reset_crouch(self):
        self.crouch = False

    def set_attack(self, cycle_count=1):
        self._attack_count = cycle_count

    def reset_attack(self):
        self._attack_count = 0

    def set_use(self):
        self.use = True

    def reset_use(self):
        self.use = False

    def set_quit(self):
        self.quit = True


