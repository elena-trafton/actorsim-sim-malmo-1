
import json
import math
import traceback

from container_classes import PlayerInventory, BlockInventory, NULL_BLOCK_INVENTORY, NULL_PlAYER_INVENTORY, ItemStack
from location import Location, EntityLocation, BlockLocation, MobLocation, DropLocation
from malmo_types import MalmoTypeManager
from loggers import MalmoLoggers

logger = MalmoLoggers.character_logger
obs_logger = MalmoLoggers.obs_logger

class CharacterState():
    def __init__(self, character: 'Character'):
        self.character = character

        #items and state from latest observation (i.e., the most current state)
        self.location = EntityLocation()  # type: EntityLocation
        self.location.mc_type = "Player_{}".format(character.name)
        self.height = 1.62 #from game wiki
        self.CURRENT_LOCATION_NAME = "{}_current_location".format(str(self.character.name).lower())

        self.obs = None
        self.data = None
        self.world_time = 0
        self.state_index = 0
        self.life = 0
        self.inventory = NULL_PlAYER_INVENTORY
        self.chest = NULL_BLOCK_INVENTORY
        self._selected_hotbar = 0
        self.mobs_killed = 0
        self.damage_dealt = 0

        self.look_at = None
        self.look_distance = 0
        self.look_hit = ""  # type: str
        self.look_hit_type = ""  # type: str
        self.look_properties = dict() #type: Dict[str]
        self.look_in_range = False  # type: bool
        self.__clear_look()

        #delta of items and state from latest to previous observation
        self.new_mobs_killed = 0
        self.new_damage_dealt = 0


    def process_observation(self, new_obs, memory: 'CharacterMemory'):
        try:
            obs_logger.debug("\nProcessing observation")
            obs_logger.trace("current:{}\npast:{}".format(new_obs, self.obs))
            if type(new_obs) is str:
                data = json.loads(new_obs)

                self.__process_character(memory, data)
                self.__process_look(data)
                self.__process_inventory_items(data)
                self.__process_container_items(data)
                memory.process_blocks_and_entities(self, data)
                # TODO: change this obs_logger.debug("Current inventory locations '{}'".format(str(self.inventory.name_to_index_map)))
                self.obs = new_obs
                self.data = data
                memory.register_new_state(self)
            else:
                obs_logger.debug("Observation was empty and processing skipped.")
        except Exception as e:
            logger.error("PROBLEM: process_observation failed because {}".format(e))
            logger.error("{}".format(traceback.format_exc()))

    def __process_character(self, memory: 'CharacterMemory', data):
        obs_logger.debug("processing character")

        self.world_time = int(data.get("WorldTime"))
        self.total_time = int(data.get("TotalTime"))
        memory.reserve_state_index(self)

        x = float(data.get('XPos'))
        y = float(data.get('YPos'))
        z = float(data.get('ZPos'))
        self.location.set(x, y, z)
        self.location.pitch = float(data.get('Pitch'))
        self.location.yaw = float(data.get('Yaw'))

        self.name = data.get("Name")
        self.life = float(data.get('Life', 0))
        self.food = int(data.get("Food"))
        self.air = int(data.get("Air"))
        self.xp = int(data.get("XP"))

        self.new_mobs_killed = 0
        obs_logger.debug("processing mobs and damage")
        current_mobs_killed = float(data.get('MobsKilled', 0))
        if current_mobs_killed > self.mobs_killed:
            self.new_mobs_killed = current_mobs_killed - self.mobs_killed
        self.mobs_killed = current_mobs_killed

        self.new_damage_dealt = 0
        current_damage_dealt = float(data.get('DamageDealt', 0))
        if current_damage_dealt > self.damage_dealt:
            self.new_damage_dealt = current_damage_dealt - self.damage_dealt
        self.damage_dealt = current_damage_dealt

    def __clear_look(self):
        self.look_at = None
        self.look_distance = 0
        self.look_hit = ""  # type: str
        self.look_hit_type = "" # type: str
        self.look_properties.clear()
        self.look_in_range = False  # type: bool

    def __process_look(self, json_data):
        line_of_sight = json_data.get('LineOfSight')  # type: Dict[str, str]
        self.__clear_look()

        if line_of_sight is not None:
            obs_logger.debug("{}".format(line_of_sight))
            x = line_of_sight.get('x')
            y = line_of_sight.get('y')
            z = line_of_sight.get('z')
            if self.look_at is None:
                self.look_at = Location()
            self.look_at.set(x, y, z)
            self.look_distance = line_of_sight.get('distance')
            self.look_hit = line_of_sight.get('hitType')
            self.look_hit_type = MalmoTypeManager.get_real_name(line_of_sight.get('type'))
            self.look_in_range = bool(line_of_sight.get('inRange'))
            for key in line_of_sight.keys():
                if key.startswith("prop"):
                    value = line_of_sight[key]
                    self.look_properties[key] = value

            obs_logger.debug("look at:{} dist:{} hit:{} obj:{} in_range:{} properties:{}" \
                             .format(self.look_at, self.look_distance, self.look_hit, self.look_hit_type,
                                     self.look_in_range, self.look_properties))

    def __process_inventory_items(self, json_data):
        obs_logger.debug("processing items for inventory")
        obs_logger.trace("  now collecting items from '{}'".format(json_data))

        item_list = list()
        for i in range(PlayerInventory.DEFAULT_CHARACTER_INVENTORY_SIZE):
            item_string = "InventorySlot_{}_item".format(str(i))
            size_string = "InventorySlot_{}_size".format(str(i))
            colour_string = "InventorySlot_{}_colour".format(str(i))
            item = json_data.get(item_string)
            if item is not None:
                name = MalmoTypeManager.get_real_name(item)
                count = int(json_data.get(size_string))
                colour = json_data.get(colour_string)
                if name != "air":
                    obs_logger.trace("  Found item {} index {} color {}".format(name, count, colour))
                item_list.append(ItemStack(name, count, colour, i))
        self.inventory = PlayerInventory(item_list)
        obs_logger.trace("inventory:{}".format(self.inventory))

    def __process_container_items(self, json_data):
        if self.look_hit_type == "chest" and self.look_in_range:
            item_list = list()
            obs_logger.debug("processing items for chest")
            obs_logger.trace("  now collecting items from '{}'".format(json_data))
            for i in range(BlockInventory.DEFAULT_BLOCK_INVENTORY_SIZE):
                item_string = "container.chestSlot_{}_item".format(str(i))
                size_string = "container.chestSlot_{}_size".format(str(i))
                colour_string = "container.chestSlot_{}_colour".format(str(i))
                item = json_data.get(item_string)
                if item is not None:
                    name = MalmoTypeManager.get_real_name(item)
                    count = int(json_data.get(size_string))
                    colour = json_data.get(colour_string)
                    if name != "air":
                        obs_logger.trace("  Found item {} index {} color {}".format(name, count, colour))
                    item_list.append(ItemStack(name, count, colour, i))
            round_x = math.floor(self.look_at.x)
            round_y = math.floor(self.look_at.y)
            round_z = math.floor(self.look_at.z)
            self.chest = BlockInventory(item_list, BlockLocation(round_x, round_y, round_z, "chest"))
            obs_logger.trace("Container:{}".format(self.chest))

    def calculate_next_item_needed(self, items_requested):
        items_remaining = []
        for item in items_requested:
            if item not in self.inventory:
                items_remaining.append(item)

        next_item = None
        if len(items_remaining) > 0:
            next_item = items_remaining[0]

        return next_item

