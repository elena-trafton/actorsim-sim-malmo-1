from enum import Enum

from utils.LogManager import LogManager
from area import Area
from character.character import Character
from character.goal_priority import TidyInventoryPriority, ExploreForPriority, ObtainItemPriority
from utils.plan_manager import PlanManager

logger = LogManager.get_logger('actorsim.malmo.character')


class NoValue(Enum):
    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self.name)


class PlannerType(NoValue):
    NONE = 1
    PYHOP = 2
    SHOP = 4


class FoodPreference(NoValue):
    NONE = 1
    CARNIVORE = 2
    HERBIVORE = 3
    OMNIVORE = 4
    TEST_BREAD = 5
    TEST_CHICKEN = 6
    TEST_BEEF = 7
    TEST_POTATOES = 8
    TEST_CARROTS = 9
    TEST_BEETROOTS = 10
    TEST_CAKE = 11
    TEST_PUMPKIN_PIE = 12

class GoalManagerType(NoValue):
    NONE = 1
    GOAL_PRIORITY_MANAGER = 2
    ACTORSIM_GOAL_MANAGER = 3

class CharacterFactory():

    def create_character(self,
                         mission: 'Mission',
                         planner_type: PlannerType = PlannerType.NONE,
                         goal_manager_type: GoalManagerType = GoalManagerType.GOAL_PRIORITY_MANAGER,
                         explore_area=Area(min_x=-45, max_x=45, min_z=-45, max_z=45),
                         food_preference: FoodPreference = FoodPreference.NONE,
                         include_explore_at_end = True
                         ):
        self.mission = mission
        self.planner_type = planner_type
        self.goal_manager_type = goal_manager_type
        self.food_preference = food_preference
        self.area = explore_area

        if self.food_preference is not FoodPreference.NONE:
            if planner_type is PlannerType.NONE:
                msg = "You must specify a valid plan manager to use a food preference"
                logger.error(msg)
                raise Exception(msg)

        self.character = Character(mission)

        self.plan_manager = None
        if planner_type is PlannerType.SHOP:
            self.plan_manager = PlanManager()
            self.character.plan_manager = self.plan_manager  # PlanManager

        if goal_manager_type is GoalManagerType.GOAL_PRIORITY_MANAGER:
            from character.goal_manager import GoalManager
            GoalManager(self.character, mission)  # type: GoalManager

            self.add_tidy_inventory_priority()
            self.add_food_goal_priorities()
            if include_explore_at_end:
                self.add_explore_priority()
        elif goal_manager_type is GoalManagerType.ACTORSIM_GOAL_MANAGER:
            from character.goal_manager_actorsim import ActorSimGoalManager
            ActorSimGoalManager(self.character, mission)


        return self.character

    def add_food_goal_priorities(self):
        if self.food_preference is FoodPreference.CARNIVORE:
            self.add_carnivore_priorities()
        elif self.food_preference is FoodPreference.HERBIVORE:
            self.add_herbivore_priorities()
        elif self.food_preference is FoodPreference.OMNIVORE:
            self.add_omnivore_priorities()
        elif self.food_preference is FoodPreference.TEST_BREAD:
            self.add_obtain_crafted_item_priority("bread")
        elif self.food_preference is FoodPreference.TEST_CHICKEN:
            self.add_obtain_chicken_meat_priority()
        elif self.food_preference is FoodPreference.TEST_BEEF:
            self.add_obtain_beef_priority()
        elif self.food_preference is FoodPreference.TEST_POTATOES:
            self.add_crop_priority("potato")
        elif self.food_preference is FoodPreference.TEST_CARROTS:
            self.add_crop_priority("carrots")
        elif self.food_preference is FoodPreference.TEST_BEETROOTS:
            self.add_crop_priority("beetroot")
        elif self.food_preference is FoodPreference.TEST_CAKE:
            self.add_obtain_crafted_item_priority("cake")
        elif self.food_preference is FoodPreference.TEST_PUMPKIN_PIE:
            self.add_obtain_crafted_item_priority("pumpkin_pie")

    def add_carnivore_priorities(self):
        self.add_obtain_beef_priority()
        self.add_obtain_chicken_meat_priority()

    def add_herbivore_priorities(self):
        self.add_obtain_crafted_item_priority("bread")
        self.add_crop_priority("potato")
        self.add_crop_priority("carrot")
        self.add_crop_priority("beetroot")

    def add_omnivore_priorities(self):
        self.add_obtain_crafted_item_priority("cake")
        self.add_obtain_crafted_item_priority("pumpkin_pie")
        self.add_carnivore_priorities()
        self.add_herbivore_priorities()

    def add_obtain_crafted_item_priority(self, item_name):
        priorities = self.character.goal_manager.goal_priorities
        priorities.append(ObtainItemPriority(self.character, self.plan_manager, item_name))

    def add_obtain_chicken_meat_priority(self):
        priorities = self.character.goal_manager.goal_priorities
        priorities.append(ObtainItemPriority(self.character, self.plan_manager, "chicken_meat"))

    def add_obtain_beef_priority(self):
        priorities = self.character.goal_manager.goal_priorities
        priorities.append(ObtainItemPriority(self.character, self.plan_manager, "beef"))

    def add_crop_priority(self, crop_name):
        priorities = self.character.goal_manager.goal_priorities
        priorities.append(ObtainItemPriority(self.character, self.plan_manager, crop_name))

    def add_explore_priority(self):
        priorities = self.character.goal_manager.goal_priorities
        priorities.append(ExploreForPriority(self.character, self.area))

    def add_tidy_inventory_priority(self):
        priorities = self.character.goal_manager.goal_priorities
        priorities.append(TidyInventoryPriority(self.character))

