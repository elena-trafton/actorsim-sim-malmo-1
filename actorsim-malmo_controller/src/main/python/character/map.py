import math

from entity import Entity

from location import Location, BlockLocation, EntityLocation
from mission import ObservationGridDetails


class CharacterMap:

    def __init__(self):
        self.cells = dict()  #type: Dict[str, Location]
        self.type_to_location = dict() #type: Dict[str, List[Location]]
        self.newly_seen_since_last_request = set()
        self.already_seen = set()

    def __repr__(self):
        result = ""
        sep = ""
        for key, item in self.cells.items():
            result += "{}{}={}".format(sep, key, item)
            sep = ", "
        return result

    def type_at(self, location: Location):
        type_str = None
        key_str = location.int_key_str()
        if key_str in self.cells:
            cell = self.cells[key_str]
            type_str = cell.type
        return type_str

    def contains(self, type):
        in_container = False
        if type in self.type_to_location:
            in_container = True
        return in_container

    def get_recently_seen(self):
        previously_seen = self.newly_seen_since_last_request
        self.newly_seen_since_last_request = set()
        return previously_seen

    def get_layer_matrix_as_string_array(self, details :ObservationGridDetails, start: Location, y_layer: int):
        lines = list()
        half_x = int(details.x_range / 2)
        half_y = int(details.y_range / 2)
        half_z = int(details.z_range / 2)

        x_start = math.floor(start.x) - half_x
        # y_start = math.floor(start.y) - half_y
        z_start = math.floor(start.z) - half_z

        location = Location()

        width = 4    #the printing width for columns
        header = "{:<{width}}".format("", width=width)
        for z in range(details.z_range):
            absolute_z = z_start + z
            header += "{:<{width}}".format(absolute_z, width=width)
        lines.append(header)

        i = 0
        absolute_y = y_layer
        for x in range(details.x_range-1, -1, -1):
            absolute_x = x_start + x
            line = "{:<{width}}".format(absolute_x, width=width)
            for z in range(details.z_range):
                absolute_z = z_start + z
                location.set(absolute_x, absolute_y, absolute_z)
                key_str = location.int_key_str()
                block_type = "?"
                if key_str in self.cells:
                    block_location = self.cells[key_str] #type: BlockLocation
                    block_type = block_location.mc_type[0:(width-1)]
                line += "{:<{width}}".format(block_type, width=width)
            lines.append(line)
        return lines

    def clear(self):
        self.cells.clear()
        self.type_to_location.clear()

    def update_block_location(self, location: BlockLocation):
        key = location.int_key_str()
        old_type = None

        if key in self.cells:
            cell = self.cells[key]
            old_type = cell.type
            cell.update(location)
        else:
            cell = location.copy()
            self.cells[key] = cell

        new_type = cell.type
        self.__update_type_to_location(old_type, new_type, cell)
        if new_type not in self.already_seen:
            self.newly_seen_since_last_request.add(new_type)
            self.already_seen.add(new_type)

    def update_entity_location(self, location: EntityLocation):
        key = location.int_key_str()
        old_type = None

        if key in self.cells:
            cell = self.cells[key]
            cell.update(location)
            old_type = cell.type
        else:
            cell = location.copy()
            self.cells[key] = cell

        new_type = cell.type
        self.__update_type_to_location(old_type, new_type, cell)
        if new_type not in self.already_seen:
            self.newly_seen_since_last_request.add(new_type)
            self.already_seen.add(new_type)

    def __update_type_to_location(self, old_type: str, new_type: str, location: Location):
        if not isinstance(location, Location):
            raise RuntimeError("expected location to be a location instead of {}".format(type(location)))

        if old_type != new_type:
            if old_type in self.type_to_location:
                old_type_locs = self.type_to_location[old_type]
                old_type_locs.remove(location)
                if len(old_type_locs) == 0:
                    del self.type_to_location[old_type]
                else:
                    self.type_to_location[old_type] = old_type_locs

            if new_type in self.type_to_location:
                new_type_locs = self.type_to_location[new_type]
                new_type_locs.append(location)
                self.type_to_location[new_type] = new_type_locs
            else:
                self.type_to_location[new_type] = [location]

