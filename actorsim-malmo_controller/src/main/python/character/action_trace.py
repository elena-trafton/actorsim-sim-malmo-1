
import threading
from typing import List

import collections

from location import Location, NULL_LOCATION
from utils.plan_utils import ProblemDescription


class TraceList:
    """A class for collecting trace_list information during the character's lifetime."""

    def __init__(self):
        self.trace_list = list() #type: List
        self.lock = threading.Lock() #type: threading.Lock

    def append(self, entry: 'Trace'):
        """Appends entry to the trace_list"""
        if isinstance(entry, collections.Sequence):
            self.append_all(entry)
        else:
            self.lock.acquire()
            self.trace_list.append(entry)
            self.lock.release()

    def append_all(self, entries: List['Trace']):
        for entry in entries:
            self.append(entry)

    def get_and_reset(self):
        self.lock.acquire()
        old_trace = self.trace_list
        self.trace_list = list()
        self.lock.release()
        return old_trace

    def print(self, logger):
        self.lock.acquire()
        for trace in self.trace_list:  # type: Trace
            if len(trace.newly_seen_items) > 0:
                logger.info("{}".format(trace))
        self.lock.release()

    def get_recent_primitive_actions(self, limit = 10, unique_only = True) -> List['Trace']:
        trace_subset = list()  # List[Trace]

        with self.lock:
            last_primitive = ""
            for trace in reversed(self.trace_list): # type: ActionTrace  reversed returns iterator not copy
                trace_primitive = trace.primitive_action_name
                keep_trace = False
                if len(trace.newly_seen_items) > 0:
                    keep_trace = True

                if trace_primitive != "":
                    if unique_only:
                        if last_primitive != trace_primitive:
                            keep_trace = True
                    else:
                        keep_trace = True

                if keep_trace:
                    trace_subset.append(trace)
                    last_primitive = trace_primitive

                if len(trace_subset) >= limit:
                    break

        return trace_subset

class Trace:
    pass


class PlanTrace(Trace):
    """A structure holding the trace_list information we desire to capture for plans."""
    def __init__(self, problem: ProblemDescription):
        self.problem = problem

    def __repr__(self):
        return "state_index:{}  PLAN TRACE FORMATE NOT YET DEFINED".format(self.problem.state_index)

    def __str__(self):
        return self.__repr__()


class ActionTrace(Trace):
    """A structure holding the trace_list information we desire to capture."""
    def __init__(self):
        self.mc_time = 0 # type: int
        self.behavior_name = "" # type: str

        self.action_name = ""
        self.action_classname = ""
        self.primitive_action_name = "" # type: str
        self.primitive_action_params = "" # type: str
        self.action_target = NULL_LOCATION

        self.commands = "" # type: str

        self.newly_seen_items = None
        self.applied_state_index = -1  # type: int

    def clone(self):
        observation = ActionTrace()
        observation.mc_time = self.mc_time
        observation.applied_state_index = self.applied_state_index
        observation.behavior_name = self.behavior_name

        observation.action_name = self.action_name
        observation.action_classname = self.action_classname
        observation.primitive_action_name = self.primitive_action_name
        observation.primitive_action_params = self.primitive_action_params
        observation.action_target = self.action_target.copy() if isinstance(self.action_target,
                                                                            Location) else self.action_target
        observation.commands = self.commands
        observation.newly_seen_items = self.newly_seen_items
        return observation

    def __repr__(self):
        behavior_str = ""
        if self.behavior_name != "":
            behavior_str = "behavior:{}".format(self.behavior_name)
        newly_seen_str = "("
        seperator = ""
        for item in self.newly_seen_items:
            newly_seen_str += "{}{}".format(seperator, item)
            seperator = ", "
        newly_seen_str += ")"
        result = "time:{}|  {}|  action:{}|  seen:{}".format(self.mc_time, behavior_str, self.action_name, newly_seen_str)
        return result

    def __str__(self):
        return self.__repr__()


class ControlTrace(Trace):
    """A structure holding the control loop trace."""
    def __init__(self):
        self.cycle_start = 0
        self.cycle_end = 0
        self.cycle_time = 0

        self.mc_time = 0

        self.action_trace = ActionTrace() # type: ActionTrace

NULL_CONTROL_TRACE = ControlTrace()