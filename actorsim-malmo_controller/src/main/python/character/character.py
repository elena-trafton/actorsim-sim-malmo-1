import datetime
import math
import time
import traceback
from threading import Thread

from past.utils import old_div

from action import ComplexAction_DEPRECATED, ActionBase
from character.action_trace import ActionTrace, TraceList, NULL_CONTROL_TRACE
from character.command import Command
from character.memory import CharacterMemory, MemoryObject, ConvertWorldStateOptions
from character.state import CharacterState
from connector import MinecraftConnector
from count_down_latch import CountDownLatch
from location import Location, NULL_LOCATION, EntityLocation, BlockLocation, NullLocation
from loggers import MalmoLoggers
from malmo_types import MalmoTypeManager
from mission import Mission

logger = MalmoLoggers.character_logger
obs_logger = MalmoLoggers.obs_logger
calc_logger = MalmoLoggers.calc_logger


class Character:
    STATE_PRINT_INTERVAL_IN_GAME_TICKS = 30  # there are 20 game ticks per second

    def __init__(self, mission: Mission, name="Alex"):
        self.connector = MinecraftConnector()
        MalmoTypeManager.MOB_TYPES.append(name)  # add self as a mob
        self.mission = mission  # type: Mission
        self.name = name
        self.command = Command(mission, self.name)  # type: Command
        self._memory = CharacterMemory(self, mission)  # type: CharacterMemory
        self.action_trace_list = TraceList()
        self.plan_trace_list = TraceList()
        self.role = 0
        self.experimentId = "0"

        # worker thread and hooks
        self.action_queue = list() #type: List[ActionBase]
        self._keep_running = True
        self.threads_to_start_after_mission_starts = list()
        self.add_thread_to_start_with_mission(Character.Worker(self))
        self.cognitive_loop_countdown_latch = CountDownLatch(0)
        self.current_trace = NULL_CONTROL_TRACE
        self._post_hook_005_cycle_start = list()
        self._post_hook_010_observation = list()
        self._post_hook_015_told_observations = list()
        self._post_hook_020_actions_selected = list()
        self._post_hook_030_action_trace = list()
        self._post_hook_040_command_evaluate = list()
        self._post_hook_050_send_commands = list()

        self.add_post_hook_005_cycle_start(Character.log_pause_cycle_at_start)
        self.add_post_hook_005_cycle_start(Character.log_cycle_start)
        self.add_post_hook_010_observation(Character.log_post_observation_start)
        self.add_post_hook_050_send_commands(Character.log_command)

    def add_thread_to_start_with_mission(self, worker_tread):
        self.threads_to_start_after_mission_starts.append(worker_tread)

    def _start_external_treads(self):
        threads_to_start = self.threads_to_start_after_mission_starts
        for thread in threads_to_start:
            thread.start()

    def mission_running(self):
        return self.connector.world_state.is_mission_running and self._keep_running

    #####################################################
    #
    #  The Character's Cognitive Loop
    #
    #####################################################
    class Worker(Thread):
        def __init__(self, character: 'Character'):
            Thread.__init__(self)
            self.character = character
            self.connector = character.connector
            self.last_cycle_start = 0
            old_name = self.getName()
            self.name = "Cog_{}_{}".format(self.character.name, old_name)

        def run(self):
            character = self.character
            while character.mission_running():
                try:
                    character.command.reset_turn()
                    character.command.reset_move()
                    trace = character.get_new_trace_object()
                    trace.cycle_start = datetime.datetime.now()

                    character._run_post_hooks_005_cycle_start()

                    # Perceive
                    new_state = character.process_observation()
                    # clients wanting to notification of new state should register a hook_010_observation
                    character._run_post_hooks_010_observation()

                    character.process_told_observations(new_state)
                    character._run_post_hooks_015_told_observations()

                    # Think - is completed by goal manager and plan manager threads
                    #         which should enqueue actions for the character to execute

                    character.update_action_trace_list(trace, new_state)
                    character.run_post_hook_030_action_trace()

                    # Act
                    character.execute_actions()
                    character.run_post_hook_040_execute_actions()

                    character.send_commands()
                    character.run_post_hook_050_send_commands()

                    trace.cycle_end = datetime.datetime.now()
                    trace.cycle_time = trace.cycle_end - trace.cycle_start

                    if not character._keep_running:
                        break
                except Exception as e:
                    logger.error("exception occurred:{}".format(e))
                    logger.error("{}".format(traceback.format_exc()))

            logger.debug("{} finished run".format(self.name))

    def start_mission(self, ports=[10000]):
        """Initializes minecraft, generates a mission spec and attempts to start the mission."""
        self.connector.ports = ports
        self.connector.initialize_minecraft()
        self.mission.generate_mission_spec()
        if MalmoLoggers.log_mission_spec:
            level = logger.getEffectiveLevel()
            logger.log(level, "MissionSpec:\n{}".format(self.mission.spec))
        self.connector.start_sim(self.mission.spec, self.experimentId, self.role)

        if self.connector.world_state.is_mission_running:
            logger.info("Starting threads")
            self._start_external_treads()

    def stop_mission(self):
        self.command.set_quit()
        self._keep_running = False

    def wait_on_mission_start(self, sleep_between_checks_in_seconds=1):
        logger.info("Waiting on mission start...")
        while not self.connector.world_state.is_mission_running:
            time.sleep(sleep_between_checks_in_seconds)

    def wait_on_mission_end(self, sleep_between_checks_in_seconds=1):
        while self.connector.world_state.is_mission_running:
            time.sleep(sleep_between_checks_in_seconds)

    def wait_for_item_in_inventory(self, item, desired=1, sleep_between_checks_in_seconds=1, quit_when_complete=True):
        while self.connector.world_state.is_mission_running:
            time.sleep(sleep_between_checks_in_seconds)
            count = self.get_inventory().amount_of(item)
            if count >= desired:
                logger.debug("inventory has {} item {}, quitting mission".format(count, item))
                if quit_when_complete:
                    self.command.set_quit()
                break

    def mission_running(self):
        return self.connector.world_state.is_mission_running

    def get_new_trace_object(self):
        trace = ActionTrace()
        return trace

    def update_action_trace_list(self, trace: ActionTrace, latest_state: CharacterState):
        from utils.plan_manager import determine_primitive_from_action

        """Capture the state of the world and next action just before it is executed."""
        trace.mc_time = latest_state.world_time
        trace.applied_state_index = latest_state.state_index

        # capture any desired state here!
        if len(self.action_queue) > 0:
            active_action = self.action_queue[0]

            if isinstance(active_action, ComplexAction_DEPRECATED):
                behavior = active_action
                trace.behavior_name = behavior.name
                if behavior.last_action is not None:
                    trace.action_name = behavior.last_action.name
                    trace.action_classname = behavior.last_action.__class__.__name__
                    determine_primitive_from_action(trace, behavior.last_action)

            else:
                trace.action_name = active_action.name
                trace.action_classname = active_action.__class__.__name__
                determine_primitive_from_action(trace, active_action)

            if hasattr(active_action, "adjusted_target"):
                if active_action.adjusted_target is not None:
                    if not isinstance(active_action.adjusted_target, NullLocation):
                        trace.action_target = active_action.adjusted_target

        trace.newly_seen_items = self._memory.get_recently_seen()

        self.action_trace_list.append(trace)

    def add_post_hook_005_cycle_start(self, method):
        self._post_hook_005_cycle_start.append(method)

    def _run_post_hooks_005_cycle_start(self):
        for method in self._post_hook_005_cycle_start:
            method(self)

    def add_post_hook_010_observation(self, method):
        self._post_hook_010_observation.append(method)

    def _run_post_hooks_010_observation(self):
        for method in self._post_hook_010_observation:
            method(self)

    def add_post_hook_015_told_observations(self, method):
        self._post_hook_015_told_observations.append(method)

    def _run_post_hooks_015_told_observations(self):
        for method in self._post_hook_015_told_observations:
            method(self)

    def add_post_hook_020_actions_selected(self, method):
        self._post_hook_020_actions_selected.append(method)

    def _run_post_hook_020_actions_selected(self):
        for method in self._post_hook_020_actions_selected:
            method(self)

    def add_post_hook_030_action_trace(self, method):
        self._post_hook_030_action_trace.append(method)

    def run_post_hook_030_action_trace(self):
        for method in self._post_hook_030_action_trace:
            method(self)

    def add_post_hook_040_command_evaluate(self, method):
        self._post_hook_040_command_evaluate.append(method)

    def run_post_hook_040_execute_actions(self):
        for method in self._post_hook_040_command_evaluate:
            method(self)

    def add_post_hook_050_send_commands(self, method):
        self._post_hook_050_send_commands.append(method)

    def run_post_hook_050_send_commands(self):
        for method in self._post_hook_050_send_commands:
            method(self)

    @staticmethod
    def log_cycle_start(character: 'Character'):
        logger.debug("{}: ".format(character.name))
        logger.debug("{}: ".format(character.name))
        logger.debug("{}: ".format(character.name))
        logger.debug("{}: ^^^^^ starting the cogitive cycle .........................................................................".format(character.name))

    @staticmethod
    def log_pause_cycle_at_start(character: 'Character'):
        logger.debug("{}: checking the count down latch for pause".format(character.name))
        character.cognitive_loop_countdown_latch.await(logger)

    @staticmethod
    def log_post_observation_start(character: 'Character'):
        latest_state = character.get_latest_state()
        logger.debug("{}: post observation at time:{} ............".format(character.name, latest_state.world_time))

    @staticmethod
    def log_command(character: 'Character'):
        c = character
        s = character.get_latest_state()
        if not hasattr(character, 'last_print'):
            c.last_print = 0

        if MalmoLoggers.log_last_command:
            level = obs_logger.getEffectiveLevel()
            if len(c.action_queue) > 0:
                logger.log(level, "action: {}".format(c.action_queue[0]))
            else:
                logger.log(level, "no active action")

            logger.log(level, "command:{}".format(c.command.last_command))

        if c.STATE_PRINT_INTERVAL_IN_GAME_TICKS > 0:
            if (s.world_time - c.last_print) >= c.STATE_PRINT_INTERVAL_IN_GAME_TICKS:
                look_props_str = ""
                if len(s.look_properties) > 0:
                    look_props_str += "_("
                    sep = ""
                    for look_property in s.look_properties.items():
                        key = look_property[0].replace("prop_", "")
                        value = look_property[1]
                        look_props_str += "{}{}={}".format(sep, key, value)
                        sep = ", "
                    look_props_str += ")"

                logger.info("{} t:{} l:{} look:{}_{}{}"
                            .format(s.name, s.world_time, s.location, s.look_hit_type, s.look_at, look_props_str))
                logger.info("  action:{}".format(c.action_queue))
                logger.info("  cmd:{!r}".format(c.command))
                c.last_print = s.world_time

    #####################################################
    #
    #  The Character's Action Queue
    #
    #####################################################

    def clear_action_queue(self):
        self.action_queue.clear()

    def enqueue_action(self, action : ActionBase):
        self.action_queue.append(action)

    def execute_actions(self):
        for action in self.action_queue:
            action.execute()
            if action.is_complete():
                logger.debug("{}: action is complete, removing {}".format(self.name, action))
                self.action_queue.remove(action)
            if not action.is_consistent:
                logger.debug("{}: action is inconsistent, removing {}".format(self.name, action))
                self.action_queue.remove(action)

    def send_commands(self):
        self.command.send_commands(self.connector.agent_host)

    #####################################################
    #
    #  Memory and Observations
    #
    #####################################################

    def process_observation(self):
        obs, image, reward = self.connector.get_observation()
        new_state = CharacterState(self)
        new_state.process_observation(obs, self._memory)

        return new_state

    def process_told_observations(self, new_state: CharacterState):
        self._memory.process_told_observations(new_state)


    def get_block_observation_grid(self):
        grid_names = self.mission.char_to_grids[self.name]
        assert(len(grid_names) == 1)  # assume we only return one; otherwise this should calculate max
        return self.mission.observation_grids[grid_names[0]]

    def get_entity_observation_grid(self):
        grid_names = self.mission.char_to_ents[self.name]
        assert(len(grid_names) == 1)  # assume we only return one; otherwise this should calculate max
        return self.mission.ents_to_details[grid_names[0]]

    def tell(self, location: Location):
        """Tell the character about something at a location"""
        logger.debug("Told about {}".format(location))
        self._memory.tell(location)

    def get_latest_state(self) -> CharacterState:
        return self._memory.get_latest_state()

    def get_location(self) -> EntityLocation:
        return self.get_latest_state().location

    def get_height(self) -> EntityLocation:
        return self.get_latest_state().height

    def get_current_time(self) -> float:
        return self.get_latest_state().world_time

    def get_look_hit_type(self):
        latest_state = self.get_latest_state()  # type: CharacterState
        return latest_state.look_hit_type

    def get_look_in_range(self):
        latest_state = self.get_latest_state()  # type: CharacterState
        return latest_state.look_in_range

    def get_look_distance(self):
        latest_state = self.get_latest_state()  # type: CharacterState
        return latest_state.look_distance

    def get_actual_eye_height(self):
        latest_state = self.get_latest_state()  # type: CharacterState
        return latest_state.location.y + latest_state.height

    def get_inventory(self):
        latest_state = self.get_latest_state()  # type: CharacterState
        inventory = latest_state.inventory
        return inventory

    def get_last_entity_update_time(self):
        latest_entity_update = self._memory.get_latest_entity_update_index()
        return latest_entity_update

    def get_observation(self, id) -> MemoryObject:
        memory_object = self._memory.get_observation(id)
        return memory_object

    def get_block_type_at(self, target):
        block_type = self._memory.block_type_at(target)
        return block_type

    def get_closest_entity_location(self, entity_type):
        entity_location = self._memory.get_closest_entity_location(entity_type)
        return entity_location

    def get_closest_block_location(self, target_type):
        block_location = self._memory.get_closest_block_location(target_type)
        return block_location

    def has_observed(self, type, at_least_one_valid_location=True):
        return self._memory.has_observed(type, at_least_one_valid_location=at_least_one_valid_location)

    def get_selected_hotbar_slot(self):
        selected = self._memory.get_selected_hotbar_slot()
        return selected

    def set_selected_hotbar_slot(self, slot):
        return self._memory.set_selected_hotbar_slot(slot)

    def get_closest_drop_location(self, type):
        location = self._memory.get_closest_drop_location(type)
        return location

    def amount_of_item_in_inventory(self, item_name):
        inventory = self.get_inventory()
        return inventory.amount_of(item_name)

    def inventory_contains(self, item_name):
        inventory = self.get_inventory()
        return inventory.contains(item_name)

    def has_plan_state(self, state_index):
        return self._memory.has_plan_state(state_index)

    def get_plan_state(self, state_index):
        return self._memory.get_plan_state(state_index)

    def has_state(self, state_index):
        return self._memory.has_state(state_index)

    def get_state(self, state_index):
        return self._memory.get_state(state_index)

    def register_new_plan_state(self, state, plan_state):
        self._memory.register_new_plan_state(state, plan_state)

    def visit_observed_objects(self, visit_func_callback, visit_func_kwparams):
        self._memory.visit_observed_objects(visit_func_callback, visit_func_kwparams)

    def get_observed_chests(self):
        chests = self._memory.get_observed_chests()
        return chests

    def UNIT_TEST_ONLY_get_memory(self):
        return self._memory

    def calc_yaw_pitch_distance(self, target):
        current_loc = self.get_location()  # type: EntityLocation
        height = self.get_height()
        return calc_yaw_pitch_distance(current_loc, height, target)

    def calc_pitch(self, target_location: Location, distance: float):
        current_loc = self.get_location()  # type: EntityLocation
        height = self.get_height()
        return calc_pitch(current_loc, height, target_location, distance)

    def calc_pitch_velocity(self, target_pitch, scale=90.0):
        current_loc = self.get_location()  # type: EntityLocation
        return calc_pitch_velocity(current_loc.pitch, target_pitch, scale=scale)

    def calc_yaw_velocity(self, target_yaw, scale=90.0):
        current_loc = self.get_location()  # type: EntityLocation
        return calc_yaw_velocity(current_loc.yaw, target_yaw, scale=scale)


    def calc_move_and_turn_velocities(self, distance, yaw, distance_threshold,
                                      yaw_threshold=1.0,
                                      yaw_threshold_for_moving=30,
                                      distance_scale=3):
        current_loc = self.get_location()  # type: EntityLocation
        return calc_move_and_turn_velocities(current_loc, distance, yaw, distance_threshold,
                                      yaw_threshold=yaw_threshold,
                                      yaw_threshold_for_moving=yaw_threshold_for_moving,
                                      distance_scale=distance_scale)


    def find_best_block_look_location(self, target: Location):
        current_loc = self.get_location()  # type: EntityLocation
        return find_best_block_look_location(current_loc, target)


    def find_best_location(self, target: Location, block_size=1):
        current_loc = self.get_location()  # type: EntityLocation
        return find_best_location(current_loc, target, block_size=block_size)

    def create_world_state(self, state: CharacterState, options: ConvertWorldStateOptions):
        return self._memory.create_world_state(state, options)

    def toJSON(self, observation_reporting_interval):
        return self._memory.toJSON(observation_reporting_interval=observation_reporting_interval)


    def blocks_to_check(self, pose: Location):  # This will return a set of 2d points
        # Convert yaw and pose from polar to cartesian space
        step_dist = 1.0 # This is a constant, and represents roughtly two blocks worth of traveling
        yaw = math.pi/180.0 * pose.yaw
        dz = step_dist * math.cos(yaw) #+ pose.x
        dx = step_dist * -math.sin(yaw) #+ pose.z

        block_in_front = [pose.x + dx, pose.z + dz]

        block_list = []

        block_list.append([math.floor(pose.x + dx), math.floor(pose.z + dz)])

        return block_list

    def is_cliff(self, check):
        blocks_to_avoid = ['air', 'tallgrass']
        char_loc = self.get_location()
        for block in check:
            down_two = self.get_block_type_at(Location(block[0], char_loc.y-2, block[1]))
            down = self.get_block_type_at(Location(block[0], char_loc.y-1, block[1]))

            return (down in blocks_to_avoid) and (down_two in blocks_to_avoid)

    def is_water(self, check):
        blocks_to_avoid = ['water', 'lava']
        char_loc = self.get_location()
        water_in_front = False
        for block in check:
            for height in range(-1, 4):
                facing = self.get_block_type_at(Location(block[0], char_loc.y-height, block[1]))
                facing_water = (facing in blocks_to_avoid)

                water_in_front = water_in_front or facing_water

        return water_in_front

    def facing_danger(self):
        char_loc = self.get_location()
        check = self.blocks_to_check(char_loc)
        return self.is_cliff(check) or self.is_water(check)


#####################################################
#
#  Navigation Helpers
#
#####################################################


def calc_yaw_pitch_distance(current_loc: EntityLocation, height, target: Location):
    cur_x = current_loc.x
    cur_y = current_loc.y
    cur_z = current_loc.z
    tar_x = target.x
    tar_y = target.y
    tar_z = target.z

    dx = tar_x - cur_x
    dy = tar_y - cur_y
    dz = tar_z - cur_z
    yaw = -180 * math.atan2(dx, dz) / math.pi
    calc_logger.debug("     yaw to target (absolute): {}".format(yaw))
    distance = math.sqrt(dx * dx + dy * dy + dz * dz)


    pitch = calc_pitch(current_loc, height, target, distance)
    calc_logger.debug("d ({:.2f},{:.2f}) ({:.2f},{:.2f})={:.2f}".format(cur_x, cur_z, tar_x, tar_z, distance))
    return yaw, pitch, distance


def calc_pitch(current_loc: EntityLocation, height, target_location: Location, distance: float):
    cur_y = current_loc.y + height
    tar_y = target_location.y

    # rey's version: focused on zombies
    # pitch = math.atan2(((cur_y + 1.625) - (tar_y + target_height * 0.9)), distance) * 180.0 / math.pi

    pitch = math.atan2((cur_y - tar_y), distance) * 180.0 / math.pi
    return pitch


def calc_move_and_turn_velocities(current_loc: EntityLocation, distance, yaw, distance_threshold,
                                  yaw_threshold=1.0,
                                  yaw_threshold_for_moving=30,
                                  distance_scale=3):
    """
    Calculates the velocities for moving and turning the character to a target.
    Using a lower distance_scale results in faster movement.
    """
    calc_logger.debug("calculating velocity for object at distance {}".format(distance))
    calc_logger.debug("current yaw {} and want to be at yaw {}".format(current_loc.yaw, yaw))
    if distance < distance_threshold:
        translation_velocity = 0
        if abs(yaw - current_loc.yaw) > yaw_threshold:
            rotation_velocity = calc_yaw_velocity(current_loc.yaw, yaw)
        else:
            rotation_velocity = 0

    else:
        if abs(normalize_angle(yaw - current_loc.yaw)) < yaw_threshold_for_moving:
            translation_velocity = calc_dist_velocity(distance, scale=distance_scale)
        else:
            translation_velocity = 0

        rotation_velocity = calc_yaw_velocity(current_loc.yaw, yaw)

    calc_logger.debug("returning translate:{:.4f} rotate:{:.4f}".format(translation_velocity, rotation_velocity))

    return translation_velocity, rotation_velocity


def calc_pitch_velocity(current_pitch, target_pitch, scale=90.0):
    """Use sigmoid function to choose a delta that will help smoothly steer from current angle to target angle."""
    delta, vel = _calc_angle_velocity(current_pitch, target_pitch, scale=scale)
    calc_logger.debug("     pitch delta:{} vel:{}".format(delta, vel))
    return vel


def calc_yaw_velocity(current_yaw, target_yaw, scale=90.0):
    """Use sigmoid function to choose a delta that will help smoothly steer from current angle to target angle.
    """
    delta, vel = _calc_angle_velocity(current_yaw, target_yaw, scale=scale)
    calc_logger.debug("     yaw delta:{} vel:{}".format(delta, vel))
    return vel


def _calc_angle_velocity(start, end, scale=90.0):
    """Calculates the angular velocity between a start and end and scales it by scale.
       """
    delta = normalize_angle(end - start)
    while delta < -180:
        delta += 360
    while delta > 180:
        delta -= 360
    # rey's version: focused on zombies
    # vel = (old_div(2.0, (1.0 + math.exp(old_div(-delta, scale))))) - 1.0

    delta_scale = old_div(delta, scale)
    vel = delta_scale / (1 + delta_scale * delta_scale)
    return delta, vel


def calc_dist_velocity(distance, scale=3):
    """Calculate a distance velocity that will move toward target smoothly.
       Using a lower scale results in faster movement.
    """
    vel = (old_div(2.0, (1.0 + math.exp(old_div(-distance, scale))))) - 1.0
    calc_logger.debug("     translate dist:{} and vel:{}".format(distance, vel))

    return vel


def find_best_look_location(target: Location):
    """Sets the look location of a block to the be the center of the top of the block"""
    best_x = target.x + 0.49
    best_y = target.y
    best_z = target.z + 0.49
    return best_x, best_y, best_z


def find_best_block_look_location(current_loc: Location, target: Location):
    """Sets the look location of a block to the be the center of the top of the block"""
    best_x = target.x + 0.49
    best_z = target.z + 0.49
    best_y = target.y + 0.49  # look in the center of the target unless otherwise changed below

    current_height = current_loc.y
    height_diff = current_height - target.y
    if height_diff < 0:
        logger.debug("target is below character, look at top")
        best_y = target.y
    elif height_diff > 3:
        logger.debug("target is above character, look at bottom of target")
        best_y = target.y - 1
    else:
        logger.debug("target is beside character, already looking at middle of target")

    return best_x, best_y, best_z


def find_best_entity_look_location(target: Location, height=0.5):
    """Sets the look location for an entity to be the height of the entity."""
    best_x = target.x
    best_y = target.y + height
    best_z = target.z
    return best_x, best_y, best_z


def find_best_location(current_loc: Location, target: Location, block_size=1):
    """
    :param target:
    :param block_size:
    :return:
    """
    cur_x = current_loc.x
    cur_z = current_loc.z
    target_x = target.x
    target_z = target.z

    # go to center of block, not where it spawned
    target_x = target_x + 0.5
    target_z = target_z + 0.5

    bound = 0.8 * block_size

    min_distance = 100000000
    best_x = cur_x
    best_z = cur_z
    for z in [-bound, bound]:
        dx = target_x - cur_x
        dz = (target_z + z) - cur_z
        distance = math.sqrt(dx * dx + dz * dz)
        if distance < min_distance:
            min_distance = distance
            best_x, best_z = target_x, target_z + z

    for x in [-bound, bound]:
        dx = (target_x + x) - cur_x
        dz = target_z - cur_z
        distance = math.sqrt(dx * dx + dz * dz)
        if distance < min_distance:
            min_distance = distance
            best_x, best_z = target_x + x, target_z

    dx = target_x - best_x
    dz = target_z - best_z
    best_yaw = -180 * math.atan2(dx, dz) / math.pi

    return best_x, best_z, best_yaw


def normalize_angle(angle):
    while angle < -180:
        angle += 360
    while angle > 180:
        angle -= 360
    return angle