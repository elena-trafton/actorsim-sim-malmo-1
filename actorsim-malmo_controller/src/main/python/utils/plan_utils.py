from utils.LogManager import LogManager
from malmo_options import MalmoOptions

logger = LogManager.get_logger('actorsim.planner')


class DomainDescription():
    def __init__(self):
        self.domain_filename = None
        self.domain_string = None


class PlanResult():
    def __init__(self):
        self.success = False
        self.planner_output = ""
        self.plan = None

class ProblemDescription():
    def __init__(self, character: 'Character'):
        self.character = character # type: 'Character'
        self.state_index = None  # type: float
        self.goal_predicate = None
        self.task_to_decompose = None  # type: list
        self.partial_plan = None
        self.result = PlanResult()  # type: PlanResult
        self.shop_lines = None

class PlanRecProblemDescription:
    def __init__(self, character: 'Character'):
        self.planning_problem = ProblemDescription(character)
        self.planning_problem.task_to_decompose = ["(tlt)"]
        self.observed_plan = None
        self.goals_recognized = []
