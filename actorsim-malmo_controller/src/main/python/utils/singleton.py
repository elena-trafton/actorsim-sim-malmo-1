class Singleton(type):
    """Creates a singleton for the associated class.

       Use as a metaclass:

       class Logger(object):
           __metaclass__ = Singleton

    """

    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
