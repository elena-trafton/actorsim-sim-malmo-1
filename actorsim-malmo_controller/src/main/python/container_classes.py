from abc import ABC

from location import NULL_LOCATION
from loggers import MalmoLoggers

logger = MalmoLoggers.inventory_logger


class Container(ABC):
    """
    ItemContainer simulates the inventory of any entity that would have one (i.e. the player and chests).
        This class is used to keep track of the state of anything that can hold items, but not add or remove them.
        Any changes to the inventory will be updated with new data from malmo. The container class  is the abstract
        parent of the player inventory class and the block inventory.
    """
    def __init__(self, item_list):
        self._items = item_list  # type: ItemStack

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        container_str = ""
        sep = ""
        for item in self._items:
            if item.name != "air":
                container_str += "{}{}".format(sep, item)
                sep = " "
        return container_str

    def contains(self, item_name):
        return self.index_of(item_name) > -1

    def index_of(self, item_name):
        for index in range(len(self._items)):
            if self._items[index].name == item_name:
                return index
        return -1

    def index_of_all(self, item_name):
        item_index_list = list()
        for index in range(len(self._items)):
            if self._items[index].name == item_name:
                item_index_list.append(index)
                logger.debug("Item {} found at {}".format(item_name, index))
        return item_index_list

    def amount_of(self, item_name):
        item_amount = 0
        for item in self._items:
            if item.name == item_name:
                logger.debug("Item {} found at {} with a stack count of {}".format(item_name, item.index, item.count))
                item_amount += item.count
        return item_amount

    def inventory_size(self):
        return len(self._items)

    def stack_at(self, stack_index):
        return self._items[stack_index]


class PlayerInventory(Container):
    """ This class describes the players inventory which is similar to other inventories just with a hotbar"""
    DEFAULT_CHARACTER_HOT_BAR_SIZE = 9
    DEFAULT_CHARACTER_INVENTORY_SIZE = 36

    def __init__(self, item_list):
        self.hot_bar_size = self.DEFAULT_CHARACTER_HOT_BAR_SIZE
        super(PlayerInventory, self).__init__(item_list)

    def is_in_hot_bar(self, item_name):
        """Returns whether the item is in the hotbar and the zero-based index if it is.
           If the item is not in inventory, returns <false, -1>.
        """
        hotbar_index = self.index_of(item_name)
        return -1 < hotbar_index & hotbar_index < self.hot_bar_size, hotbar_index


class BlockInventory(Container):
    """ This is the basic implementation for a block that can contain items. The focus of this class is chests
        but is designed with other blocks like the forge in mind. This differs from player inventory as it doesnt have
        a hotbar, but does include a location field."""
    DEFAULT_BLOCK_INVENTORY_SIZE = 27

    def __init__(self, item_list, location):
        self.location = location
        super(BlockInventory, self).__init__(item_list)

    def get_location(self):
        return self.location


class ItemStack:
    """ Represents one stack of items, which can be put into any inventory"""
    MAX_STACK_SIZE = 64

    def __init__(self, name='air', count=0, colour=None, index=-1):
        self.name = name
        self.count = count
        self.colour = colour
        self.index = index

    def __repr__(self):
        colour_str = ""
        if self.colour is not None:
            colour_str = "_" + str(self.colour)
        return "{}:{}{}".format(self.name, self.count, colour_str)

    def toJSON(self):
        return dict(name=self.name, count=self.count, colour=self.colour, index=self.index)

    def reset(self):
        self.name = 'air'
        self.count = 0
        self.colour = None


NULL_PlAYER_INVENTORY = PlayerInventory(list())
NULL_BLOCK_INVENTORY = BlockInventory(list(), NULL_LOCATION)
