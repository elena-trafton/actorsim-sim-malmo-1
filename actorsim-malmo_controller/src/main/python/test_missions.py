
import random
from typing import List

from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference, GoalManagerType
from location import Location
from loggers import MalmoLoggers
from mission import Mission, ObservationGridDetails

logger = LogManager.get_logger('actorsim.malmo.mission')

static_plant_wheat_mission = '''<?xml version="1.0" encoding="UTF-8" standalone="no" ?>                                                                                        
            <Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">                                        
                                                                                                                                                             
              <About>                                                                                                                                        
                <Summary>Basic World from Irina</Summary>                                                                                                    
              </About>                                                                                                                                       
                                                                                                                                                             
            <ServerSection>                                                                                                                                  
              <ServerInitialConditions>                                                                                                                      
                <Time>                                                                                                                                       
                    <StartTime>1000</StartTime>                                                                                                              
                    <AllowPassageOfTime>true</AllowPassageOfTime>                                                                                            
                </Time>                                                                                                                                      
              </ServerInitialConditions>                                                                                                                     
              <ServerHandlers>                                                                                                                               
                  <FlatWorldGenerator generatorString="2;7,15x42;1" forceReset="true"/>      
                  <DrawingDecorator>                                                                                                                         
                     <DrawBlock x="10" y="16" z="0" type="dirt"/>   
                     <DrawCuboid x1="11" y1="15" z1="0" x2="11" y2="15" z2="2" type="water"/>                                                                
                     <DrawCuboid x1="10" y1="15" z1="0" x2="10" y2="15" z2="2" type="farmland"/>                                                                
                     <DrawCuboid x1="11" y1="16" z1="0" x2="11" y2="16" z2="2" type="air"/>                                                                  
                  </DrawingDecorator>                                                                                                                        
                  <ServerQuitFromTimeUp timeLimitMs="60000" description="out_of_time"/>                                                                      
                  <ServerQuitWhenAnyAgentFinishes/>                                                                                                          
                </ServerHandlers>                                                                                                                            
              </ServerSection>                                                                                                                               
                                                                                                                                                             
              <AgentSection mode="Survival">                                                                                                                 
                <Name>Companion</Name>                                                                                                                       
                <AgentStart>                                                                                                                                 
                    <Placement x="0" y="18.0" z="0"/>                                                                                                        
                    <Inventory>                                                                                                                              
                       <InventoryObject slot="0" type="diamond_pickaxe"/>                                                                                    
                       <InventoryObject slot="1" type="wheat_seeds"/>                                                                                        
                       <InventoryObject slot="2" type="dye"/>                                                                                                
                       <InventoryObject slot="3" type="dye" colour="WHITE" quantity="12"/>                                                                                                
                   </Inventory>                                                                                                                              
                </AgentStart>                                                                                                                                
                <AgentHandlers>                                                                                                                              
                  <ObservationFromFullStats/>                                                                                                                
                  <ObservationFromGrid>                                                                                                                      
                      <Grid name="blocks3x3">                                                                                                                
                        <min x="1" y="-1" z="-1"/>                                                                                                           
                        <max x="1" y="1" z="1"/>                                                                                                             
                      </Grid>                                                                                                                                
                  </ObservationFromGrid>                                                                                                                     
                  <ContinuousMovementCommands turnSpeedDegs="180" />                                                                                           
                  <!-- <ObservationFromHotBar/> duplicated by inventory -->
                  <ObservationFromFullInventory />
                  <ObservationFromRay/>
                  <MissionQuitCommands/>
                  <ObservationFromNearbyEntities>
                    <Range name="Entities" xrange="30" yrange="5" zrange="30" update_frequency="7"/> 
                  </ObservationFromNearbyEntities>
                  <InventoryCommands/>                                                                                                                       
                </AgentHandlers>                                                                                                                             
              </AgentSection>                                                                                                                                
            </Mission>'''

def get_basic_mission(mission_name, agent="Alex", time_limit_in_ms=60000) -> Mission:
    mission = Mission(mission_name)
    mission.generator_str = "2;7,15x42;1"
    mission.time_in_ms = time_limit_in_ms
    mission.start[agent].y = 16
    mission.add_inventory_item(1, 'iron_sword', 1)
    mission.add_inventory_item(2, 'wheat_seeds', 10)
    mission.start[agent].yaw = -90
    return mission

def get_default_world_mission(mission_name, agent="Alex", time_limit_in_ms=60000, world_seed="0") -> Mission:
    mission = Mission(mission_name)
    mission.generator_str = "2;7,15x42;1"
    mission.time_in_ms = time_limit_in_ms
    mission.is_flat_world = False
    mission.start[agent].y = 16
    mission.start[agent].yaw = -90
    mission.force_reset = True
    mission.seed=world_seed;
    return mission

def get_simple_wheat_mission(agent="Alex", plant=False):
    mission = get_basic_mission("Plant Wheat") #type: Mission
    mission.add_wheat_farm(9, 15, 0, plant=plant)
    mission.clear_inventory()
    mission.add_inventory_item(0, 'iron_hoe', 1)
    mission.add_inventory_item(3, 'wheat_seeds', 5)
    mission.add_inventory_item(4, 'dye', 20, 'WHITE')
    mission.add_inventory_item(9, 'dirt', 16)
    return mission

def get_simple_melon_mission(plant_pumpkin=True, agent="Alex"):
    mission = get_basic_mission("Plant Mellons")
    mission.add_melon_farm(9, 15, 0, plant=plant_pumpkin)
    mission.clear_inventory()
    mission.add_inventory_item(0, 'iron_hoe', 1)
    mission.add_inventory_item(3, 'pumpkin_seeds', 5)
    mission.add_inventory_item(4, 'dye', 20, 'WHITE')
    mission.add_inventory_item(8, 'iron_axe', 1)
    mission.add_inventory_item(9, 'dirt', 16)
    return mission

def get_empty_mission(name, agent="Alex"):
    mission = Mission(name)
    mission.generator_str = "2;7,15x42;1"
    mission.start[agent].y = 16
    mission.start[agent].yaw = -90
    return mission

def get_selfie_mission(agent="Alex"):
    mission = get_basic_mission("Take a selfie!")
    mission.start[agent].x = 10
    mission.start[agent].y = 16
    mission.start[agent].z = -3.5
    mission.start[agent].yaw = -18

    farm_width = 4
    mission.add_melon_farm(9, 15, 0, plant=False, width=farm_width)
    mission.add_line(11, 16, 0, 11, 16, int(farm_width / 2), "pumpkin_stem")
    mission.add_line(11, 16, int(farm_width / 2)+1, 11, 16, farm_width, "melon_stem")
    mission.add_line(12, 16, 0, 12, 16, farm_width, "wheat")
    mission.add_line(13, 16, 0, 13, 16, farm_width, "potatoes")
    mission.add_line(14, 16, 0, 14, 16, farm_width, "carrots")
    # mission.add_line(15, 16, 0, 15, 16, farm_width, "potato")
    mission.clear_inventory()
    mission.add_inventory_item(0, 'iron_hoe', 1)
    mission.add_inventory_item(1, 'dye', 64, 'WHITE')
    mission.add_inventory_item(2, 'wheat_seeds', 1)
    mission.add_inventory_item(3, 'pumpkin_seeds', 1)
    mission.add_inventory_item(4, 'potato', 1)
    mission.add_inventory_item(5, 'melon_seeds', 1)
    mission.add_inventory_item(6, 'beetroot_seeds', 1)
    mission.add_inventory_item(9, 'dirt', 16)
    return mission



def get_multi_block_mission(agent="Alex"):
    mission = Mission("Plant all the wheat")
    mission.generator_str = "2;7,15x42;1"
    mission.start[agent].y = 16
    mission.start[agent].x = 0.5
    mission.start[agent].z = 0.5
    #mission.add_wheat_farm(-2, 15, 0, plant_wheat=False)
    mission.add_block(2, 16, 0, 'dirt')
    mission.add_block(-1, 16, 0, 'farmland')
    mission.add_block(1, 18, 2, 'gold_block')
    mission.add_block(0, 19, 0, 'diamond_block')
    mission.add_block(0, 15, 0, 'stone')
    mission.add_block(1, 15, 0, 'sand')
    mission.add_block(0, 15, -1, 'sandstone')
    mission.add_block(0, 16, -1, 'gravel')
    mission.add_block(-1, 15, 0, 'cobblestone')
    mission.add_inventory_item(0, 'iron_hoe', 1)
    mission.add_inventory_item(1, 'wheat_seeds', 5)
    mission.add_inventory_item(2, 'dye', 20, 'WHITE')
    mission.generate_mission_spec()
    return mission

def complex_planting_mission(agent="Alex"):
    mission = Mission("Plant all the seeds")
    mission.generator_str = "2;7,15x42;1"
    mission.start[agent].y = 16
    mission.start[agent].x = 0.5
    mission.start[agent].z = 0.5
    mission.add_wheat_farm(0, 15, 0)
    mission.add_inventory_item(0, 'iron_hoe', 1)
    mission.add_inventory_item(1, 'dye', 64, 'WHITE')
    mission.add_inventory_item(2, 'wheat_seeds', 1)
    mission.add_inventory_item(3, 'pumpkin_seeds', 1)
    mission.add_inventory_item(4, 'potato', 1)
    mission.add_inventory_item(5, 'melon_seeds', 1)
    mission.add_inventory_item(6, 'beetroot_seeds', 1)
    mission.add_inventory_item(9, 'dirt', 16)
    mission.generate_mission_spec()
    return mission




def get_single_chicken_mission(agent="Alex"):
    mission = Mission("Chicken Run!")
    mission.generator_str = "2;7,15x42;1"
    mission.start[agent].y = 16
    mission.add_inventory_item(0, 'iron_sword', 1)
    mission.start[agent].yaw = -90
    mission.add_chicken(9, 16, 0)
    return mission

def get_two_chicken_mission(agent="Alex"):
    mission = Mission("Chicken Run!")
    mission.generator_str = "2;7,15x42;1"
    mission.start[agent].y = 16
    mission.add_inventory_item(0, 'iron_sword', 1)
    mission.start[agent].yaw = -90
    mission.add_chicken(9.5, 16, 0.5)
    mission.add_chicken(12.5, 16, 1.5)
    return mission

def get_multiple_chickens_mission(num_chickens = 10, center: Location = Location(9, 16, 0), radius = 5, agent="Alex"):
    '''Spawns num_chickes within radius of center'''
    mission = get_basic_mission("Henpecked!", agent)
    add_multiple_chickens(mission, num_chickens, center, radius)

    return mission


def add_multiple_chickens(mission: Mission, num_chickens = 10, center: Location = Location(9, 16, 0), radius = 5):
    min_x = center.x - radius
    max_x = center.x + radius
    min_z = center.z - radius
    max_z = center.z + radius
    mission.add_decorator_comment("Chickens!")
    for chicken_num in range(num_chickens):
        random_x = random.randint(min_x, max_x)
        random_z = random.randint(min_z, max_z)
        mission.add_chicken(random_x, 16, random_z)
        mission.add_item(random_x, 16, random_z, "egg")


def get_multiple_cows_mission(num_cows = 10, center: Location = Location(9, 16, 0),
                              radius = 5, agent="Alex", drop_height_for_easier_kill=10):
    '''Spawns num_chickes within radius of center'''
    mission = get_basic_mission("Bull-ony", agent)
    add_multiple_cows(mission, num_cows, center, radius, drop_height_for_easier_kill)

    return mission


def add_multiple_cows(mission: Mission, num_cows = 10, center: Location = Location(9, 16, 0),
                      radius = 5, drop_height_for_easier_kill=10):
    min_x = center.x - radius
    max_x = center.x + radius
    min_z = center.z - radius
    max_z = center.z + radius
    mission.add_decorator_comment("Cows!")
    for chicken_num in range(num_cows):
        random_x = random.randint(min_x, max_x)
        random_z = random.randint(min_z, max_z)
        mission.add_cow(random_x, 16 + drop_height_for_easier_kill, random_z)  # drop the cows so the
        mission.add_item(random_x, 16, random_z, "milk_bucket")


def get_single_cow_mission(agent="Alex"):
    mission = Mission("Bull-ony!")
    mission.generator_str = "2;7,15x42;1"
    mission.start[agent].y = 16
    mission.add_inventory_item(0, 'iron_sword', 1)
    mission.start[agent].yaw = -90
    mission.add_cow(9, 16, 0)
    return mission

def add_multiple_items(mission: Mission, item, count = 10, colour=None, center: Location = Location(9, 16, 0), radius = 5):
    min_x = center.x - radius
    max_x = center.x + radius
    min_z = center.z - radius
    max_z = center.z + radius
    mission.add_item(center.x, center.y+1, center.z, item, colour)
    mission.add_block(center.x, center.y + 5, center.z, "air") #slow malmo to help spawnitem properly
    for item_num in range(count-1):
        random_x = random.randint(min_x, max_x)
        random_z = random.randint(min_z, max_z)
        y = center.y + 1
        mission.add_item(random_x, y, random_z, item, colour)
        mission.add_block(random_x, center.y + 5, random_z, "air") #slow malmo to help spawnitem properly

def get_exploration_mission(agent="Alex"):
    mission = Mission("LOST!")
    mission.generator_str = "2;7,15X42;1"
    mission.start[agent].x = 25
    mission.start[agent].z = 25
    mission.start[agent].y = 16
    mission.start[agent].yaw = 0
    min_x = 0
    max_x = 50
    min_z = 0
    max_z = 50

    mission.time_in_ms = 30000
    mission.observation_from_grid = True

    return mission

def get_test_farm_mission(x, y, z, agent="Alex", w1=40, w2=30, w3=20):
    mission = Mission("Buy the farm!")
    mission.generator_str = "2;7,15x42;1"
    mission.time_in_ms = 60000
    mission.start[agent].y = 16
    mission.start[agent].x = 0
    mission.add_inventory_item(0, 'iron_sword', 1)
    mission.add_inventory_item(1, 'wheat_seeds', 10)
    mission.start[agent].yaw = -90

    mission.add_coop(-w1, y, z)
    mission.add_sugar(w1, y, z)
    mission.add_pumpkins(x, y, w1)
    mission.add_wheat_field(x, y, -w1)
    mission.add_beet_field(-w2, y, -w2)
    mission.add_potato_field(-w2, y, w2)
    mission.add_woods(w2, y, -w2)
    mission.add_bones(w2, y, w2)
    mission.add_carrot_field(w3, y, w3)

    return mission

def get_simple_world(x, y, z, options, agent="Alex"):
    mission = Mission("Buy the farm!", options=options)
    mission.generator_str = "2;7,15x42;1"
    mission.time_in_ms = 6000
    mission.start[agent].y = 16
    mission.start[agent].x = 0
    mission.start[agent].z = 0
    mission.add_inventory_item(0, 'iron_sword', 1)
    mission.add_inventory_item(1, 'wheat_seeds', 10)
    mission.start[agent].yaw = -90

    mission.world_generator()

    return mission

def get_empty_world(x, y, z, options, agent="Alex"):
    mission = Mission("Buy the farm!", options=options)
    mission.generator_str = "2;7,15x42;1"
    mission.time_in_ms = 6000
    mission.start[agent].y = 16
    mission.start[agent].x = 0
    mission.start[agent].z = 0
    mission.add_inventory_item(0, 'iron_sword', 1)
    mission.add_inventory_item(1, 'wheat_seeds', 10)
    mission.start[agent].yaw = -90
    return mission

def generate_mission(mission_name):
    mission = None
    if mission_name == "plant_wheat":
        mission = get_simple_wheat_mission(agent="Alex")
    elif mission_name == "simple_world":
        mission = get_simple_world(agent="Alex")
    elif mission_name == "kill_chicken":
        #mission = get_single_chicken_mission(agent="Alex")
        #mission = get_two_chicken_mission(agent="Alex")
        mission = get_multiple_chickens_mission(agent="Alex")
    elif mission_name == "kill_cow":
        mission = get_single_cow_mission(agent="Alex")
    elif mission_name == "slaughter_chicken":
        mission = get_multiple_chickens_mission(agent="Alex")
    elif mission_name == "test_block_breaking":
        mission = get_simple_melon_mission()
    elif mission_name == "test_pumpkin_breaking":
        mission = get_simple_melon_mission()
    elif mission_name == "static_plant_wheat":
        mission = Mission()
        mission.spec = static_plant_wheat_mission
    elif mission_name == "multi_block":
        mission = get_multi_block_mission(agent="Alex")
    elif mission_name == "multi_seeds":
        mission = complex_planting_mission(agent="Alex")
    return mission


def create_basic_chicken_mission(time_in_ms,
                                 center: Location = Location(9, 16, 0)):
    mission = get_multiple_chickens_mission(center=center)
    mission.clear_inventory()
    mission.add_inventory_item(1, 'iron_sword', 1)
    mission.add_inventory_item(2, 'dye', 5, 'WHITE')
    mission.add_inventory_item(3, 'wheat_seeds', 1)
    mission.add_inventory_item(4, 'pumpkin_seeds', 1)
    mission.add_inventory_item(5, 'potato', 1)
    mission.add_inventory_item(6, 'melon_seeds', 1)
    mission.add_inventory_item(7, 'beetroot_seeds', 1)
    mission.add_inventory_item(8, 'carrot', 1)
    mission.add_inventory_item(11, 'milk_bucket', 3)
    mission.add_inventory_item(12, 'sugar', 3)

    mission.time_in_ms = time_in_ms

    return mission

def create_basic_perimeter_mision(mission_name, time_in_ms,
                                  area: Area, item_list: List[str],
                                  base_height):
    mission = get_basic_mission(mission_name)
    mission.clear_inventory()
    mission.add_inventory_item(1, 'iron_sword', 1)
    mission.time_in_ms = time_in_ms

    add_perimeter_items(mission, area, item_list, base_height)

    return mission


def add_perimeter_items(mission, area, item_list, base_height):
    positions = set()
    min_z = area.min_z
    min_x = area.min_x
    max_z = area.max_x
    max_x = area.max_x
    perimeter_size = area.get_perimeter_size()
    num_items = len(item_list)
    spread = int(perimeter_size / (num_items * 2))
    not_in_corner = int(spread / 4)

    for z in (min_z, max_z):
        for x in range(min_x + not_in_corner, max_x, spread):
            positions.add((x, z))
    for x in (min_x, max_x):
        for z in range(min_z + not_in_corner, max_z, spread):
            positions.add((x, z))
    items_to_place = list()
    for position in positions:
        if len(items_to_place) == 0:
            items_to_place = item_list.copy()
            random.shuffle(items_to_place)
            logger.debug("Placing {} items: {}".format(len(items_to_place), items_to_place))

        item_count = items_to_place.pop()
        logger.debug("  Placing item: {}".format(item_count))
        height = base_height

        center = Location(position[0], height, position[1])
        if item_count[0] == "Cow":
            add_multiple_cows(mission, item_count[1], center, radius=3)
        elif item_count[0] == "Chicken":
            add_multiple_chickens(mission, item_count[1], center, radius=3)
        else:
            add_multiple_items(mission, *item_count, center=center, radius=1)


def get_perimeter_mission_items():
    item_list = [
        ("dye", 12, "WHITE"),
        ("sugar", 3),
        ("potato", 3),
        ("wheat_seeds", 3),
        ("carrot", 3),
        ("beetroot_seeds", 3),
        ("pumpkin", 3),

        ("Cow", 3),
        ("Chicken", 3),
    ]
    return item_list


def setup_baseline_experiment_shop(seed : int, food_preference : FoodPreference, explore_at_end=True, ms_per_tick=25):
    """Walk a larger perimeter and behave like a herbivore."""
    MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
    random.seed(seed)

    area = Area(min_x=-45, max_x=45, min_z=-45, max_z=45)
    item_list = get_perimeter_mission_items()
    mission = create_basic_perimeter_mision("walk about", 300000,
                                                          area, item_list, 15)
    mission.add_melon_farm(0, 15, 0, plant=False, width=1)
    mission.ms_per_tick = ms_per_tick

    alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
    mission.observation_grids["Alex3x3"] = alex_grid
    mission.start["Alex"].x = 0
    mission.start["Alex"].z = 0

    factory = CharacterFactory()
    character = factory.create_character(mission,
                                         PlannerType.SHOP,
                                         food_preference=food_preference,
                                         explore_area=area,
                                         include_explore_at_end=explore_at_end)
    return character


def setup_baseline_experiment_actorsim(seed : int):
    """Walk a larger perimeter and behave like a herbivore."""
    MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)
    random.seed(seed)

    area = Area(min_x=-45, max_x=45, min_z=-45, max_z=45)
    item_list = get_perimeter_mission_items()
    mission = create_basic_perimeter_mision("walk about", 300000, area, item_list, 15)
    mission.add_melon_farm(0, 15, 0, plant=False, width=1)
    mission.ms_per_tick = 25

    alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
    mission.observation_grids["Alex3x3"] = alex_grid
    mission.start["Alex"].x = 0
    mission.start["Alex"].z = 0

    factory = CharacterFactory()
    character = factory.create_character(mission,
                                         PlannerType.NONE,
                                         food_preference=FoodPreference.NONE,
                                         goal_manager_type=GoalManagerType.ACTORSIM_GOAL_MANAGER,
                                         explore_area=area,
                                         include_explore_at_end=False)
    return character

