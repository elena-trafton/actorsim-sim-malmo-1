import numpy as np

class Location():
    x = 0
    y = 0
    z = 0

    def __init__(self, x: float = 0, y: float = 0, z: float = 0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return "({}, {}, {})".format(self.x, self.y, self.z)

    def dist(self, other : 'Location' ):
        delta_x = other.x - self.x
        delta_y = other.y - self.y
        delta_z = other.z - self.z
        return np.sqrt((delta_x * delta_x) + (delta_y * delta_y) + (delta_z * delta_z))

    def set(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def as_array(self):
        return np.array([self.x, self.y, self.z])

    def as_array_xz(self):
        return np.array([self.x, self.z])

