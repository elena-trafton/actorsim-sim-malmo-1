import malmoenv
import argparse
from PIL import Image

from character import CharacterState
from mission import build_mission_spec

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Zombie or Apple')
    # parser.add_argument('--mission', type=str, default='missions/mobchase_single_agent.xml', help='the mission xml')
    parser.add_argument('--port', type=int, default=9000, help='the mission server port')
    parser.add_argument('--server', type=str, default='127.0.0.1', help='the mission server DNS or IP address')
    parser.add_argument('--port2', type=int, default=None, help="(Multi-agent) role N's mission port. Defaults to server port.")
    parser.add_argument('--server2', type=str, default=None, help="(Multi-agent) role N's server DNS or IP")
    parser.add_argument('--episodes', type=int, default=10, help='the number of resets to perform - default is 1')
    parser.add_argument('--episode', type=int, default=0, help='the start episode - default is 0')
    parser.add_argument('--role', type=int, default=0, help='the agent role - defaults to 0')
    parser.add_argument('--episodemaxsteps', type=int, default=0, help='max number of steps per episode')
    parser.add_argument('--saveimagesteps', type=int, default=0, help='save an image every N steps')
    parser.add_argument('--resync', type=int, default=0, help='exit and re-sync every N resets'
                                                              ' - default is 0 meaning never.')
    parser.add_argument('--experimentUniqueId', type=str, default='test1', help="the experiment's unique id.")

    parser.add_argument('--actionSelection', type=str, default="random_all", help="the action selection type {random_all, random_union, goal_biased,  - default is RANDOM")
    parser.add_argument('--task', type=str, default="apple", help="the task to complete {apple, caged, zombie, agenda} - default:apple")
    parser.add_argument('--frameskip', type=int, default=4, help='skip N frames every cycle')
    parser.add_argument('--step_penalty', type=float, default=0.1, help='skip N frames every cycle')
    args = parser.parse_args()
    if args.server2 is None:
        args.server2 = args.server

    character = CharacterState()
    action_selection = None
    character.set_task(args.task)
    character.set_action_selection(args.actionSelection)
    task_descriptor = character.get_task_descriptor()
    mission_spec = build_mission_spec(task_descriptor)

    character.main_goal.print_subgoals()

    env = malmoenv.Env(reshape=True)
    env.init(mission_spec, args.port,
             server=args.server,
             server2=args.server2, port2=args.port2,
             role=args.role,
             exp_uid=args.experimentUniqueId,
             episode=args.episode, resync=args.resync)

    for i in range(args.episodes):
        print("reset " + str(i))
        image = env.reset()

        steps = 0
        done = False
        while not done and (args.episodemaxsteps <= 0 or steps < args.episodemaxsteps):
            goal_state = character.main_goal.subgoal_str()

            # TODO: develop more sophisticated action selection
            goal = character.get_lowest_subgoal()
            is_random=1
            action_space = env.action_space
            action = action_space.sample()

            reward = 0.0
            step_done = None
            for i in range(0,args.frameskip):
                image, tmp_reward, step_done, info = env.step(action)
                character.process_observation(info)
                character.determine_achieved_subgoals(debug=0)
                reward -= args.step_penalty
                reward += character.process_rewards()

                goal_name = "---"
                if goal != None:
                    goal_name = goal.goal_type.name
                steps += 1
                if args.saveimagesteps > 0 and steps % args.saveimagesteps == 0:
                    h, w, d = env.observation_space.shape
                    img = Image.fromarray(image.reshape(h, w, d))
                    img.save('image' + str(args.role) + '_' + str(steps) + '.png')


            print("i:{:7d} {} {:<3} ran:{} action:{}".format(steps, goal_state, goal_name[0:3], is_random, action), end="")
            if reward < 0:
                print("        r:{: 0.02f}".format(reward))
            else:
                print(" r:{: 0.02f}".format(reward))

            if step_done:
                done = True
            if character.mission_achieved:
                done = True

            #time.sleep(.05)

    env.close()
