
import numpy as np

from typing import List
from enum import Enum, IntEnum

class ComplexGoalType(IntEnum):
    NONE = 0
    COLLECT = 1
    KILL = 2
    PRACTICE = 3

    # move == 1: 'move 1'
    # move == 2: 'move -1'
    # move == 3: 'turn -.25'
    # move == 4: 'turn .25'
    # move == 5: 'strafe -1'
    # move == 6: 'strafe 1'
    # move > 7: 'attack 1'

class PrimitiveGoalType(Enum):
    NONE = (0, [])
    TARGET = (1, [3, 4])
    APPROACH = (2, [1, 3, 4])
    PICKUP = (3, [1, 3, 4])
    AIM = (4, [3, 4])
    STRIKE = (5, [3, 4, 10, 11])
    KILL = (6, [3, 4, 10, 11])

    def __init__(self, id: int, actions : List[int]):
        self.id = id
        self.actions = actions
        self._action_union = None

    def get_mask(self):
        mask = 0
        if (id >  0):
            mask = np.power(2, id - 1)
        return mask

    def get_action_union(self):
        if self._action_union is None:
            self._action_union = set()
            for goal in PrimitiveGoalType:
                self._action_union.update(goal.actions)
        return self._action_union


class Goal():
    _is_active = False
    _is_achieved = False

    def is_active(self):
        return self._is_active

    def set_active(self, value: bool):
        self._is_active = value

    def set_achieved(self):
        self._is_achieved = True

    def is_achieved(self):
        return self._is_achieved

    def reset(self):
        self._is_achieved = False

class PrimitiveGoal(Goal):
    def __init__(self, goal_type : PrimitiveGoalType):
        self.goal_type = goal_type
        self.strength = 243  # 95% of 256
        self.attempts = 0
        self.successes = 0

    def add_attempt(self, success: bool):
        self.attempts += 1
        if success:
            self.successes += 1

    def get_success_rate(self):
        rate = 0.0
        if self.attempts > 0:
            rate = self.successes / self.attempts
        return rate

    def get_random_action(self):
        action = 0
        actions = self.goal_type.actions
        choice = np.random.randint(len(actions))
        action = actions[choice]
        return action

class ComplexGoal(Goal):
    def __init__(self, goal_type: ComplexGoalType, reward: int, target: str, subgoals : List[PrimitiveGoal]):
        self.goal_type = goal_type
        self.max_reward = reward
        self.target = target
        self.subgoals = subgoals
        self.num_subgoals = len(subgoals)
        if self.num_subgoals < 1:
            raise AttributeError("You need at least one primitive goal in a complex goal.")

    def reset(self):
        super(ComplexGoal, self).reset()
        for subgoal in self.subgoals:
            subgoal.reset()

    def get_reward(self):
        reward = 0
        if self.is_achieved():
            reward = self.max_reward
        elif self.goal_type != ComplexGoalType.PRACTICE:
            achieved = 0
            for subgoal in self.subgoals:
                if subgoal.is_achieved():
                    achieved += 1
                ratio = (achieved * achieved) / (self.num_subgoals * self.num_subgoals * self.num_subgoals)
            reward = self.max_reward * ratio
            #print("achieved:{} ratio:{} reward={}".format(achieved, ratio, reward))
        # print(" ", end="")
        # self.print_subgoals("::", "r:{:0.2}".format(reward), end_in="")
        return reward

    def print_subgoals(self, prefix = "", suffix = "", end_in=""):
        main_achieved = "-"
        if self.is_achieved():
            main_achieved = "X"
        print("{}{}{}".format(prefix, self.target[0], main_achieved), end="")
        for subgoal in self.subgoals:
            achieved = "-"
            if subgoal.is_achieved():
                achieved = "X"
            print("{}{}".format(subgoal.goal_type.name[0], achieved), end="")
        print("{}".format(suffix), end=end_in)

    def subgoal_str(self):
        main_achieved = "-"
        if self.is_achieved():
            main_achieved = "X"
        return_string = "{}{}".format(self.target[0:3], main_achieved)
        for subgoal in self.subgoals:
            achieved = "-"
            if subgoal.is_achieved():
                achieved = "X"
            return_string += "_{}{}".format(subgoal.goal_type.name[0], achieved)
        return return_string

