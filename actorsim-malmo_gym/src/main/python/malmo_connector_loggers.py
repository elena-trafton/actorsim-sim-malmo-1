from actorsim_logging.LogManager import LogManager, QUIET_FORMAT
from actorsim_logging.Loggers import Loggers


class MalmoConnectorLoggers(Loggers):

    malmoenv_connector_root_logger = LogManager.get_logger(Loggers.root_name + '.mec')


