
# The Malmo Connector for ActorSim

This repository houses the standalone connector for Malmo.

The repository follows a [https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html](Maven project structure), where each module has its own pom.xml file.

There are several modules in this repository:
  - actorsim-malmo_controller is required to test or develop controller skills
  - actorsim-malmo_interop is required to connect to ActorSim's Java code to use Temporal Goal Networks
  - actorsim-malmo_gym is a standalone connector to the MalmoEnv code that allows testing Goal-Biased Learning Agenda code; however, it is expected we will unify this with actorsim-malmo_controller in the near future

Archived or infrequently used modules include:
  - actorsim-malmo_shop used to be required if you want to test or develop using the JSHOP2 planner; but relevant code was move to actorsim-malmo_controller
  - actorsim-malmo_companions is a links the controller to the Companions architecture; this was for a 2018 summer project



To get started, see [actorsim-malmo_controller/src/site/INSTALL.md](actorsim-malmo_controller/src/site/INSTALL.md) for detailed installation instructions.

See [actorsim-malmo_controller/src/site/index.md](actorsim-malmo_controller/src/site/index.md) for an index into the full set of documents for this connector.
