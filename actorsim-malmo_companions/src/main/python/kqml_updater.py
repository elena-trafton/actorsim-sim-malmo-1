
import time
from threading import Thread
from threading import Lock
from random import randint

from LogManager import LogManager
from character import Character
from location import Location, NullLocation

import sys
sys.path.append('C:/qrg/companions/v1/pythonian') #NOTE: dependency on Companions
from pythonian import *


logger = LogManager.get_logger("companions.kqml")


class KQMLUpdater(Pythonian):
    #name = "MinecraftAgent_Alex"  # This is the name of the agent to register with

    def __init__(self, character : Character, **kwargs):
        self.name = "MinecraftAgent_" + character.name
        super(KQMLUpdater, self).__init__(**kwargs)

        self.time_between_updates_in_ms = 0.251  #a prime bit more than a quarter second
        self.last_update = 0
        self.character = character
        self.establish_kqml_communication()
        self.worker_thread = KQMLUpdater.Worker(self)
        self.worker_thread.start()



    def establish_kqml_communication(self):
        self.add_achieves()
        self.add_asks()

    def add_asks(self):
       # self.add_ask('get_current_inventory', self.get_current_inventory, '(get_current_inventory ?inventory)')
       # self.add_ask('get_current_grid_map', self.get_current_grid_map, '(get_current_grid_map ?map)')
       # self.add_ask('get_current_entity_map', self.get_current_entity_map, '(get_current_entity_map ?map)')
       # self.add_ask('get_action_list', self.get_action_list, '(get_action_list ?actions)') # information about actions alex is taking (may or may not be necessary/useful/cheating)
        pass

    def add_achieves(self):
        pass

    def character_state_to_knowledge(self):
        c = self.character
        s = c.state
        obs_time = s.world_time
        exp_id = c.experimentId
        name = c.name
        lock = c.observation_lock
        facts = list()

        if obs_time > self.last_update: # only need to send new facts if there's been a new observation
            ist_info_string = "(ist-Information (ObservationMtFn " + str(exp_id) + " " + str(name) + " " + \
                              str(obs_time) + ") "
            lock.acquire()
            grid_map = s.grid_map
            entity_map = s.entity_map
            inventory = s.inventory.items

            alex_trace = c.action_trace_list

            #TODO: get Alex location here (maybe comes from alex_trace?)

            trace_facts = self.get_trace_facts(alex_trace, ist_info_string)
            grid_facts = self.get_grid_map_facts(grid_map, ist_info_string)
            entity_facts = self.get_entity_map_facts(entity_map, ist_info_string)
            inventory_facts = self.get_inventory_facts(inventory, ist_info_string, name)
            lock.release()

            facts += trace_facts
            facts += grid_facts
            facts += entity_facts
            facts += inventory_facts

            self.last_update = obs_time

        return facts

    def get_trace_facts(self, trace, ist_info_string):
        for action in trace:
            behavior_name = action.behavior_name
            action_name = action.action_name
            action_target = action.action_target






    def get_inventory_facts(self, items, ist_info_string, char_name):
        facts = list()
        for i in items:
            if i.name != "air":
                collection = self.get_collection(i.name)
                ent_id = randint(0,10000000)
                size = i.count
                token = collection.lower().replace(" ", "").replace("(", "").replace(")", "") + str(ent_id)
                isa_fact = ist_info_string + "(inventoryFact ((InventoryItemFn  " + collection + ") " + token + \
                    " " + str(size) + "))"
                own_fact = ist_info_string + "(inventoryFact (owns " + char_name + " " + token + ")))"
                facts.append(isa_fact)
                facts.append(own_fact)
        return facts

    def get_entity_map_facts(self, entity_map, ist_info_string):
        facts = list()
        entities = entity_map.cells
        location = Location()
        for e in entities:
            logger.debug("looking for entity with key {}".format(e))
            entity = entities[e]
            location.set_from_key_str(e)
            x = location.x
            y = location.y
            z = location.z

            entity_type = entity.type
            if entity.type != "Alex":
                collection = self.get_collection(entity_type)
                entity_id = entity.id
                token = collection.lower().replace(" ", "").replace("(", "").replace(")", "") + "-" + str(entity_id)
                isa_fact = ist_info_string + "(entityFact (isa " + token + " " + collection + ")))"
                loc_fact = ist_info_string + "(entityFact (on-Physical {} (BlockFn {} {} {}))))".format(token, x, y, z)
                facts.append(isa_fact)
                facts.append(loc_fact)
        return facts

    def get_grid_map_facts(self, grid_map, ist_info_string):
        facts = list()
        blocks = grid_map.cells
        location = Location()
        for b in blocks:
            location.set_from_key_str(b)
            x = location.x
            y = location.y
            z = location.z
            block = blocks[b]

            b_type = block.type
            if b_type != "air":
                collection = self.get_collection(b_type) # TODO: ontologize BlockFn
                isa_fact = ist_info_string + "(gridFact (isa (BlockFn {} {} {}) {})))".format(x, y, z, collection) # consider using blockOfType
                #adjacent_facts = self.get_adjacent_block_facts(x, y, z, ist_info_string) #TODO: get these via reasoning instead
                facts.append(isa_fact)
                #facts += adjacent_facts
        return facts

    def get_adjacent_block_facts(self, x, y, z, ist_info_string):
        self_token = "(BlockFn {} {} {})".format(x, y, z)
        adjacent_tokens = self.get_adjacent_blocks(x, y, z)
        adjacent_facts = [ist_info_string + "(nextTo " + self_token + " " + neighbor + "))" for neighbor in adjacent_tokens]
        return adjacent_facts

    def get_adjacent_blocks(self, x, y, z):
        h_neighbors = range(int(x) - 1, int(x) + 2)
        v_neighbors = range(int(y) - 1, int(y) + 2)
        d_neighbors = range(int(z) - 1, int(z) + 2)
        tokens = []
        for h in h_neighbors:
            for v in v_neighbors:
                for d in d_neighbors:
                    if h != x or v != y or d != z:
                        token = "(BlockFn {} {} {})".format(h, v, d)
                        tokens.append(token)
        return tokens

    def get_collection(self, name):
        collection = ""
        split = name.split("_")

        if name[0].isupper():  # already CamelCased
            collection = name
        elif len(split) < 2:
            collection = split[0].capitalize()
        elif split[1] == "seeds":
            item_type = split[0].capitalize()
            collection = "(SeedFn " + item_type + ")"
        else:
            collection = split[0].capitalize() + split[1].capitalize()
        return collection

        #(achieve :sender smgr:receiver interaction-manager:language fire:content (initializeToMExp ?gpool))


    class Worker(Thread):
        def __init__(self, parent: 'KQMLUpdater'):
            Thread.__init__(self)
            self.parent = parent
            self.character = parent.character
            self.connector = parent.character.connector
            self.experimentId = parent.character.experimentId
            self.kqml_lock = parent.character.kqml_lock

        def run(self):
            is_running: bool = False
            if hasattr(self.character.connector, "world_state"):
                is_running = self.connector.world_state.is_mission_running
            while (is_running and self.character._keep_running):
                logger.debug("updating KQML")
                #slurp character state to kqml and update database
                facts = self.parent.character_state_to_knowledge()

                self.kqml_lock.acquire()
                for fact in facts:
                    self.parent.insert_data('interaction-manager', fact)  # TODO: should be sending to wm only, but that's still broken I think
                    #.parent.achieve_on_agent('interaction-manager',
                    #                             "(doRerepresentation {})".format(self.experimentId))
                self.kqml_lock.release()
                time.sleep(self.parent.time_between_updates_in_ms)
                is_running = self.connector.world_state.is_mission_running

