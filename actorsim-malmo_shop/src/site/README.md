
This directory contains domain and problem files to generate SHOP plans for the malmo connector.

These files can be run via the java-shop-server and locally.

Either option requires running code from the shop-connector, which require compiling that code and setting the path.

Running the java-shop-server
============================

Copy the latest domain_minecraft_full.lisp to shop-connector-git/src/java-shop-connector/jar/
and then run "java -jar jshop2-server.jar" from that jar directory.

Running locally
===============

To compile locally using the make file, you must compile via make the project in shop-connector-git/src/jshop2/.
This requires setting the classpath as designated in the 'classpath.sh' file.


Files in this directory
=======================

Makefile - the make file to compile and run "domain.lisp" and "problem.lisp", which can be symlinked to any of the following:

domain_minecraft_full.lisp - the latest full domain for the minecraft connector

domain_[test].lisp  - the domain file for test (often a subset of domain_minecraft_full.lisp)
problem_[test].lisp - the problem file for test

