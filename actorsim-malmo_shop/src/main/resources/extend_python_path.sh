shop_connector_path=../../shop-connector-git/src/python-shop-connector/

tracegen_path=`pwd`
malmo_connector_root_path="$tracegen_path"/..
connector_path=$malmo_connector_root_path"/src/malmo-connector-src/"
utils_path=$malmo_connector_root_path"/src/utils-src/"
shop_path=$malmo_connector_root_path"/src/shop-src/"


export PYTHONPATH=$PYTHONPATH:$connector_path:$utils_path:$shop_path:$malmo_connector_root_path:$shop_connector_path



if [ -e MalmoPython.so ]
then
	echo "found Malmo library"
else
	echo "You need to link MalmoPython.so to $tracegen_path"
fi


# malmo_connector_root_path="/home/pavankantharaju/Documents/workspace/actorsim/"
# connector_path=$malmo_connector_root_path"minecraft-bb/malmo-connector/src/malmo-connector-src/"
# utils_path=$malmo_connector_root_path"minecraft-bb/malmo-connector/src/utils-src/"
# shop_path=$malmo_connector_root_path"minecraft-bb/malmo-connector/src/shop-src/"
# tracegen_path=$malmo_connector_root_path"minecraft-bb/malmo-connector/tracegen/"
# minecraft_malmo_path="/home/pavankantharaju/.virtualenv/venv-malmo/Malmo/Python_Examples"

# export PYTHONPATH=$PYTHONPATH:$connector_path:$utils_path:$shop_path:$tracegen_path:$minecraft_malmo_path

